; -------------------------------------------------------------------------------------------------
;
; MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
;  MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
;  MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
;  MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
;  MMMM       MMMM     PPPP              MMMM       MMMM
;  MMMM       MMMM     PPPP              MMMM       MMMM
; MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
;
;                       ZZZZZZZZZZZZZZ        1111       888888888888        000000000
;                     ZZZZZZZZZZZZZZ          1111     8888888888888888    0000000000000
;                             ZZZZ          111111     8888        8888  0000         0000
;                           ZZZZ              1111       888888888888    0000         0000
;                         ZZZZ                1111     8888        8888  0000         0000
;                       ZZZZZZZZZZZZZZ      11111111   8888888888888888    0000000000000
;                     ZZZZZZZZZZZZZZ        11111111     888888888888        000000000
;
;  Copyright (C) 1991-2024, Gunther Strube, hello@bits4fun.net
;
;  This file is part of Mpm.
;  Mpm is free software; you can redistribute it and/or modify
;  it under the terms of the GNU General Public License as published by the Free Software Foundation;
;  either version 2, or (at your option) any later version.
;  Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
;  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
;  See the GNU General Public License for more details.
;  You should have received a copy of the GNU General Public License along with Mpm;
;  see the file COPYING.  If not, write to the
;  Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
;
; -------------------------------------------------------------------------------------------------
;
; Z180 CPU reference test for Mpm V1.5
;
;  To see all opcodes, generate listing files: mpm -mz180 -va z80.asm z180.asm
;  To generate a binary: mpm -mz180 -oz180.bin -vab z80.asm z180.asm
;  (-v is verbose compile, can be omitted)
; -------------------------------------------------------------------------------------------------

module z180ref

; the Z180 specific instruction set

slp

mlt bc
mlt de
mlt hl
mlt sp

otdm
otdmr
otim
otimr

tst a
tst a,b
tst b
tst c
tst d
tst e
tst h
tst l
tst 0x20
tst (hl)
tstio 0x21

in0 a,(0x23)
in0 b,(0x23)
in0 c,(0x23)
in0 d,(0x23)
in0 e,(0x23)
in0 h,(0x23)
in0 l,(0x23)


out0 (0x24),a
out0 (0x24),b
out0 (0x24),c
out0 (0x24),d
out0 (0x24),e
out0 (0x24),h
out0 (0x24),l

