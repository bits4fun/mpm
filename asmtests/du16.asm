; ***************************************************************************************************
; Mpm test library routine, compiled as part of
;
;       mpm -d -xtest.lib du16 m16 m24
;

xlib du16

; ***************************************************************************************************
;
; 16-bit unsigned division
; (derived from GN_D16 / OZ, https://bitbucket.org/cambridge/oz, GPL v2)
;
; IN:
;    DE = divisor
;    HL = divident
;
; OUT:
;    Fc = 1, if division by zero attempted
;    HL = quotient
;    DE = remainder
;
; Registers changed after return:
;    ..BC..../IXIY  same
;    AF..DEHL/....  different
;
.du16
                    ld   a, e
                    or   d
                    scf                        ; divide by zero
                    ret  z
                    push bc
                    ld   c, l                  ; AC=HL(in)
                    ld   a, h
                    ld   hl, 0                 ; HL=0
                    ld   b, 16
                    or   a                     ; Fc=0
.d16_2
                    rl   c                     ; HLAC<<1 | Fc
                    rla
                    rl   l
                    rl   h
                    push hl                    ; HL-DE(in)>=0? Fc = 1, HC -= DE(in)
                    sbc  hl, de
                    ccf
                    jr   c, d16_3
                    ex   (sp), hl              ; else Fc=0
.d16_3
                    inc  sp
                    inc  sp
                    djnz d16_2

                    ex   de, hl                ; DE=remainder
                    rl   c                     ; HL= AC<<1 | Fc
                    ld   l, c
                    rla
                    ld   h, a
                    or   a                     ; Fc = 0
                    pop  bc
                    ret
