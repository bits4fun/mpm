; -------------------------------------------------------------------------------------------------
;
;  MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
;   MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
;   MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
;   MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
;   MMMM       MMMM     PPPP              MMMM       MMMM
;   MMMM       MMMM     PPPP              MMMM       MMMM
;  MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
;
;                        ZZZZZZZZZZZZZZ    888888888888        000000000
;                      ZZZZZZZZZZZZZZ    8888888888888888    0000000000000
;                              ZZZZ      8888        8888  0000         0000
;                            ZZZZ          888888888888    0000         0000
;                          ZZZZ          8888        8888  0000         0000
;                        ZZZZZZZZZZZZZZ  8888888888888888    0000000000000
;                      ZZZZZZZZZZZZZZ      888888888888        000000000
;
;  Copyright (C) 1991-2024, Gunther Strube, hello@bits4fun.net
;
;  This file is part of Mpm.
;  Mpm is free software; you can redistribute it and/or modify
;  it under the terms of the GNU General Public License as published by the Free Software Foundation;
;  either version 2, or (at your option) any later version.
;  Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
;  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
;  See the GNU General Public License for more details.
;  You should have received a copy of the GNU General Public License along with Mpm;
;  see the file COPYING.  If not, write to the
;  Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
;
; -----------------------------------------------------------------------------
;  Z80 CPU Instruction Reference, supported by Mpm V1.5
;
;  To see opcodes, generate listing file: mpm -mz80 -va z80.asm
;  To generate a binary: mpm -mz80 -vab z80.asm
;  (-v is verbose compile, can be omitted)
; -------------------------------------------------------------------------------------------------

module z80ref

org $4000

xdef reladrz80

; -------------------------------------------------------------------------------------------------
; Main instruction set
; -------------------------------------------------------------------------------------------------

        NOP
        LD      BC,$0000
        LD      (BC),A
        INC     BC
        INC     B
        DEC     B
        LD      B,0
        RLCA
        EX      AF,AF'
        ADD     HL,BC
        LD      A,(BC)
        DEC     BC
        INC     C
        DEC     C
        LD      C,0
        RRCA
reladrz80
        DJNZ    reladrz80
        LD      DE,$0000
        LD      (DE),A
        INC     DE
        INC     D
        DEC     D
        LD      D,0
        RLA
        JR      reljmp2
        ADD     HL,DE
reljmp2 LD      A,(DE)
        DEC     DE
        INC     E
        DEC     E
        LD      E,0
        RRA
        JR      NZ, reladrz80
        LD      HL,$0000
        LD      ($0000),HL
        INC     HL
        INC     H
        DEC     H
        LD      H,0
        DAA
        JR      Z, reladrz80
        ADD     HL,HL
        LD      HL,($9988)
        DEC     HL
        INC     L
        DEC     L
        LD      L,0
        CPL
        JR      NC, reladrz80
        LD      SP,$0000
        LD      ($0000),A
        INC     SP
        INC     (HL)
        DEC     (HL)
        LD      (HL),0
        SCF
        JR      C, reladrz80
        ADD     HL,SP
        LD      A,($0000)
        DEC     SP
        INC     A
        DEC     A
        LD      A,0
        CCF
        LD      B,B
        LD      B,C
        LD      B,D
        LD      B,E
        LD      B,H
        LD      B,L
        LD      B,(HL)
        LD      B,A
        LD      C,B
        LD      C,C
        LD      C,D
        LD      C,E
        LD      C,H
        LD      C,L
        LD      C,(HL)
        LD      C,A
        LD      D,B
        LD      D,C
        LD      D,D
        LD      D,E
        LD      D,H
        LD      D,L
        LD      D,(HL)
        LD      D,A
        LD      E,B
        LD      E,C
        LD      E,D
        LD      E,E
        LD      E,H
        LD      E,L
        LD      E,(HL)
        LD      E,A
        LD      H,B
        LD      H,C
        LD      H,D
        LD      H,E
        LD      H,H
        LD      H,L
        LD      H,(HL)
        LD      H,A
        LD      L,B
        LD      L,C
        LD      L,D
        LD      L,E
        LD      L,H
        LD      L,L
        LD      L,(HL)
        LD      L,A
        LD      (HL),B
        LD      (HL),C
        LD      (HL),D
        LD      (HL),E
        LD      (HL),H
        LD      (HL),L
        HALT
        LD      (HL),A
        LD      A,B
        LD      A,C
        LD      A,D
        LD      A,E
        LD      A,H
        LD      A,L
        LD      A,(HL)
        LD      A,A
        ADD     A,B
        ADD     A,C
        ADD     A,D
        ADD     A,E
        ADD     A,H
        ADD     A,L
        ADD     A,(HL)
        ADD     A,A
        ADC     A,B
        ADC     A,C
        ADC     A,D
        ADC     A,E
        ADC     A,H
        ADC     A,L
        ADC     A,(HL)
        ADC     A,A
        SUB     B
        SUB     C
        SUB     D
        SUB     E
        SUB     H
        SUB     L
        SUB     (HL)
        SUB     A,A                             ; optional syntax
        SUB     A
        SBC     A,B
        SBC     A,C
        SBC     A,D
        SBC     A,E
        SBC     A,H
        SBC     A,L
        SBC     A,(HL)
        SBC     A,A
        AND     A,B                             ; optional syntax
        AND     B
        AND     C
        AND     D
        AND     E
        AND     H
        AND     L
        AND     (HL)
        AND     A
        XOR     A,B                             ; optional syntax
        XOR     B
        XOR     C
        XOR     D
        XOR     E
        XOR     H
        XOR     L
        XOR     (HL)
        XOR     A
        OR      A,B                             ; optional syntax
        OR      B
        OR      C
        OR      D
        OR      E
        OR      H
        OR      L
        OR      (HL)
        OR      A
        CP      A,B                             ; optional syntax
        CP      B
        CP      C
        CP      D
        CP      E
        CP      H
        CP      L
        CP      (HL)
        CP      A
        RET     NZ
        POP     BC
        JP      NZ,$0000
        JP      $0000
        CALL    NZ,$0000
        PUSH    BC
        ADD     A,0
        RST     0
        RET     Z
        RET
        JP      Z,$0000

        ; CBh instruction group

        CALL    Z,$0000
        CALL    $0000
        ADC     A,0
        RST     08h
        RET     NC
        POP     DE
        JP      NC,$0000
        OUT     (0),A
        CALL    NC,$0000
        PUSH    DE
        SUB     12
        RST     10h
        EXX
        RET     C
        JP      C,$0000
        IN      A,(0)
        CALL    C,$0000

        ; DDh instruction group

        SBC     A,0
        RST     18h
        RET     PO
        POP     HL
        JP      PO,$0000
        EX      (SP),HL
        CALL    PO,$0000
        PUSH    HL
        AND     0
        RST     $20
        RET     PE
        JP      (HL)
        JP      PE,$0000
        EX      DE,HL
        CALL    PE,$0000

        ; EDh instruction group

        XOR     0
        RST     28h
        RET     P
        POP     AF
        JP      P,$0000
        DI
        CALL    P,$0000
        PUSH    AF
        OR      0
        RST     30h
        RET     M
        LD      SP,HL
        JP      M,$0000
        EI
        CALL    M,$0000

        ; FDh    instruction group

        CP      0
        RST     38h



; -------------------------------------------------------------------------------------------------
; Bit instruction group, CB xx
; -------------------------------------------------------------------------------------------------

        RLC     B
        RLC     C
        RLC     D
        RLC     E
        RLC     H
        RLC     L
        RLC     (HL)
        RLC     A
        RRC     B
        RRC     C
        RRC     D
        RRC     E
        RRC     H
        RRC     L
        RRC     (HL)
        RRC     A
        RL      B
        RL      C
        RL      D
        RL      E
        RL      H
        RL      L
        RL      (HL)
        RL      A
        RR      B
        RR      C
        RR      D
        RR      E
        RR      H
        RR      L
        RR      (HL)
        RR      A
        SLA     B
        SLA     C
        SLA     D
        SLA     E
        SLA     H
        SLA     L
        SLA     (HL)
        SLA     A
        SRA     B
        SRA     C
        SRA     D
        SRA     E
        SRA     H
        SRA     L
        SRA     (HL)
        SRA     A

if !Z380
        SLL     B                  ; Undocumented
        SLL     C                  ; Undocumented
        SLL     D                  ; Undocumented
        SLL     E                  ; Undocumented
        SLL     H                  ; Undocumented
        SLL     L                  ; Undocumented
        SLL     (HL)               ; Undocumented
        SLL     A                  ; Undocumented
endif

        SRL     B
        SRL     C
        SRL     D
        SRL     E
        SRL     H
        SRL     L
        SRL     (HL)
        SRL     A
        BIT     0,B
        BIT     0,C
        BIT     0,D
        BIT     0,E
        BIT     0,H
        BIT     0,L
        BIT     0,(HL)
        BIT     0,A
        BIT     1,B
        BIT     1,C
        BIT     1,D
        BIT     1,E
        BIT     1,H
        BIT     1,L
        BIT     1,(HL)
        BIT     1,A
        BIT     2,B
        BIT     2,C
        BIT     2,D
        BIT     2,E
        BIT     2,H
        BIT     2,L
        BIT     2,(HL)
        BIT     2,A
        BIT     3,B
        BIT     3,C
        BIT     3,D
        BIT     3,E
        BIT     3,H
        BIT     3,L
        BIT     3,(HL)
        BIT     3,A
        BIT     4,B
        BIT     4,C
        BIT     4,D
        BIT     4,E
        BIT     4,H
        BIT     4,L
        BIT     4,(HL)
        BIT     4,A
        BIT     5,B
        BIT     5,C
        BIT     5,D
        BIT     5,E
        BIT     5,H
        BIT     5,L
        BIT     5,(HL)
        BIT     5,A
        BIT     6,B
        BIT     6,C
        BIT     6,D
        BIT     6,E
        BIT     6,H
        BIT     6,L
        BIT     6,(HL)
        BIT     6,A
        BIT     7,B
        BIT     7,C
        BIT     7,D
        BIT     7,E
        BIT     7,H
        BIT     7,L
        BIT     7,(HL)
        BIT     7,A
        RES     0,B
        RES     0,C
        RES     0,D
        RES     0,E
        RES     0,H
        RES     0,L
        RES     0,(HL)
        RES     0,A
        RES     1,B
        RES     1,C
        RES     1,D
        RES     1,E
        RES     1,H
        RES     1,L
        RES     1,(HL)
        RES     1,A
        RES     2,B
        RES     2,C
        RES     2,D
        RES     2,E
        RES     2,H
        RES     2,L
        RES     2,(HL)
        RES     2,A
        RES     3,B
        RES     3,C
        RES     3,D
        RES     3,E
        RES     3,H
        RES     3,L
        RES     3,(HL)
        RES     3,A
        RES     4,B
        RES     4,C
        RES     4,D
        RES     4,E
        RES     4,H
        RES     4,L
        RES     4,(HL)
        RES     4,A
        RES     5,B
        RES     5,C
        RES     5,D
        RES     5,E
        RES     5,H
        RES     5,L
        RES     5,(HL)
        RES     5,A
        RES     6,B
        RES     6,C
        RES     6,D
        RES     6,E
        RES     6,H
        RES     6,L
        RES     6,(HL)
        RES     6,A
        RES     7,B
        RES     7,C
        RES     7,D
        RES     7,E
        RES     7,H
        RES     7,L
        RES     7,(HL)
        RES     7,A
        SET     0,B
        SET     0,C
        SET     0,D
        SET     0,E
        SET     0,H
        SET     0,L
        SET     0,(HL)
        SET     0,A
        SET     1,B
        SET     1,C
        SET     1,D
        SET     1,E
        SET     1,H
        SET     1,L
        SET     1,(HL)
        SET     1,A
        SET     2,B
        SET     2,C
        SET     2,D
        SET     2,E
        SET     2,H
        SET     2,L
        SET     2,(HL)
        SET     2,A
        SET     3,B
        SET     3,C
        SET     3,D
        SET     3,E
        SET     3,H
        SET     3,L
        SET     3,(HL)
        SET     3,A
        SET     4,B
        SET     4,C
        SET     4,D
        SET     4,E
        SET     4,H
        SET     4,L
        SET     4,(HL)
        SET     4,A
        SET     5,B
        SET     5,C
        SET     5,D
        SET     5,E
        SET     5,H
        SET     5,L
        SET     5,(HL)
        SET     5,A
        SET     6,B
        SET     6,C
        SET     6,D
        SET     6,E
        SET     6,H
        SET     6,L
        SET     6,(HL)
        SET     6,A
        SET     7,B
        SET     7,C
        SET     7,D
        SET     7,E
        SET     7,H
        SET     7,L
        SET     7,(HL)
        SET     7,A


; -------------------------------------------------------------------------------------------------
; IX instruction group, DD xx
; -------------------------------------------------------------------------------------------------

        ADD     IX,BC
        ADD     IX,DE
        LD      IX,$0000
        LD      ($0000),IX
        INC     IX

        INC     IXH                ; Undocumented
        DEC     IXH                ; Undocumented
        LD      IXH,0              ; Undocumented

        ADD     IX,IX
        LD      IX,($0000)
        DEC     IX

        INC     IXL                ; Undocumented
        DEC     IXL                ; Undocumented
        LD      IXL,0              ; Undocumented

        INC     (IX+0)
        DEC     (IX+0)
        INC     (IX)
        DEC     (IX)               ; +0 displacement is default, when not specified

        LD      (IX),01h           ; +0 displacement is default, when not specified
        LD      (IX+127),01h

        ADD     IX,SP

        LD      B,IXH              ; Undocumented
        LD      B,IXU              ; Undocumented (IXU = IXH, alternate notation, used for both Z80 and Z380)

        LD      B,IXL              ; Undocumented

        LD      B,(IX+0)
        LD      B,(IX)             ; +0 displacement is default, when not specified

        LD      C,IXH              ; Undocumented
        LD      C,IXL              ; Undocumented

        LD      C,(IX+0)
        LD      C,(IX)             ; +0 displacement is default, when not specified

        LD      D,IXH              ; Undocumented
        LD      D,IXL              ; Undocumented

        LD      D,(IX+0)

        LD      E,IXH              ; Undocumented
        LD      E,IXL              ; Undocumented

        LD      E,(IX+0)

        LD      IXH,B              ; Undocumented
        LD      IXH,C              ; Undocumented
        LD      IXH,D              ; Undocumented
        LD      IXH,E              ; Undocumented
        LD      IXH,IXH            ; Undocumented
        LD      IXH,IXL            ; Undocumented

        LD      H,(IX+0)

        LD      IXH,A              ; Undocumented
        LD      IXL,B              ; Undocumented
        LD      IXL,C              ; Undocumented
        LD      IXL,D              ; Undocumented
        LD      IXL,E              ; Undocumented
        LD      IXL,IXH            ; Undocumented
        LD      IXL,IXL            ; Undocumented

        LD      L,(IX+0)

        LD      IXL,A              ; Undocumented

        LD      (IX+0),B
        LD      (IX),B             ; +0 displacement is default, when not specified
        LD      (IX+0),C
        LD      (IX),C             ; +0 displacement is default, when not specified
        LD      (IX+0),D
        LD      (IX+0),E
        LD      (IX+0),H
        LD      (IX+0),L
        LD      (IX+0),A

        LD      A,IXH              ; Undocumented
        LD      A,IXL              ; Undocumented

        LD      A,(IX+0)

        ADD     A,IXH              ; Undocumented
        ADD     A,IXL              ; Undocumented

        ADD     A,(IX+0)
        ADD     A,(IX)             ; +0 displacement is default, when not specified

        ADC     A,IXH              ; Undocumented
        ADC     A,IXL              ; Undocumented

        ADC     A,(IX+0)
        ADC     A,(IX)             ; +0 displacement is default, when not specified

        SUB     A,IXH              ; Undocumented
        SUB     A,IXL              ; Undocumented

        SUB     (IX+0)
        SUB     (IX)               ; +0 displacement is default, when not specified

        SBC     A,IXH              ; Undocumented
        SBC     A,IXL              ; Undocumented

        SBC     A,(IX+0)
        SBC     A,(IX)             ; +0 displacement is default, when not specified

        AND     A,IXH              ; Undocumented
        AND     A,IXL              ; Undocumented

        AND     (IX+0)
        AND     (IX)               ; +0 displacement is default, when not specified

        XOR     A,IXH              ; Undocumented
        XOR     A,IXL              ; Undocumented

        XOR     (IX+0)
        XOR     (IX)               ; +0 displacement is default, when not specified

        OR      A,IXH              ; Undocumented
        OR      A,IXL              ; Undocumented

        OR      (IX+0)
        OR      (IX)               ; +0 displacement is default, when not specified

        CP      A,IXH              ; Undocumented
        CP      A,IXL              ; Undocumented

        CP      (IX+0)
        CP      (IX)               ; +0 displacement is default, when not specified

        RLC     (IX+2)
        RLC     (IX)               ; +0 displacement is default, when not specified

        RRC     (IX+2)
        RRC     (IX)               ; +0 displacement is default, when not specified

        RL      (IX+2)
        RL      (IX)               ; +0 displacement is default, when not specified

        RR      (IX+2)
        RR      (IX)               ; +0 displacement is default, when not specified

        SLA     (IX+2)
        SLA     (IX)               ; +0 displacement is default, when not specified

        SRA     (IX+2)
        SRA     (IX)               ; +0 displacement is default, when not specified

        SRL     (IX+2)
        SRL     (IX)               ; +0 displacement is default, when not specified

if !Z380
        RLC     (IX+0),B           ; Undocumented
        RLC     (IX),B             ; +0 displacement is default, when not specified
        RLC     (IX+0),C           ; Undocumented
        RLC     (IX+0),D           ; Undocumented
        RLC     (IX+0),E           ; Undocumented
        RLC     (IX+0),H           ; Undocumented
        RLC     (IX+0),L           ; Undocumented
        RLC     (IX+0),A           ; Undocumented

        RRC     (IX+0),B           ; Undocumented
        RRC     (IX+0),C           ; Undocumented
        RRC     (IX+0),D           ; Undocumented
        RRC     (IX+0),E           ; Undocumented
        RRC     (IX+0),H           ; Undocumented
        RRC     (IX+0),L           ; Undocumented
        RRC     (IX+0),A           ; Undocumented

        RL      (IX+0),B           ; Undocumented
        RL      (IX+0),C           ; Undocumented
        RL      (IX+0),D           ; Undocumented
        RL      (IX+0),E           ; Undocumented
        RL      (IX+0),H           ; Undocumented
        RL      (IX+0),L           ; Undocumented
        RL      (IX+0),A           ; Undocumented

        RR      (IX+0),B           ; Undocumented
        RR      (IX+0),C           ; Undocumented
        RR      (IX+0),D           ; Undocumented
        RR      (IX+0),E           ; Undocumented
        RR      (IX+0),H           ; Undocumented
        RR      (IX+0),L           ; Undocumented
        RR      (IX+0),A           ; Undocumented

        SLA     (IX+0),B           ; Undocumented
        SLA     (IX+0),C           ; Undocumented
        SLA     (IX+0),D           ; Undocumented
        SLA     (IX+0),E           ; Undocumented
        SLA     (IX+0),H           ; Undocumented
        SLA     (IX+0),L           ; Undocumented
        SLA     (IX+0),A           ; Undocumented

        SRA     (IX+0),B           ; Undocumented
        SRA     (IX+0),C           ; Undocumented
        SRA     (IX+0),D           ; Undocumented
        SRA     (IX+0),E           ; Undocumented
        SRA     (IX+0),H           ; Undocumented
        SRA     (IX+0),L           ; Undocumented
        SRA     (IX+0),A           ; Undocumented

        SRL     (IX+0),B           ; Undocumented
        SRL     (IX+0),C           ; Undocumented
        SRL     (IX+0),D           ; Undocumented
        SRL     (IX+0),E           ; Undocumented
        SRL     (IX+0),H           ; Undocumented
        SRL     (IX+0),L           ; Undocumented
        SRL     (IX+0),A           ; Undocumented

        SLL     (IX+0),B           ; Undocumented
        SLL     (IX+0),C           ; Undocumented
        SLL     (IX+0),D           ; Undocumented
        SLL     (IX+0),E           ; Undocumented
        SLL     (IX+0),H           ; Undocumented
        SLL     (IX+0),L           ; Undocumented
        SLL     (IX+0)             ; Undocumented
        SLL     (IX)               ; +0 displacement is default, when not specified
        SLL     (IX+0),A           ; Undocumented
endif

        BIT     0,(IX+0)
        BIT     0,(IX)             ; +0 displacement is default, when not specified
        BIT     1,(IX+0)
        BIT     2,(IX+0)
        BIT     3,(IX+0)
        BIT     4,(IX+0)
        BIT     5,(IX+0)
        BIT     6,(IX+0)
        BIT     7,(IX+0)

        RES     0,(IX+0)
        RES     0,(IX)             ; +0 displacement is default, when not specified
        RES     1,(IX+0)
        RES     2,(IX+0)
        RES     3,(IX+0)
        RES     4,(IX+0)
        RES     5,(IX+0)
        RES     6,(IX+0)
        RES     7,(IX+0)

        SET     0,(IX+0)
        SET     0,(IX)             ; +0 displacement is default, when not specified
        SET     1,(IX+0)
        SET     2,(IX+0)
        SET     3,(IX+0)
        SET     4,(IX+0)
        SET     5,(IX+0)
        SET     6,(IX+0)
        SET     7,(IX+0)

if !Z380
        BIT     0,(IX+0),B         ; Undocumented
        BIT     0,(IX),B           ; +0 displacement is default, when not specified
        BIT     0,(IX+0),C         ; Undocumented
        BIT     0,(IX+0),D         ; Undocumented
        BIT     0,(IX+0),E         ; Undocumented
        BIT     0,(IX+0),H         ; Undocumented
        BIT     0,(IX+0),L         ; Undocumented
        BIT     0,(IX+0),A         ; Undocumented

        BIT     1,(IX+0),B         ; Undocumented
        BIT     1,(IX+0),C         ; Undocumented
        BIT     1,(IX+0),D         ; Undocumented
        BIT     1,(IX+0),E         ; Undocumented
        BIT     1,(IX+0),H         ; Undocumented
        BIT     1,(IX+0),L         ; Undocumented
        BIT     1,(IX+0),A         ; Undocumented

        BIT     2,(IX+0),B         ; Undocumented
        BIT     2,(IX+0),C         ; Undocumented
        BIT     2,(IX+0),D         ; Undocumented
        BIT     2,(IX+0),E         ; Undocumented
        BIT     2,(IX+0),H         ; Undocumented
        BIT     2,(IX+0),L         ; Undocumented
        BIT     2,(IX+0),A         ; Undocumented

        BIT     3,(IX+0),B         ; Undocumented
        BIT     3,(IX+0),C         ; Undocumented
        BIT     3,(IX+0),D         ; Undocumented
        BIT     3,(IX+0),E         ; Undocumented
        BIT     3,(IX+0),H         ; Undocumented
        BIT     3,(IX+0),L         ; Undocumented
        BIT     3,(IX+0),A         ; Undocumented

        BIT     4,(IX+0),B         ; Undocumented
        BIT     4,(IX+0),C         ; Undocumented
        BIT     4,(IX+0),D         ; Undocumented
        BIT     4,(IX+0),E         ; Undocumented
        BIT     4,(IX+0),H         ; Undocumented
        BIT     4,(IX+0),L         ; Undocumented
        BIT     4,(IX+0),A         ; Undocumented

        BIT     5,(IX+0),B         ; Undocumented
        BIT     5,(IX+0),C         ; Undocumented
        BIT     5,(IX+0),D         ; Undocumented
        BIT     5,(IX+0),E         ; Undocumented
        BIT     5,(IX+0),H         ; Undocumented
        BIT     5,(IX+0),L         ; Undocumented
        BIT     5,(IX+0),A         ; Undocumented

        BIT     6,(IX+0),B         ; Undocumented
        BIT     6,(IX+0),C         ; Undocumented
        BIT     6,(IX+0),D         ; Undocumented
        BIT     6,(IX+0),E         ; Undocumented
        BIT     6,(IX+0),H         ; Undocumented
        BIT     6,(IX+0),L         ; Undocumented
        BIT     6,(IX+0),A         ; Undocumented

        BIT     7,(IX+0),B         ; Undocumented
        BIT     7,(IX+0),C         ; Undocumented
        BIT     7,(IX+0),D         ; Undocumented
        BIT     7,(IX+0),E         ; Undocumented
        BIT     7,(IX+0),H         ; Undocumented
        BIT     7,(IX+0),L         ; Undocumented
        BIT     7,(IX+0),A         ; Undocumented

        RES     0,(IX+0),B         ; Undocumented
        RES     0,(IX),B           ; +0 displacement is default, when not specified
        RES     0,(IX+0),C         ; Undocumented
        RES     0,(IX+0),D         ; Undocumented
        RES     0,(IX+0),E         ; Undocumented
        RES     0,(IX+0),H         ; Undocumented
        RES     0,(IX+0),L         ; Undocumented
        RES     0,(IX+0),A         ; Undocumented

        RES     1,(IX+0),B         ; Undocumented
        RES     1,(IX+0),C         ; Undocumented
        RES     1,(IX+0),D         ; Undocumented
        RES     1,(IX+0),E         ; Undocumented
        RES     1,(IX+0),H         ; Undocumented
        RES     1,(IX+0),L         ; Undocumented
        RES     1,(IX+0),A         ; Undocumented

        RES     2,(IX+0),B         ; Undocumented
        RES     2,(IX+0),C         ; Undocumented
        RES     2,(IX+0),D         ; Undocumented
        RES     2,(IX+0),E         ; Undocumented
        RES     2,(IX+0),H         ; Undocumented
        RES     2,(IX+0),L         ; Undocumented
        RES     2,(IX+0),A         ; Undocumented

        RES     3,(IX+0),B         ; Undocumented
        RES     3,(IX+0),C         ; Undocumented
        RES     3,(IX+0),D         ; Undocumented
        RES     3,(IX+0),E         ; Undocumented
        RES     3,(IX+0),H         ; Undocumented
        RES     3,(IX+0),L         ; Undocumented
        RES     3,(IX+0),A         ; Undocumented

        RES     4,(IX+0),B         ; Undocumented
        RES     4,(IX+0),C         ; Undocumented
        RES     4,(IX+0),D         ; Undocumented
        RES     4,(IX+0),E         ; Undocumented
        RES     4,(IX+0),H         ; Undocumented
        RES     4,(IX+0),L         ; Undocumented
        RES     4,(IX+0),A         ; Undocumented

        RES     5,(IX+0),B         ; Undocumented
        RES     5,(IX+0),C         ; Undocumented
        RES     5,(IX+0),D         ; Undocumented
        RES     5,(IX+0),E         ; Undocumented
        RES     5,(IX+0),H         ; Undocumented
        RES     5,(IX+0),L         ; Undocumented
        RES     5,(IX+0),A         ; Undocumented

        RES     6,(IX+0),B         ; Undocumented
        RES     6,(IX+0),C         ; Undocumented
        RES     6,(IX+0),D         ; Undocumented
        RES     6,(IX+0),E         ; Undocumented
        RES     6,(IX+0),H         ; Undocumented
        RES     6,(IX+0),L         ; Undocumented
        RES     6,(IX+0),A         ; Undocumented

        RES     7,(IX+0),B         ; Undocumented
        RES     7,(IX+0),C         ; Undocumented
        RES     7,(IX+0),D         ; Undocumented
        RES     7,(IX+0),E         ; Undocumented
        RES     7,(IX+0),H         ; Undocumented
        RES     7,(IX+0),L         ; Undocumented
        RES     7,(IX+0),A         ; Undocumented

        SET     0,(IX+0),B         ; Undocumented
        SET     0,(IX),B           ; +0 displacement is default, when not specified
        SET     0,(IX+0),C         ; Undocumented
        SET     0,(IX+0),D         ; Undocumented
        SET     0,(IX+0),E         ; Undocumented
        SET     0,(IX+0),H         ; Undocumented
        SET     0,(IX+0),L         ; Undocumented
        SET     0,(IX+0),A         ; Undocumented

        SET     1,(IX+0),B         ; Undocumented
        SET     1,(IX+0),C         ; Undocumented
        SET     1,(IX+0),D         ; Undocumented
        SET     1,(IX+0),E         ; Undocumented
        SET     1,(IX+0),H         ; Undocumented
        SET     1,(IX+0),L         ; Undocumented
        SET     1,(IX+0),A         ; Undocumented

        SET     2,(IX+0),B         ; Undocumented
        SET     2,(IX+0),C         ; Undocumented
        SET     2,(IX+0),D         ; Undocumented
        SET     2,(IX+0),E         ; Undocumented
        SET     2,(IX+0),H         ; Undocumented
        SET     2,(IX+0),L         ; Undocumented
        SET     2,(IX+0),A         ; Undocumented

        SET     3,(IX+0),B         ; Undocumented
        SET     3,(IX+0),C         ; Undocumented
        SET     3,(IX+0),D         ; Undocumented
        SET     3,(IX+0),E         ; Undocumented
        SET     3,(IX+0),H         ; Undocumented
        SET     3,(IX+0),L         ; Undocumented
        SET     3,(IX+0),A         ; Undocumented

        SET     4,(IX+0),B         ; Undocumented
        SET     4,(IX+0),C         ; Undocumented
        SET     4,(IX+0),D         ; Undocumented
        SET     4,(IX+0),E         ; Undocumented
        SET     4,(IX+0),H         ; Undocumented
        SET     4,(IX+0),L         ; Undocumented
        SET     4,(IX+0),A         ; Undocumented

        SET     5,(IX+0),B         ; Undocumented
        SET     5,(IX+0),C         ; Undocumented
        SET     5,(IX+0),D         ; Undocumented
        SET     5,(IX+0),E         ; Undocumented
        SET     5,(IX+0),H         ; Undocumented
        SET     5,(IX+0),L         ; Undocumented
        SET     5,(IX+0),A         ; Undocumented

        SET     6,(IX+0),B         ; Undocumented
        SET     6,(IX+0),C         ; Undocumented
        SET     6,(IX+0),D         ; Undocumented
        SET     6,(IX+0),E         ; Undocumented
        SET     6,(IX+0),H         ; Undocumented
        SET     6,(IX+0),L         ; Undocumented
        SET     6,(IX+0),A         ; Undocumented

        SET     7,(IX+0),B         ; Undocumented
        SET     7,(IX+0),C         ; Undocumented
        SET     7,(IX+0),D         ; Undocumented
        SET     7,(IX+0),E         ; Undocumented
        SET     7,(IX+0),H         ; Undocumented
        SET     7,(IX+0),L         ; Undocumented
        SET     7,(IX+0),A         ; Undocumented
endif
        POP     IX
        EX      (SP),IX
        PUSH    IX
        JP      (IX)
        LD      SP,IX


; -------------------------------------------------------------------------------------------------
; Extended instruction group, ED xx
; -------------------------------------------------------------------------------------------------

        IN      B,(C)
        OUT     (C),B
        SBC     HL,BC
        LD      ($0000),BC
        NEG
        RETN
        IM      0
        LD      I,A
        IN      C,(C)
        OUT     (C),C
        ADC     HL,BC
        LD      BC,($0000)
        RETI
        LD      R,A
        IN      D,(C)
        OUT     (C),D
        SBC     HL,DE
        LD      ($0000),DE
        IM      1
        LD      A,I
        IN      E,(C)
        OUT     (C),E
        ADC     HL,DE
        LD      DE,($0000)
        IM      2
        LD      A,R
        IN      H,(C)
        OUT     (C),H
        SBC     HL,HL
        LD      ($0000),HL         ; ED 63 nn nn (Mpm optimizes to 22 nn nn)
        RRD
        IN      L,(C)
        OUT     (C),L
        ADC     HL,HL
        LD      HL,($0000)         ; ED 6B nn nn (Mpm optimizes to 2A nn nn)
        RLD
        IN      F,(C)              ; Undocumented
        SBC     HL,SP
        LD      ($0000),SP
        IN      A,(C)
        OUT     (C),A
        ADC     HL,SP
        LD      SP,($0000)
        LDI
        CPI
        INI
        OUTI
        LDD
        CPD
        IND
        OUTD
        LDIR
        CPIR
        INIR
        OTIR
        LDDR
        CPDR
        INDR
        OTDR


; -------------------------------------------------------------------------------------------------
; IY instruction group, FD xx
; -------------------------------------------------------------------------------------------------

        ADD     IY,BC
        ADD     IY,DE
        LD      IY,$0000
        LD      ($0000),IY
        INC     IY

        INC     IYH                ; Undocumented
        DEC     IYH                ; Undocumented
        LD      IYH,0              ; Undocumented

        ADD     IY,IY
        LD      IY,($0000)
        DEC     IY

        INC     IYL                ; Undocumented
        DEC     IYL                ; Undocumented
        LD      IYL,0              ; Undocumented

        INC     (IY+0)
        DEC     (IY+0)
        INC     (IY)
        DEC     (IY)               ; +0 displacement is default, when not specified

        LD      (IY+0),01h
        LD      (IY),01h           ; +0 displacement is default, when not specified

        ADD     IY,SP

        LD      B,IYH              ; Undocumented
        LD      B,IYL              ; Undocumented

        LD      B,(IY+0)
        LD      B,(IY)             ; +0 displacement is default, when not specified

        LD      C,IYH              ; Undocumented
        LD      C,IYL              ; Undocumented

        LD      C,(IY+0)

        LD      D,IYH              ; Undocumented
        LD      D,IYL              ; Undocumented

        LD      D,(IY+0)

        LD      E,IYH              ; Undocumented
        LD      E,IYL              ; Undocumented

        LD      E,(IY+0)

        LD      IYH,B              ; Undocumented
        LD      IYH,C              ; Undocumented
        LD      IYH,D              ; Undocumented
        LD      IYH,E              ; Undocumented
        LD      IYH,IYH            ; Undocumented
        LD      IYH,IYL            ; Undocumented

        LD      H,(IY+0)

        LD      IYH,A              ; Undocumented
        LD      IYU,A              ; Undocumented (IYU = IYH, alternate notation, used for both Z80 and Z380))
        LD      IYL,B              ; Undocumented
        LD      IYL,C              ; Undocumented
        LD      IYL,D              ; Undocumented
        LD      IYL,E              ; Undocumented
        LD      IYL,IYH            ; Undocumented
        LD      IYL,IYL            ; Undocumented

        LD      L,(IY+0)

        LD      IYL,A              ; Undocumented

        LD      (IY+0),B
        LD      (IY),B             ; +0 displacement is default, when not specified
        LD      (IY+0),C
        LD      (IY+0),D
        LD      (IY+0),E
        LD      (IY+0),H
        LD      (IY+0),L
        LD      (IY+0),A

        LD      A,IYH              ; Undocumented
        LD      A,IYL              ; Undocumented

        LD      A,(IY+0)

        ADD     A,IYH              ; Undocumented
        ADD     A,IYL              ; Undocumented

        ADD     A,(IY+0)
        ADD     A,(IY)             ; +0 displacement is default, when not specified

        ADC     A,IYH              ; Undocumented
        ADC     A,IYL              ; Undocumented

        ADC     A,(IY+0)
        ADC     (IY)               ; +0 displacement is default, when not specified

        SUB     A,IYH              ; Undocumented
        SUB     A,IYL              ; Undocumented

        SUB     (IY+0)
        SUB     (IY)               ; +0 displacement is default, when not specified

        SBC     A,IYH              ; Undocumented
        SBC     A,IYL              ; Undocumented

        SBC     A,(IY+0)
        SBC     (IY)               ; +0 displacement is default, when not specified

        AND     A,IYH              ; Undocumented
        AND     A,IYL              ; Undocumented

        AND     (IY+0)
        AND     (IY)               ; +0 displacement is default, when not specified

        XOR     A,IYH              ; Undocumented
        XOR     A,IYL              ; Undocumented

        XOR     (IY+0)
        XOR     (IY)               ; +0 displacement is default, when not specified

        OR      A,IYH              ; Undocumented
        OR      A,IYL              ; Undocumented

        OR      (IY+0)
        OR      (IY)               ; +0 displacement is default, when not specified

        CP      A,IYH              ; Undocumented
        CP      A,IYL              ; Undocumented

        CP      (IY+2)
        CP      (IY)               ; +0 displacement is default, when not specified

        RLC     (IY+2)
        RLC     (IY)               ; +0 displacement is default, when not specified

        RRC     (IY+2)
        RRC     (IY)               ; +0 displacement is default, when not specified

        RL      (IY+2)
        RL      (IY)               ; +0 displacement is default, when not specified

        RR      (IY+2)
        RR      (IY)               ; +0 displacement is default, when not specified

        SLA     (IY+2)
        SLA     (IY)               ; +0 displacement is default, when not specified

        SRA     (IY+2)
        SRA     (IY)               ; +0 displacement is default, when not specified

        SRL     (IY+2)
        SRL     (IY)               ; +0 displacement is default, when not specified

if !Z380
        RLC     (IY+0),B           ; Undocumented
        RLC     (IY),B             ; Undocumented, +0 displacement is default, when not specified
        RLC     (IY+0),C           ; Undocumented
        RLC     (IY+0),D           ; Undocumented
        RLC     (IY+0),E           ; Undocumented
        RLC     (IY+0),H           ; Undocumented
        RLC     (IY+0),L           ; Undocumented
        RLC     (IY+0),A           ; Undocumented

        RRC     (IY+0),B           ; Undocumented
        RRC     (IY+0),C           ; Undocumented
        RRC     (IY+0),D           ; Undocumented
        RRC     (IY+0),E           ; Undocumented
        RRC     (IY+0),H           ; Undocumented
        RRC     (IY+0),L           ; Undocumented
        RRC     (IY+0),A           ; Undocumented

        RL      (IY+0),B           ; Undocumented
        RL      (IY+0),C           ; Undocumented
        RL      (IY+0),D           ; Undocumented
        RL      (IY+0),E           ; Undocumented
        RL      (IY+0),H           ; Undocumented
        RL      (IY+0),L           ; Undocumented
        RL      (IY+0),A           ; Undocumented

        RR      (IY+0),B           ; Undocumented
        RR      (IY+0),C           ; Undocumented
        RR      (IY+0),D           ; Undocumented
        RR      (IY+0),E           ; Undocumented
        RR      (IY+0),H           ; Undocumented
        RR      (IY+0),L           ; Undocumented
        RR      (IY+0),A           ; Undocumented

        SLA     (IY+0),B           ; Undocumented
        SLA     (IY+0),C           ; Undocumented
        SLA     (IY+0),D           ; Undocumented
        SLA     (IY+0),E           ; Undocumented
        SLA     (IY+0),H           ; Undocumented
        SLA     (IY+0),L           ; Undocumented
        SLA     (IY+0),A           ; Undocumented

        SRA     (IY+0),B           ; Undocumented
        SRA     (IY+0),C           ; Undocumented
        SRA     (IY+0),D           ; Undocumented
        SRA     (IY+0),E           ; Undocumented
        SRA     (IY+0),H           ; Undocumented
        SRA     (IY+0),L           ; Undocumented
        SRA     (IY+0),A           ; Undocumented

        SRL     (IY+0),B           ; Undocumented
        SRL     (IY+0),C           ; Undocumented
        SRL     (IY+0),D           ; Undocumented
        SRL     (IY+0),E           ; Undocumented
        SRL     (IY+0),H           ; Undocumented
        SRL     (IY+0),L           ; Undocumented
        SRL     (IY+0),A           ; Undocumented

        SLL     (IY+0),B           ; Undocumented
        SLL     (IY+0),C           ; Undocumented
        SLL     (IY+0),D           ; Undocumented
        SLL     (IY+0),E           ; Undocumented
        SLL     (IY+0),H           ; Undocumented
        SLL     (IY+0),L           ; Undocumented
        SLL     (IY+0)             ; Undocumented
        SLL     (IY)               ; +0 displacement is default, when not specified
        SLL     (IY+0),A           ; Undocumented
endif

        BIT     0,(IY+0)
        BIT     0,(IY)             ; +0 displacement is default, when not specified
        BIT     1,(IY+0)
        BIT     2,(IY+0)
        BIT     3,(IY+0)
        BIT     4,(IY+0)
        BIT     5,(IY+0)
        BIT     6,(IY+0)
        BIT     7,(IY+0)

        RES     0,(IY+0)
        RES     0,(IY)             ; +0 displacement is default, when not specified
        RES     1,(IY+0)
        RES     2,(IY+0)
        RES     3,(IY+0)
        RES     4,(IY+0)
        RES     5,(IY+0)
        RES     6,(IY+0)
        RES     7,(IY+0)

        SET     0,(IY+0)
        SET     0,(IY)             ; +0 displacement is default, when not specified
        SET     1,(IY+0)
        SET     2,(IY+0)
        SET     3,(IY+0)
        SET     4,(IY+0)
        SET     5,(IY+0)
        SET     6,(IY+0)
        SET     7,(IY+0)

if !Z380
        BIT     0,(IY+0),B         ; Undocumented
        BIT     0,(IY),B           ; +0 displacement is default, when not specified
        BIT     0,(IY+0),C         ; Undocumented
        BIT     0,(IY+0),D         ; Undocumented
        BIT     0,(IY+0),E         ; Undocumented
        BIT     0,(IY+0),H         ; Undocumented
        BIT     0,(IY+0),L         ; Undocumented
        BIT     0,(IY+0),A         ; Undocumented

        BIT     1,(IY+0),B         ; Undocumented
        BIT     1,(IY+0),C         ; Undocumented
        BIT     1,(IY+0),D         ; Undocumented
        BIT     1,(IY+0),E         ; Undocumented
        BIT     1,(IY+0),H         ; Undocumented
        BIT     1,(IY+0),L         ; Undocumented
        BIT     1,(IY+0),A         ; Undocumented

        BIT     2,(IY+0),B         ; Undocumented
        BIT     2,(IY+0),C         ; Undocumented
        BIT     2,(IY+0),D         ; Undocumented
        BIT     2,(IY+0),E         ; Undocumented
        BIT     2,(IY+0),H         ; Undocumented
        BIT     2,(IY+0),L         ; Undocumented
        BIT     2,(IY+0),A         ; Undocumented

        BIT     3,(IY+0),B         ; Undocumented
        BIT     3,(IY+0),C         ; Undocumented
        BIT     3,(IY+0),D         ; Undocumented
        BIT     3,(IY+0),E         ; Undocumented
        BIT     3,(IY+0),H         ; Undocumented
        BIT     3,(IY+0),L         ; Undocumented
        BIT     3,(IY+0),A         ; Undocumented

        BIT     4,(IY+0),B         ; Undocumented
        BIT     4,(IY+0),C         ; Undocumented
        BIT     4,(IY+0),D         ; Undocumented
        BIT     4,(IY+0),E         ; Undocumented
        BIT     4,(IY+0),H         ; Undocumented
        BIT     4,(IY+0),L         ; Undocumented
        BIT     4,(IY+0),A         ; Undocumented

        BIT     5,(IY+0),B         ; Undocumented
        BIT     5,(IY+0),C         ; Undocumented
        BIT     5,(IY+0),D         ; Undocumented
        BIT     5,(IY+0),E         ; Undocumented
        BIT     5,(IY+0),H         ; Undocumented
        BIT     5,(IY+0),L         ; Undocumented
        BIT     5,(IY+0),A         ; Undocumented

        BIT     6,(IY+0),B         ; Undocumented
        BIT     6,(IY+0),C         ; Undocumented
        BIT     6,(IY+0),D         ; Undocumented
        BIT     6,(IY+0),E         ; Undocumented
        BIT     6,(IY+0),H         ; Undocumented
        BIT     6,(IY+0),L         ; Undocumented
        BIT     6,(IY+0),A         ; Undocumented

        BIT     7,(IY+0),B         ; Undocumented
        BIT     7,(IY+0),C         ; Undocumented
        BIT     7,(IY+0),D         ; Undocumented
        BIT     7,(IY+0),E         ; Undocumented
        BIT     7,(IY+0),H         ; Undocumented
        BIT     7,(IY+0),L         ; Undocumented
        BIT     7,(IY+0),A         ; Undocumented

        RES     0,(IY+0),B         ; Undocumented
        RES     0,(IY),B           ; +0 displacement is default, when not specified
        RES     0,(IY+0),C         ; Undocumented
        RES     0,(IY+0),D         ; Undocumented
        RES     0,(IY+0),E         ; Undocumented
        RES     0,(IY+0),H         ; Undocumented
        RES     0,(IY+0),L         ; Undocumented
        RES     0,(IY+0),A         ; Undocumented

        RES     1,(IY+0),B         ; Undocumented
        RES     1,(IY+0),C         ; Undocumented
        RES     1,(IY+0),D         ; Undocumented
        RES     1,(IY+0),E         ; Undocumented
        RES     1,(IY+0),H         ; Undocumented
        RES     1,(IY+0),L         ; Undocumented
        RES     1,(IY+0),A         ; Undocumented

        RES     2,(IY+0),B         ; Undocumented
        RES     2,(IY+0),C         ; Undocumented
        RES     2,(IY+0),D         ; Undocumented
        RES     2,(IY+0),E         ; Undocumented
        RES     2,(IY+0),H         ; Undocumented
        RES     2,(IY+0),L         ; Undocumented
        RES     2,(IY+0),A         ; Undocumented

        RES     3,(IY+0),B         ; Undocumented
        RES     3,(IY+0),C         ; Undocumented
        RES     3,(IY+0),D         ; Undocumented
        RES     3,(IY+0),E         ; Undocumented
        RES     3,(IY+0),H         ; Undocumented
        RES     3,(IY+0),L         ; Undocumented
        RES     3,(IY+0),A         ; Undocumented

        RES     4,(IY+0),B         ; Undocumented
        RES     4,(IY+0),C         ; Undocumented
        RES     4,(IY+0),D         ; Undocumented
        RES     4,(IY+0),E         ; Undocumented
        RES     4,(IY+0),H         ; Undocumented
        RES     4,(IY+0),L         ; Undocumented
        RES     4,(IY+0),A         ; Undocumented

        RES     5,(IY+0),B         ; Undocumented
        RES     5,(IY+0),C         ; Undocumented
        RES     5,(IY+0),D         ; Undocumented
        RES     5,(IY+0),E         ; Undocumented
        RES     5,(IY+0),H         ; Undocumented
        RES     5,(IY+0),L         ; Undocumented
        RES     5,(IY+0),A         ; Undocumented

        RES     6,(IY+0),B         ; Undocumented
        RES     6,(IY+0),C         ; Undocumented
        RES     6,(IY+0),D         ; Undocumented
        RES     6,(IY+0),E         ; Undocumented
        RES     6,(IY+0),H         ; Undocumented
        RES     6,(IY+0),L         ; Undocumented
        RES     6,(IY+0),A         ; Undocumented

        RES     7,(IY+0),B         ; Undocumented
        RES     7,(IY+0),C         ; Undocumented
        RES     7,(IY+0),D         ; Undocumented
        RES     7,(IY+0),E         ; Undocumented
        RES     7,(IY+0),H         ; Undocumented
        RES     7,(IY+0),L         ; Undocumented
        RES     7,(IY+0),A         ; Undocumented

        SET     0,(IY+0),B         ; Undocumented
        SET     0,(IY),B           ; +0 displacement is default, when not specified
        SET     0,(IY+0),C         ; Undocumented
        SET     0,(IY+0),D         ; Undocumented
        SET     0,(IY+0),E         ; Undocumented
        SET     0,(IY+0),H         ; Undocumented
        SET     0,(IY+0),L         ; Undocumented
        SET     0,(IY+0),A         ; Undocumented

        SET     1,(IY+0),B         ; Undocumented
        SET     1,(IY+0),C         ; Undocumented
        SET     1,(IY+0),D         ; Undocumented
        SET     1,(IY+0),E         ; Undocumented
        SET     1,(IY+0),H         ; Undocumented
        SET     1,(IY+0),L         ; Undocumented
        SET     1,(IY+0),A         ; Undocumented

        SET     2,(IY+0),B         ; Undocumented
        SET     2,(IY+0),C         ; Undocumented
        SET     2,(IY+0),D         ; Undocumented
        SET     2,(IY+0),E         ; Undocumented
        SET     2,(IY+0),H         ; Undocumented
        SET     2,(IY+0),L         ; Undocumented
        SET     2,(IY+0),A         ; Undocumented

        SET     3,(IY+0),B         ; Undocumented
        SET     3,(IY+0),C         ; Undocumented
        SET     3,(IY+0),D         ; Undocumented
        SET     3,(IY+0),E         ; Undocumented
        SET     3,(IY+0),H         ; Undocumented
        SET     3,(IY+0),L         ; Undocumented
        SET     3,(IY+0),A         ; Undocumented

        SET     4,(IY+0),B         ; Undocumented
        SET     4,(IY+0),C         ; Undocumented
        SET     4,(IY+0),D         ; Undocumented
        SET     4,(IY+0),E         ; Undocumented
        SET     4,(IY+0),H         ; Undocumented
        SET     4,(IY+0),L         ; Undocumented
        SET     4,(IY+0),A         ; Undocumented

        SET     5,(IY+0),B         ; Undocumented
        SET     5,(IY+0),C         ; Undocumented
        SET     5,(IY+0),D         ; Undocumented
        SET     5,(IY+0),E         ; Undocumented
        SET     5,(IY+0),H         ; Undocumented
        SET     5,(IY+0),L         ; Undocumented
        SET     5,(IY+0),A         ; Undocumented

        SET     6,(IY+0),B         ; Undocumented
        SET     6,(IY+0),C         ; Undocumented
        SET     6,(IY+0),D         ; Undocumented
        SET     6,(IY+0),E         ; Undocumented
        SET     6,(IY+0),H         ; Undocumented
        SET     6,(IY+0),L         ; Undocumented
        SET     6,(IY+0),A         ; Undocumented

        SET     7,(IY+0),B         ; Undocumented
        SET     7,(IY+0),C         ; Undocumented
        SET     7,(IY+0),D         ; Undocumented
        SET     7,(IY+0),E         ; Undocumented
        SET     7,(IY+0),H         ; Undocumented
        SET     7,(IY+0),L         ; Undocumented
        SET     7,(IY+0),A         ; Undocumented
endif

; $ is available to reflect assembler program counter
        JR      $                  ; Jump to itself (infinite loop), see https://jnz.dk/z80/jr_e.html
        JR      $+5                ; jump to 'shortlbl' label (JP instr is 3 bytes)
        JP      $
shortlbl
        LD      HL,$               ; load the address of the LD HL,x (21H) opcode

DEFC offset = 10
DEFC offsetN = -2

        POP     IY
        EX      (SP),IY
        PUSH    IY
        JP      (IY)
        LD      SP,IY

; multiple instruction mnemonics per line

        push bc : ld b,d : ld c,e : pop de
        or a : sbc hl,bc
        ld (IY+offset),l : ld (IY+offset+1),h
        ld b,b : ld c,c
        push ix : ex (sp),iy : pop ix
        ld c,(hl) : inc hl : ld b,(hl) : dec hl


if !Z380
; Meta instructions only valid for Z80, Z180 and ZXN
; (Z380 generates direct 16bit instruction opcodes)

        EX      BC,DE                           ; push bc : ld b,d : ld c,e : pop de
        EX      BC,HL                           ; push bc : ld b,h : ld c,l : pop hl
        EX      BC,IX                           ; push bc : ex (sp),ix : pop bc
        EX      BC,IY                           ; push bc : ex (sp),iy : pop bc
        EX      DE,IX                           ; push de : ex (sp),ix : pop de
        EX      DE,IY                           ; push de : ex (sp),iy : pop de
        EX      HL,IX                           ; push hl : ex (sp),ix : pop hl
        EX      HL,IY                           ; push hl : ex (sp),iy : pop hl
        EX      IX,IY                           ; push ix : ex (sp),iy : pop ix

        LD      BC,BC                           ; ld b,b : ld c,c
        LD      BC,DE                           ; ld b,d : ld c,e
        LD      BC,HL                           ; ld b,h : ld c,l
        LD      BC,IX                           ; ld b,ixh : ld c,ixl
        LD      BC,IY                           ; ld b,iyh : ld c,iyl

        LD      DE,BC                           ; ld d,b : ld e,c
        LD      DE,DE                           ; ld d,d : ld e,e
        LD      DE,HL                           ; ld d,h : ld e,l
        LD      DE,IX                           ; ld d,ixh : ld e,ixl
        LD      DE,IY                           ; ld d,iyh : ld e,iyl

        LD      HL,BC                           ; ld h,b : ld l,c
        LD      HL,DE                           ; ld h,d : ld l,e
        LD      HL,HL                           ; ld h,h : ld l,l
        LD      HL,IX                           ; push ix : pop hl
        LD      HL,IY                           ; push iy : pop hl

        LD      IX,BC                           ; ld ixh,b : ld ixl,c
        LD      IX,DE                           ; ld ixh,d : ld ixl,e
        LD      IX,HL                           ; push hl : pop ix
        LD      IX,IX                           ; ld ixh,ixh : ld ixl,ixl
        LD      IX,IY                           ; push iy : pop ix

        LD      IY,BC                           ; ld iyh,b : ld iyl,c
        LD      IY,DE                           ; ld iyh,d : ld iyl,e
        LD      IY,HL                           ; push hl : pop iy
        LD      IY,IX                           ; push ix : pop iy
        LD      IY,IY                           ; ld iyh,iyh : ld iyl,iyl

        LD      BC,(HL)                         ; ld c,(hl) : inc hl : ld b,(hl) : dec hl
        LD      DE,(HL)                         ; ld e,(hl) : inc hl : ld d,(hl) : dec hl

        LD      (HL),BC                         ; ld (hl),c : inc hl : ld (hl),b : dec hl
        LD      (HL),DE                         ; ld (hl),e : inc hl : ld (hl),d : dec hl

        LD      BC,(IX)                         ; no offset expression specified, 0 and 1 is embedded as offsets in instruction opcode
        LD      BC,(IX+offset)                  ; ld c,(ix+n) : ld b,(ix+n+1)
        LD      DE,(IX)
        LD      DE,(IX+offset)                  ; ld e,(ix+n) : ld d,(ix+n+1)                          
        LD      HL,(IX)
        LD      HL,(IX+offsetN)                 ; ld l,(ix+n) : ld h,(ix+n+1)

        LD      BC,(IY)                         ; no offset expression specified, 0 and 1 is embedded as offsets in instruction opcode
        LD      BC,(IY+offset)                  ; ld c,(iy+n) : ld b,(iy+n+1)
        LD      DE,(IY)
        LD      DE,(IY+offset)                  ; ld e,(iy+n) : ld d,(iy+n+1)                          
        LD      HL,(IY)
        LD      HL,(IY+offsetN)                 ; ld l,(iy+n) : ld h,(iy+n+1)

        LD      (IX),BC                         ; no offset expression specified, 0 and 1 is embedded as offsets in instruction opcode
        LD      (IX+offset),BC                  ; ld (ix+n),c : ld (ix+n+1),b
        LD      (IX),DE
        LD      (IX+offset),DE                  ; ld (ix+n),e : ld (ix+n+1),d
        LD      (IX),HL
        LD      (IX+offsetN),HL                 ; ld (ix+n),l : ld (ix+n+1),h

        LD      (IY),BC                         ; no offset expression specified, 0 and 1 is embedded as offsets in instruction opcode
        LD      (IY+offset),BC                  ; ld (IY+n),c : ld (IY+n+1),b
        LD      (IY),DE
        LD      (IY+offset),DE                  ; ld (IY+n),e : ld (IY+n+1),d
        LD      (IY),HL
        LD      (IY+offsetN),HL                 ; ld (IY+n),l : ld (IY+n+1),h

        RL      BC                              ; rl c : rl b
        RL      DE                              ; rl e : rl d
        RL      HL                              ; rl l : rl h

        RLW     BC                              ; same as RL, but syntax compatible with Z380
        RLW     DE
        RLW     HL
        RLW     (HL)                            ; rl (hl) : inc hl : rl (hl) : dec hl

        RR      BC                              ; rr b : rr c
        RR      DE                              ; rr d : rr e
        RR      HL                              ; rr h : rr l

        RRW     BC                              ; same as RR, but syntax compatible with Z380
        RRW     DE
        RRW     HL
        RRW     (HL)                            ; inc hl : rr (hl) : dec hl : rr (hl)

        SLA     BC                              ; sla c : rl b
        SLA     DE                              ; sla e : rl d
        SLA     HL                              ; add hl,hl

        SLAW    BC                              ; same as SLA, but syntax compatible with Z380
        SLAW    DE
        SLAW    HL
        SLAW    (HL)                            ; sla (hl) : inc hl : rl (hl) : dec hl

        SRA     BC                              ; sra b : rr c
        SRA     DE                              ; sra d : rr e
        SRA     HL                              ; sra h : rr l

        SRAW    BC                              ; same as SRA, but syntax compatible with Z380
        SRAW    DE
        SRAW    HL
        SRAW    (HL)                            ; inc hl : sra (hl) : dec hl : rr (hl)

        SRL     BC                              ; srl b : rr c
        SRL     DE                              ; srl d : rr e
        SRL     HL                              ; srl h : rr l

        SRLW    BC                              ; same as SRL, but syntax compatible with Z380
        SRLW    DE
        SRLW    HL
        SRLW    (HL)                            ; inc hl : srl (hl) : dec hl : rr (hl)

        SUB     HL,BC                           ; or a : sbc hl,bc
        SUB     HL,DE                           ; or a : sbc hl,de
        SUB     HL,HL                           ; or a : sbc hl,hl
        SUB     HL,SP                           ; or a : sbc hl,sp

        SUBW    HL,BC                           ; or a : sbc hl,bc
        SUBW    HL,DE                           ; or a : sbc hl,de
        SUBW    HL,HL                           ; or a : sbc hl,hl
        SUBW    HL,SP                           ; or a : sbc hl,sp

endif
