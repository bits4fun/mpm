
; *****************************************************************************************************
;
;  MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
;   MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
;   MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
;   MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
;   MMMM       MMMM     PPPP              MMMM       MMMM
;   MMMM       MMMM     PPPP              MMMM       MMMM
;  MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
;
;  Copyright (C) 1991-2024, Gunther Strube, hello@bits4fun.net
;
;  This file is part of Mpm.
;  Mpm is free software; you can redistribute it and/or modify
;  it under the terms of the GNU General Public License as published by the Free Software Foundation;
;  either version 2, or (at your option) any later version.
;  Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
;  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
;  See the GNU General Public License for more details.
;  You should have received a copy of the GNU General Public License along with Mpm;
;  see the file COPYING.  If not, write to the
;  Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
;
; -----------------------------------------------------------------------------------------------------
;  Mpm Directive & Expression reference, supported by Mpm V1.5 and newer
;
;  To see final output via listing file: mpm -vas directives.asm
;  (-v is verbose compile, can be omitted)
; *****************************************************************************************************


; -----------------------------------------------------------------------------------------------------
; optionally, specify a name for this source code. If not, the filename (without extension) will be used
; -----------------------------------------------------------------------------------------------------
module Directives

; -----------------------------------------------------------------------------------------------------
; Optionally, specify address origin is only used for first module (of several). If not specified, default 0
; -----------------------------------------------------------------------------------------------------
org $4000

; -----------------------------------------------------------------------------------------------------
; Labels might be defined with a leading '.' or a trailing ':' or nothing (not in conflict with mnemonics)
; -----------------------------------------------------------------------------------------------------
aPlace:
anotherPlace   db 0                             ; labels can be standalone and also followed by another directive or mnemonic
.here_too


; -----------------------------------------------------------------------------------------------------
; Integer constant definitions using DC, DEFC and EQU (32bit)
; -----------------------------------------------------------------------------------------------------
dc   DR_Get = $01 + -1
dc   ldist = here_too - aPlace                   ; distance between two labels (addresses)
defc Dm_Dev = $81+DR_Get, x = -16383, y = 'a'    ; constant definition is really just an expression that must evaluate
e_ident equ 16
biig equ 2**31
CR:  EQU 0Dh                                     ; ASCII CR  (Carriage Return, a.k.a. Ctrl-M)
LF:  EQU 0Ah                                     ; ASCII LF  (Line Feed a.k.a. Ctrl-J)


; -----------------------------------------------------------------------------------------------------
; Integer storage constants using 'db', 'byte' and 'defb' directive (8bit)
; -----------------------------------------------------------------------------------------------------
db   ldist                                      ; an example of a length byte (between two labels)
db   y, DR_Get, Dm_Dev, e_ident*2               ; constant definitions (and expressions) in 8bit range
db   0, 1, -1, -128, 127d                       ; decimal (base 10)
byte #0d, $0a, 0x20, 80h                        ; hexadecimal (base 16)
defb %11'10_0001, @11110000, 0b10010001, 1111b  ; binary (base 2)


; -----------------------------------------------------------------------------------------------------
; Integer storage constants using 'dw', 'word' and 'defw' directives (16bit)
; -----------------------------------------------------------------------------------------------------
dw   x, e_ident / 2                             ; constant definitions (and expressions) in 16bit range
dw   0, -1, -32768, 32767d                      ; decimal (base 10)
word #8000, $7fff
defw 0x100, 4001h                               ; hexadecimal (base 16)
defw %11'10_0001'11110001, 0b1001000111110001   ; binary (base 2)
defw @10010001'11110001                         ; binary (base 2)


; -----------------------------------------------------------------------------------------------------
; Integer storage constants using 'dl', 'defl' and 'long' directives (32bit)
; -----------------------------------------------------------------------------------------------------
dl   biig-1
dl   999999d, -1                                ; decimal (base 10)
long #00000000, $77073096
defl 0xee0e612c, 990951bah                      ; hexadecimal (base 16)
defl %11'10_0001'11110001'1001000111110001      ; binary (base 2)
defl 0b11100001111100011001000111110001         ; binary (base 2)

; -----------------------------------------------------------------------------------------------------
; Z88 pointer storage directive using defp, dp
; -----------------------------------------------------------------------------------------------------
defp $4000,$3f                                  ; offset, bank
dp   $8100,$7f                                  ; offset, bank


; -----------------------------------------------------------------------------------------------------
; Expressions (using decimal notation as example): Arithmetic operators
; -----------------------------------------------------------------------------------------------------
db 12+13                                        ; addition
db -10, 12 - 45, 10 + -9                        ; subtraction, unary minus
db 45 * 2                                       ; multiplication
db 256 / 8                                      ; division
db >1024                                        ; unary division by 256
db 256 % 8                                      ; modulus
db <65000                                       ; unary modulus by 256
db 2 ** 7                                       ; power
db 255 & 7                                      ; binary AND
db 128 | 64                                     ; binary OR
db 64 : 64                                      ; binary NOR
db 12 ^ 3                                       ; binary XOR
db ~32                                          ; unary Binary NOT
db 2 << 4                                       ; binary left shift
db 128 >> 4                                     ; binary right shift
db `12+13                                       ; constant expression (remove any address relocation flags, if present)
db `forwsym0-forwsym2                           ; expression using forward-referenced labels

dc forwsym0 = 10, forwsym2 = 9

; -----------------------------------------------------------------------------------------------------
; Expressions (using decimal notation as example): Relational operators, returns true (1) or false (0)
; -----------------------------------------------------------------------------------------------------
db 12 = 13, 100 = 100                           ; equal to
db 12 <> 13, 100 <> 100                         ; not equal to
db 12 < 13, 100 < 100                           ; less than
db 13 > 12, 100 > 100, -1 > -2                  ; larger than
db 12 <= 13, 100 <= 100                         ; less than or equal to
db 12 >= 13, 100 >= 100                         ; larger than or equal to
db !(12 = 13)                                   ; not

; -----------------------------------------------------------------------------------------------------
; db behaves like dm/defm
; -----------------------------------------------------------------------------------------------------
db 'Good-bye, cruel world!',CR,LF               ; single-quote string and constant declarations (i8080 notation)
db 'Good-bye, cruel world!\r\n'                 ; use escape characters inside string
db "Hello word",0                               ; double-quoted string is also supported.

; -----------------------------------------------------------------------------------------------------
; Assembler functions, which can be part of expressions
; -----------------------------------------------------------------------------------------------------

; Current release version of Mpm (4 digits - max 9999)
; Formatted as:
;    x000 - major release version (1 - 9)
;    0y00 - minor release version (0 - 9)
;    00ii - incremental build release (00 - 99)
;
; Example:
;    1910 = v1.9.10
;
dw $MPMVERSION

; current assembler PC
dc here0 = $PC                                  ; assembler Program Counter may be specified as $PC
dw here0
dc here1 = $                                    ; assembler Program Counter may be also specified as $ (standard in most assemblers for relative offsets)
dw here1

dw $YEAR, $MONTH, $DAY                          ; date & time functions are fetched when Mpm tool is starting up
dw $HOUR, $MINUTE, $SECOND

dw $LINKADDR[aPlace]                            ; the final, linked address of a label (ORG + PC)
db $eval[ (((((0+1)+1)+1)+1)+1) ]               ; evaluate expression - typically used in macros


; -----------------------------------------------------------------------------------------------------
; Assembler directives
; -----------------------------------------------------------------------------------------------------

.reladdr

; ALIGN {1-16} directive
if $pc & 1
     align 4                                    ; current PC was at an odd address, align for a 32bit address
     dl $700f8001
else
     align 1
     dw $8001                                   ; align for word address
endif

; ASCII, ASCIIZ, DM, DMZ, DEFM, DEFMZ directives (all use same memory string parser)
ascii  "abc", 'd', $0c, $0a, $DAY               ; strings, and single bytes mixed in
asciiz "abc", 'e', $0c, $0a, %11'10_0001
dm "what","was","this"
dmz "and"
dm "Unix b".$91.$B4." ".$8D.$C9."ive"           ; using string concatanation syntax ( alternative to , )
defm 1,"Bmore",1,'B',"",'','xxx'                ; allow empty strings, also allow to use ' or " as delimeter
defm "no",'p'|$80, "ex af,af",'''|$80           ; special single char constant ''' also with expressions
defm """,'"',"'","y",$80                        ; special single char constant """, and using '' to wrap ", "" to wrap '
defmz "yet nothing"                             ; a null-terminator is appended

dm "\\ \r\n \t \'"                              ; using escape characters (1)
dm '\a \b \f \"'                                ; using escape characters (2)

; -----------------------------------------------------------------------------------------------------
; Insert binary data directive at $PC (no parsing)
binary "blob.bin"
incbin "blob.bin"


; -----------------------------------------------------------------------------------------------------
; Change CPU parser directive
CPU Z380
     calr reladdr

CPU I8080
     stax b

CPU ZXN
     mul d,e

CPU Z180
     mlt bc

CPU Z80
     add hl,bc


; -----------------------------------------------------------------------------------------------------
; ENUM, DEFGROUP directives
DEFGROUP
     sym_null, sym_dquote, sym_squote = 5, sym_semicolon, sym_comma, sym_fullstop
     sym_lparen, sym_lcurly, sym_rcurly, sym_rparen, sym_plus, sym_minus, sym_multiply, sym_divi
     sym_mod, sym_power, sym_assign, sym_strconq, sym_and, sym_or, sym_xor, sym_not
     sym_less, sym_greater, sym_constexpr, sym_newline, sym_lf

     sym_name, sym_number, sym_decmconst, sym_hexconst, sym_binconst, sym_charconst
     sym_lessequal, sym_greatequal, sym_notequal
     sym_negated, sym_nil
     sym_ifstatm, sym_elsestatm, sym_endifstatm
ENDDEF

ENUM
     aaa, bbb, ccc=15, ddd
     eee
ENDDEF

; let's write those enumerations out in code
db sym_null, sym_squote, sym_semicolon, sym_comma
db aaa, bbb, ccc, ddd


; -----------------------------------------------------------------------------------------------------
; DEFINE, UNDEFINE directives
if !__defsymbol__
     define __defsymbol__
endif
if __defsymbol__
     undefine __defsymbol__
endif

; LSTON, LSTOFF directives
lstoff                                          ; don't output to listing file, temporarily (directive itself is never output'ed)
; DEFSYM, UNDEF directives
if !__defsymbol__
     defsym __defsymbol__                       ; alias for define
endif
if __defsymbol__
     undef __defsymbol__                        ; alias for undefine
endif
lston                                           ; listing output re-enabled


; -----------------------------------------------------------------------------------------------------
; DEFS, DS directives (allocate space with 0 or defined value)
; DEFS <size> [,(<byte>)]
xdef sysvar_area_presv,sysvar_area_presv_end

;   42 bytes used

; Preserved clocks (26):
.sysvar_area_presv
.ubTimeBufferSelect   defs    1                 ; select time buffer A or B (was $2100A0)
.ubTIM0_A             defs    1                 ; buffer A :    centiseconds
.ubTIM1_A             defs    1                 ;               seconds
.ubTIM2_A             defs    3                 ;               minutes
.ubTIM0_B             defs    1                 ; buffer B :    centiseconds
.ubTIM1_B             defs    1                 ;               seconds
.ubTIM2_B             defs    3                 ;               minutes
.uwSysDateLow         defs    2                 ; 11 bytes buffer for system time
.uwSysDateMid         defs    2
.uwSysDateHigh        defs    2
.SysTimeBuffer        defs    5
.ulUptime             defs    4                 ; uptime (TIM1-4 since latest hard reset)

; Preserved panel (15):
.ubResetType          defs    1                 ; $00 hard / $FF soft
.ubTimeout            defs    1                 ; panel system parameters
.ubRepeat             defs    1                 ; "
.cKeyclick            defs    1                 ; "
.cSound               defs    1                 ; "
.ubBadSize            defs    1                 ; "
.cCountry             defs    1                 ; country code
.LockupPsw            defs    8                 ; password

; OZ update (1):
.ubUpdated            defs    1                 ; $00 no / $FF yes
.sysvar_area_presv_end

; use $ff and ascii space ($20) to fill space
defs 16 ($ff)
ds 8 ($20)


; -----------------------------------------------------------------------------------------------------
; DEFVARS, DV, VARAREA directives
defvars 0                                       ; Using DEFVARS to define a 'Module' structure (offsets from a base address)
     module_next         ds.p 1                 ; Pointer to next module
     module_mname        ds.p 1                 ; Pointer to string of module name
     module_startoffset  ds.l 1                 ; This module's start offset from start of code buffer (relative file pointer)
     module_origin       ds.w 1                 ; Address Origin of current machine code module during linking
     module_cfile        ds.p 1                 ; Pointer to current file record
     module_localroot    ds.p 1                 ; Pointer to root of local symbols
     module_mexpr        ds.p 1                 ; Pointer to expressions in this module
     module_jraddr       ds.p 1                 ; Pointer to header of list of JR PC addresses
     module_parsed       ds.b 1                 ; flags
     SIZEOF_module
enddef

; -----------------------------------------------------------------------------------------------------
; DEFVARS example used from RomUpdate, https://bitbucket.org/cambridge/z88/src/master/z88apps/romupdate/
defc banksize = 16384
defc RAM_pages = 17408/256                      ; total RAM DOR pages, 17K bytes continuous memory from upper 8K segment 0 ($2000 onwards)
defc buffer = $2000                             ; use segment 0 onwards as 16K buffer to load a complete 16K bank from file
defc ozstack = $1FFE                            ; top of OZ System Stack address for OZ ROM update...
defc linebuffer = buffer                        ; the line parsing buffer for config file 'romupdate.cfg' (use bank buffer)
defc vararea = buffer+banksize                  ; runtime variables just after 16K buffer

defc MAX_IDLENGTH = 64                          ; max. 64 bytes identifier length (config file)
defc SIZEOF_LINEBUFFER = banksize               ; load config file into bank buffer to speed up parsing

DV vararea
     dorcpy             ds.b 64                 ; a copy of bank file image DOR
     doroffset          ds.w 1                  ; the offset within the bank of the found application in a card
     dorbank            ds.b 1                  ; the bank number of the found application in a card
     nextdoroffset      ds.w 1                  ; the offset of the next DOR (the brother link of found application DOR)
     nextdorbank        ds.b 1                  ; the bank number of the next DOR (the brother link of found application DOR)
     appname            ds.w 1                  ; local pointer to null-terminated application/popdown DOR name of bank file to be updated
     eprbankfilename    ds.b 1                  ; "/", when an filename needs to found in a File Area
     bankfilename       ds.b 128                ; filename of bank file to be patched to card (fetched from romupdate.cfg)
     bankfilecrc        ds.l 1                  ; CRC of bank file (fetched from from romupdate.cfg)
     bankfiledor        ds.w 1                  ; offset pointer to DOR in bank file
     filename           ds.b 128                ; generic filename buffer
     presvdbanks        ds.b 4                  ; 4 byte array of bank numbers that are preserved as :RAM.-/bank.<bank number>
     presvdbankcrcs     ds.l 4                  ; array of CRCs for preserved banks (index matches preserved bank array)
     retry              ds.b 1                  ; a simple retry counter for Flash Card Erase & BlockWrite commands
     ident              ds.b 66                 ; buffer for current collected ident. (config file)
     lineptr            ds.w 1                  ; pointer to current byte in linebuffer (config file)
     nextline           ds.w 1                  ; pointer to beginning of next source line in buffer (config file)
     bufferend          ds.w 1                  ; pointer to end of buffer information, L-end (config file)
     sym                ds.b 1                  ; current symbol identifier of ident. (config file)
     longint            ds.l 1                  ; long integer variable (config file usage)
     cfgfilelineno      ds.w 1                  ; line number reference for syntax errors during file parsing

     oz_slot
     crd_slot           ds.b 1                  ; which slot is going to be updated / loaded
     flash_algorithm    ds.b 1                  ; algorithm to be used to blow banks

     total_ozbanks
     total_banks        ds.b 1                  ; no. of banks to update to slot
     parsed_ozbanks
     parsed_banks       ds.b 1                  ; no of bank entries parsed from config file

     actionflags        ds.b 1                  ; bit flags to identify various actions determined by user

     crdbanks
     ozbanks            ds.l 32                 ; reserve space for max. 32 OZ ROM banks (512K). Each entry has the following format:
                                                ; [three byte source file data] [destination bank in slot X for data]
     bank_loader
     ozbank_loader      ds.w 1                  ; address of routine, responsible for loading bank file into buffer
     slotozfiles        ds.b 1                  ; slot number of found OZ bank files, when loading from EPR.x file device
     end_vararea
ENDDEF

; -----------------------------------------------------------------------------------------------------
; LINE <nn> define explicit line latest number of external resource or reference,
; to be used for when an error is reported by the assembler for a line.
LINE 55


; -----------------------------------------------------------------------------------------------------
; END directive - stop pass 1 parsing after this directive
END

