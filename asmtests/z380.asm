; -------------------------------------------------------------------------------------------------
;
; MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
;  MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
;  MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
;  MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
;  MMMM       MMMM     PPPP              MMMM       MMMM
;  MMMM       MMMM     PPPP              MMMM       MMMM
; MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
;
;                       ZZZZZZZZZZZZZZ    3333333333      888888888888        000000000
;                     ZZZZZZZZZZZZZZ    33333333333333  8888888888888888    0000000000000
;                             ZZZZ                3333  8888        8888  0000         0000
;                           ZZZZ           333333333      888888888888    0000         0000
;                         ZZZZ                    3333  8888        8888  0000         0000
;                       ZZZZZZZZZZZZZZ  33333333333333  8888888888888888    0000000000000
;                     ZZZZZZZZZZZZZZ      3333333333      888888888888        000000000
;
;  Copyright (C) 1991-2024, Gunther Strube, hello@bits4fun.net
;
;  This file is part of Mpm.
;  Mpm is free software; you can redistribute it and/or modify
;  it under the terms of the GNU General Public License as published by the Free Software Foundation;
;  either version 2, or (at your option) any later version.
;  Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
;  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
;  See the GNU General Public License for more details.
;  You should have received a copy of the GNU General Public License along with Mpm;
;  see the file COPYING.  If not, write to the
;  Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
;
; -------------------------------------------------------------------------------------------------
;
; Z380 CPU Instruction Reference, supported by Mpm V2.0
;
;  To see all opcodes, generate listing files: mpm -mz380 -va z80.asm z180.asm z380.asm
;  To generate a binary: mpm -mz380 -oz380.bin -vab z80.asm z180.asm z380.asm
;  (-v is verbose compile, can be omitted)
; -------------------------------------------------------------------------------------------------

module z380ref

xref reladrz80

defc const = $fffe
defc const24bit = $7ffffe

hello:

; then the Z380 variations and additions

; Set Control Bit
    setc lck    ; (Lock Mode)
    setc lw     ; (Long Word Mode)
    setc xm     ; (Extended Mode)

; Reset Control Bit
    resc lck    ; (Lock Mode)
    resc lw     ; (Long Word Mode)

; Decode Directive (stand-alone instruction, but not used directly in Mpm)
;    ddir w      ; word mode
;    ddir ib,w   ; immediate byte, word mode
;    ddir iw,w   ; immediate word, word mode
;    ddir ib     ; immediate byte
;    ddir lw     ; long word mode
;    ddir ib,lw  ; immediate byte, long word mode
;    ddir iw,lw  ; immediate word, long word mode
;    ddir iw     ; immediate word

; Control Register instructions
    ldctl a,xsr
    ldctl a,dsr
    ldctl a,ysr
    ldctl dsr,a
    ldctl dsr,%1100'0010
    ldctl hl,sr
    ldctl.lw hl,sr                              ; optional long word mode applied
    ldctl sr,%1100'0000
    ldctl sr,a
    ldctl sr,hl
    ldctl.lw sr,hl                              ; optional long word mode applied
    ldctl xsr,%1100'0001
    ldctl xsr,a
    ldctl ysr,%1100'0011
    ldctl ysr,a

; Bank test
    btest

; extended address call (all variations w. conditional mnemonics)
    call.ib $7fc000
    call.ib nz,$7fc000
    call.ib z,$7fc000
    call.ib c,$7fc000
    call.ib nc,$7fc000
    call.ib po,$7fc000
    call.ib nv,$7fc000
    call.ib pe,$7fc000
    call.ib v,$7fc000
    call.ib p,$7fc000
    call.ib ns,$7fc000
    call.ib m,$7fc000
    call.ib s,$7fc000

    call.iw $80cc0000
    call.iw nz,$80cc0000
    call.iw z,$80cc0000
    call.iw c,$80cc0000
    call.iw nc,$80cc0000
    call.iw po,$80cc0000
    call.iw nv,$80cc0000
    call.iw pe,$80cc0000
    call.iw v,$80cc0000
    call.iw p,$80cc0000
    call.iw ns,$80cc0000
    call.iw m,$80cc0000
    call.iw s,$80cc0000

; extended jp (all variations w. conditional mnemonics)
    jp.ib $                                     ; jump to self
    jp.ib $7fc000
    jp.ib nz,$7fc000
    jp.ib z,$7fc000
    jp.ib c,$7fc000
    jp.ib nc,$7fc000
    jp.ib po,$7fc000
    jp.ib nv,$7fc000
    jp.ib pe,$7fc000
    jp.ib v,$7fc000
    jp.ib p,$7fc000
    jp.ib ns,$7fc000
    jp.ib m,$7fc000
    jp.ib s,$7fc000

    jp.iw $                                     ; jump to self
    jp.iw $80cc0002
    jp.iw nz,$80cc0002
    jp.iw z,$80cc0002
    jp.iw c,$80cc0002
    jp.iw nc,$80cc0002
    jp.iw po,$80cc0002
    jp.iw nv,$80cc0002
    jp.iw pe,$80cc0002
    jp.iw v,$80cc0002
    jp.iw p,$80cc0002
    jp.iw ns,$80cc0002
    jp.iw m,$80cc0002
    jp.iw s,$80cc0002


reladrz380:

; call relative
    calr reladrz380
    calr z,reladrz380
    calr nz,reladrz380
    calr c,reladrz380
    calr nc,reladrz380
    calr po,reladrz380
    calr pe,reladrz380
    calr p,reladrz380
    calr m,reladrz380

    calr.ib reladrz80                           ; 16bit relative CALL in other module
    calr.ib reladrz380
    calr.ib hello2                              ; test pass 2 re-evaluation

    calr.ib z,reladrz380
hello2:
    calr.ib nz,reladrz380
    calr.ib c,reladrz380
    calr.ib nc,reladrz380
    calr.ib po,reladrz380
    calr.ib pe,reladrz380
    calr.ib p,reladrz380
    calr.ib m,reladrz380

    calr.iw reladrz80                           ; 24bit PC-relative CALL in other module
    calr.iw reladrz380
    calr.iw hello3
    calr.iw z,reladrz380
hello3:
    calr.iw nz,reladrz380
    calr.iw c,reladrz380
    calr.iw nc,reladrz380
    calr.iw po,reladrz380
    calr.iw pe,reladrz380
    calr.iw p,reladrz380
    calr.iw m,reladrz380

; add immediate constant to SP
    add sp,$                                    ; address of instruction
    add sp,$1f00                                ; constant
    add sp,hello3-hello2                        ; expression
    add sp,reladrz80                            ; relocatable address in other module

    add.ib sp,$1fc000                           ; constant
    add.ib sp,hello3-hello2                     ; expression
    add.ib sp,reladrz80                         ; relocatable address in other module

    add.iw sp,$1f800001                         ; constant
    add.iw sp,hello3-hello2                     ; expression
    add.iw sp,reladrz80                         ; relocatable address in other module

; add or subtract to/from HL via indirect immediate address
    add hl,($1f00)
    add.ib hl,($1fc000)
    add.iw hl,($1f800001)

    sub hl,($1f00)
    sub.ib hl,($1fc000)
    sub.iw hl,($1f800001)

; subtract immediate constant from SP
    sub sp,$1f00                                ; constant
    sub sp,hello3-hello2                        ; expression
    sub sp,reladrz80                            ; relocatable address in other module

    sub.ib sp,$1fc000                           ; constant
    sub.ib sp,hello3-hello2                     ; expression
    sub.ib sp,reladrz80                         ; relocatable address in other module

    sub.iw sp,$1f800001                         ; constant
    sub.iw sp,hello3-hello2                     ; expression
    sub.iw sp,reladrz80                         ; relocatable address in other module


; addw [hl],src
    addw    bc
    addw.ib bc                                  ; .ib is ignored (same as above)
    addw    de
    addw    hl
    addw    ix
    addw    iy

    addw    (ix)                                ; default displacement is 0 (byte)
    addw.ib (ix - 1000)                         ; 16bit displacement
    addw.ib (ix)                                ; waste of 3 extra bytes (FD C3 + 1 byte extra for displacement), however valid.
    addw.iw (ix + $123456)                      ; 24bit displacement
    addw.iw (ix)                                ; waste of 4 extra bytes (FD C3 + 2 bytes extra for displacement), however valid.
    addw    (ix + 1)

    addw    (iy)                                ; default displacement is 0 (byte)
    addw.ib (iy - 1000)                         ; 16bit displacement
    addw.ib (iy)                                ; waste of 3 extra bytes (FD C3 + 1 byte extra for displacement), however valid.
    addw.iw (iy + $123456)                      ; 24bit displacement
    addw.iw (iy)                                ; waste of 4 extra bytes (FD C3 + 2 bytes extra for displacement), however valid.
    addw    (iy + 1)

    addw    $4000                               ; add constant to HL
    addw    reladrz80                           ; refer to external address (resolved during linking - it's a label)
    addw    reladrz380                          ; refer to local address (resolved during linking - it's a label)

    addw    hl,bc
    addw    hl,de
    addw    hl,hl
    addw    hl,ix
    addw    hl,iy
    addw    hl,(ix + 1)
    addw    hl,(iy + 1)
    addw    $8000

; adcw [hl],src
    adcw    bc
    adcw    de
    adcw    hl
    adcw    ix
    adcw    iy

    adcw    (ix)                                ; default displacement is 0 (byte)
    adcw.ib (ix - 1000)                         ; 16bit displacement
    adcw.ib (ix)                                ; waste of 3 extra bytes (FD C3 + 1 byte extra for displacement), however valid.
    adcw.iw (ix + $123456)                      ; 24bit displacement
    adcw.iw (ix)                                ; waste of 4 extra bytes (FD C3 + 2 bytes extra for displacement), however valid.
    adcw    (ix + 1)

    adcw    (iy)                                ; default displacement is 0 (byte)
    adcw.ib (iy - 1000)                         ; 16bit displacement
    adcw.ib (iy)                                ; waste of 3 extra bytes (FD C3 + 1 byte extra for displacement), however valid.
    adcw.iw (iy + $123456)                      ; 24bit displacement
    adcw.iw (iy)                                ; waste of 4 extra bytes (FD C3 + 2 bytes extra for displacement), however valid.
    adcw    (iy + 1)

    adcw    $4000                               ; add constant to HL
    adcw    reladrz80                           ; refer to external address (resolved during linking - it's a label)
    adcw    reladrz380                          ; refer to local address (resolved during linking - it's a label)

    adcw hl,bc
    adcw hl,de
    adcw hl,hl
    adcw hl,ix
    adcw hl,iy
    adcw hl,(ix + 1)
    adcw hl,(iy + 1)
    adcw $8000

; subw [hl],src
    subw bc
    subw de
    subw hl
    subw sp                                     ; meta instruction (compatibility with Z80):  OR A : SBC HL,SP
    subw ix
    subw iy

    subw    (ix)                                ; default displacement is 0 (byte)
    subw.ib (ix - 1000)                         ; 16bit displacement
    subw.ib (ix)                                ; waste of 3 extra bytes (FD C3 + 1 byte extra for displacement), however valid.
    subw.iw (ix + $123456)                      ; 24bit displacement
    subw.iw (ix)                                ; waste of 4 extra bytes (FD C3 + 2 bytes extra for displacement), however valid.
    subw    (ix + 1)

    subw    (iy)                                ; default displacement is 0 (byte)
    subw.ib (iy - 1000)                         ; 16bit displacement
    subw.ib (iy)                                ; waste of 3 extra bytes (FD C3 + 1 byte extra for displacement), however valid.
    subw.iw (iy + $123456)                      ; 24bit displacement
    subw.iw (iy)                                ; waste of 4 extra bytes (FD C3 + 2 bytes extra for displacement), however valid.
    subw    (iy + 1)

    subw $4000                                  ; add constant to HL
    subw reladrz80                              ; refer to external address (resolved during linking - it's a label)
    subw reladrz380                             ; refer to local address (resolved during linking - it's a label)

    subw hl,bc
    subw hl,de
    subw hl,hl
    subw hl,sp                                  ; meta instruction (compatibility with Z80):  OR A : SBC HL,SP
    subw hl,ix
    subw hl,iy
    subw hl,(ix + 1)
    subw hl,(iy + 1)
    subw $8000

; sbcw [hl],src
    sbcw bc
    sbcw de
    sbcw hl
    sbcw ix
    sbcw iy

    sbcw    (ix)                                ; default displacement is 0 (byte)
    sbcw.ib (ix - 1000)                         ; 16bit displacement
    sbcw.ib (ix)                                ; waste of 3 extra bytes (FD C3 + 1 byte extra for displacement), however valid.
    sbcw.iw (ix + $123456)                      ; 24bit displacement
    sbcw.iw (ix)                                ; waste of 4 extra bytes (FD C3 + 2 bytes extra for displacement), however valid.
    sbcw    (ix + 1)

    sbcw    (iy)                                ; default displacement is 0 (byte)
    sbcw.ib (iy - 1000)                         ; 16bit displacement
    sbcw.ib (iy)                                ; waste of 3 extra bytes (FD C3 + 1 byte extra for displacement), however valid.
    sbcw.iw (iy + $123456)                      ; 24bit displacement
    sbcw.iw (iy)                                ; waste of 4 extra bytes (FD C3 + 2 bytes extra for displacement), however valid.
    sbcw    (iy + 1)

    sbcw $4000                                  ; add constant to HL
    sbcw reladrz80                              ; refer to external address (resolved during linking - it's a label)
    sbcw reladrz380                             ; refer to local address (resolved during linking - it's a label)

    sbcw hl,bc
    sbcw hl,de
    sbcw hl,hl
    sbcw hl,ix
    sbcw hl,iy
    sbcw hl,(ix + 1)
    sbcw hl,(iy + 1)
    sbcw $8000

; andw [hl],src
    andw bc
    andw de
    andw hl
    andw ix
    andw iy

    andw    (ix)                                ; default displacement is 0 (byte)
    andw.ib (ix - 1000)                         ; 16bit displacement
    andw.ib (ix)                                ; waste of 3 extra bytes (FD C3 + 1 byte extra for displacement), however valid.
    andw.iw (ix + $123456)                      ; 24bit displacement
    andw.iw (ix)                                ; waste of 4 extra bytes (FD C3 + 2 bytes extra for displacement), however valid.
    andw    (ix + 1)

    andw    (iy)                                ; default displacement is 0 (byte)
    andw.ib (iy - 1000)                         ; 16bit displacement
    andw.ib (iy)                                ; waste of 3 extra bytes (FD C3 + 1 byte extra for displacement), however valid.
    andw.iw (iy + $123456)                      ; 24bit displacement
    andw.iw (iy)                                ; waste of 4 extra bytes (FD C3 + 2 bytes extra for displacement), however valid.
    andw    (iy + 1)

    andw $4000                                  ; add constant to HL
    andw reladrz80                              ; refer to external address (resolved during linking - it's a label)
    andw reladrz380                             ; refer to local address (resolved during linking - it's a label)

    andw hl,bc
    andw hl,de
    andw hl,hl
    andw hl,ix
    andw hl,iy
    andw hl,(ix + 1)
    andw hl,(iy + 1)
    andw $8000

; xorw [hl],src
    xorw bc
    xorw de
    xorw hl
    xorw ix
    xorw iy

    xorw    (ix)                                ; default displacement is 0 (byte)
    xorw.ib (ix - 1000)                         ; 16bit displacement
    xorw.ib (ix)                                ; waste of 3 extra bytes (FD C3 + 1 byte extra for displacement), however valid.
    xorw.iw (ix + $123456)                      ; 24bit displacement
    xorw.iw (ix)                                ; waste of 4 extra bytes (FD C3 + 2 bytes extra for displacement), however valid.
    xorw    (ix + 1)

    xorw    (iy)                                ; default displacement is 0 (byte)
    xorw.ib (iy - 1000)                         ; 16bit displacement
    xorw.ib (iy)                                ; waste of 3 extra bytes (FD C3 + 1 byte extra for displacement), however valid.
    xorw.iw (iy + $123456)                      ; 24bit displacement
    xorw.iw (iy)                                ; waste of 4 extra bytes (FD C3 + 2 bytes extra for displacement), however valid.
    xorw    (iy + 1)

    xorw $4000                                  ; add constant to HL
    xorw reladrz80                              ; refer to external address (resolved during linking - it's a label)
    xorw reladrz380                             ; refer to local address (resolved during linking - it's a label)

    xorw hl,bc
    xorw hl,de
    xorw hl,hl
    xorw hl,ix
    xorw hl,iy
    xorw hl,(ix + 1)
    xorw hl,(iy + 1)
    xorw $8000

; orw [hl],src
    orw bc
    orw de
    orw hl
    orw ix
    orw iy

    orw    (ix)                                 ; default displacement is 0 (byte)
    orw.ib (ix - 1000)                          ; 16bit displacement
    orw.ib (ix)                                 ; waste of 3 extra bytes (FD C3 + 1 byte extra for displacement), however valid.
    orw.iw (ix + $123456)                       ; 24bit displacement
    orw.iw (ix)                                 ; waste of 4 extra bytes (FD C3 + 2 bytes extra for displacement), however valid.
    orw    (ix + 1)

    orw    (iy)                                 ; default displacement is 0 (byte)
    orw.ib (iy - 1000)                          ; 16bit displacement
    orw.ib (iy)                                 ; waste of 3 extra bytes (FD C3 + 1 byte extra for displacement), however valid.
    orw.iw (iy + $123456)                       ; 24bit displacement
    orw.iw (iy)                                 ; waste of 4 extra bytes (FD C3 + 2 bytes extra for displacement), however valid.
    orw    (iy + 1)

    orw $4000                                   ; add constant to HL
    orw reladrz80                               ; refer to external address (resolved during linking - it's a label)
    orw reladrz380                              ; refer to local address (resolved during linking - it's a label)

    orw hl,bc
    orw hl,de
    orw hl,hl
    orw hl,ix
    orw hl,iy
    orw hl,(ix + 1)
    orw hl,(iy + 1)
    orw $8000

; cpw [hl],src
    cpw bc
    cpw de
    cpw hl
    cpw ix
    cpw iy


    cpw    (ix)                                 ; default displacement is 0 (byte)
    cpw.ib (ix - 1000)                          ; 16bit displacement
    cpw.ib (ix)                                 ; waste of 3 extra bytes (FD C3 + 1 byte extra for displacement), however valid.
    cpw.iw (ix + $123456)                       ; 24bit displacement
    cpw.iw (ix)                                 ; waste of 4 extra bytes (FD C3 + 2 bytes extra for displacement), however valid.
    cpw    (ix + 1)

    cpw    (iy)                                 ; default displacement is 0 (byte)
    cpw.ib (iy - 1000)                          ; 16bit displacement
    cpw.ib (iy)                                 ; waste of 3 extra bytes (FD C3 + 1 byte extra for displacement), however valid.
    cpw.iw (iy + $123456)                       ; 24bit displacement
    cpw.iw (iy)                                 ; waste of 4 extra bytes (FD C3 + 2 bytes extra for displacement), however valid.
    cpw    (iy + 1)

    cpw $4000                                   ; add constant to HL
    cpw reladrz80                               ; refer to external address (resolved during linking - it's a label)
    cpw reladrz380                              ; refer to local address (resolved during linking - it's a label)

    cpw hl,bc
    cpw hl,de
    cpw hl,hl
    cpw hl,ix
    cpw hl,iy
    cpw hl,(ix + 1)
    cpw hl,(iy + 1)
    cpw $8000

; relative jump, 16bit relative immediate
    jr.ib $                                     ; jump to itself
    jr.ib reladrz80                             ; relative jump to other module
    jr.ib reladrz380                            ; backward referenced (local)
    jr.ib hello2                                ; forward referenced
    jr.ib z,reladrz380
    jr.ib nz,reladrz380
    jr.ib c,reladrz380
    jr.ib nc,reladrz380

; relative jump, 24bit relative immediate
    jr.iw $                                     ; jump to itself
    jr.iw reladrz80                             ; relative jump to other module
    jr.iw reladrz380                            ; backward referenced (local)
    jr.iw hello2                                ; forward referenced
    jr.iw z,reladrz380
    jr.iw nz,reladrz380
    jr.iw c,reladrz380
    jr.iw nc,reladrz380

; relative jump / decrease B, 16bit relative immediate
    djnz.ib reladrz80                           ; relative jump to other module
    djnz.ib reladrz380                          ; backward referenced (local)
    djnz.ib hello4                              ; forward referenced

; relative jump / decrease B, 24bit relative immediate
    djnz.iw reladrz80                           ; relative jump to other module
    djnz.iw reladrz380                          ; backward referenced (local)
    djnz.iw hello4                              ; forward referenced

; Complement HL Register
    cplw
    cplw hl

hello4:

; Decrease 16bit registers (alias for original DEC rr)
    decw bc
    decw de
    decw hl
    decw sp
    decw ix
    decw iy

; Disable interrupts
    di 1

; Enable interrupts
    ei 1

; Interrupt mode 3 (0-2 are Z80 only)
    im 3

; Increase 16bit registers (alias for original INC rr)
    incw bc
    incw de
    incw hl
    incw sp
    incw ix
    incw iy

; Divide Unsigned (Word)
    divuw    bc                                 ; without HL specified
    divuw.ib bc                                 ; .ib is ignored (same as above)
    divuw    de
    divuw    hl
    divuw    ix
    divuw    iy
    divuw    (ix)                               ; default displacement is 0
    divuw.ib (ix - 1000)                        ; 16bit displacement
    divuw.ib (ix)                               ; waste of 3 extra bytes (DD C3 + 1 byte extra for displacement), however valid.
    divuw.iw (ix + $123456)                     ; 24bit displacement
    divuw.iw (ix)                               ; waste of 4 extra bytes (FD C3 + 2 bytes extra for displacement), however valid.
    divuw    (ix + 1)
    divuw    (iy)
    divuw    (iy + 1)
    divuw    $4000                              ; divide with constant

    divuw    hl,bc                              ; with HL specified
    divuw.ib hl,bc                              ; .ib is ignored (same as above)
    divuw    hl,de
    divuw    hl,hl
    divuw    hl,ix
    divuw    hl,iy
    divuw.ib hl,(ix - 1000)                     ; 16bit displacement
    divuw.ib hl,(ix)                            ; waste of 3 extra bytes (DD C3 + 1 byte extra for displacement), however valid.
    divuw.iw hl,(ix + $123456)                  ; 24bit displacement
    divuw.iw hl,(ix)                            ; waste of 4 extra bytes (FD C3 + 2 bytes extra for displacement), however valid.
    divuw    hl,(iy)
    divuw    hl,(ix + 1)
    divuw    hl,(iy + 1)
    divuw    hl,$8000

; Multiply Unsigned (Word)
    multuw bc                                   ; without HL specified
    multuw de
    multuw hl
    multuw ix
    multuw iy
    multuw (ix)                                 ; default displacement is 0
    multuw (ix + 1)
    multuw (iy)
    multuw (iy + 1)
    multuw $4011                                ; divide with constant

    multuw hl,bc                                ; with HL specified
    multuw hl,de
    multuw hl,hl
    multuw hl,ix
    multuw hl,iy
    multuw hl,(ix + 1)
    multuw hl,(iy + 1)
    multuw hl,$8011

; Multiply (Word)
    multw bc                                    ; without HL specified
    multw de
    multw hl
    multw ix
    multw iy
    multw (ix)                                  ; default displacement is 0
    multw (ix + 1)
    multw (iy)
    multw (iy + 1)
    multw $4011                                 ; divide with constant

    multw hl,bc                                 ; with HL specified
    multw hl,de
    multw hl,hl
    multw hl,ix
    multw hl,iy
    multw hl,(ix + 1)
    multw hl,(iy + 1)
    multw hl,$8011


; Arithmetic / Logic 8bit Indirect group with immediate 16bit or 24bit offsets
    adc.ib a,(ix)                               ; +0 displacement is default, when not specified (waste of instruction opcode space, but valid)
    adc.ib a,(ix - 2)
    adc.ib (iy)                                 ; +0 displacement is default, when not specified (waste of instruction opcode space, but valid)
    adc.ib (iy - 2)
    adc.iw a,(ix)
    adc.iw a,(ix - 2)
    adc.iw (iy)
    adc.iw (iy - 2)

    add.ib a,(ix)                               ; +0 displacement is default, when not specified (waste of instruction opcode space, but valid)
    add.ib a,(ix - 2)
    add.ib (iy)                                 ; +0 displacement is default, when not specified (waste of instruction opcode space, but valid)
    add.ib (iy - 2)
    add.iw a,(ix)
    add.iw a,(ix - 2)
    add.iw (iy)
    add.iw (iy - 2)

    sub.ib a,(ix)                               ; +0 displacement is default, when not specified (waste of instruction opcode space, but valid)
    sub.ib a,(ix - 2)
    sub.ib (iy)                                 ; +0 displacement is default, when not specified (waste of instruction opcode space, but valid)
    sub.ib (iy - 2)
    sub.iw a,(ix)
    sub.iw a,(ix - 2)
    sub.iw (iy)
    sub.iw (iy - 2)

    sbc.ib (ix)                                 ; +0 displacement is default, when not specified (waste of instruction opcode space, but valid)
    sbc.ib a,(ix - 2)
    sbc.ib (iy)                                 ; +0 displacement is default, when not specified (waste of instruction opcode space, but valid)
    sbc.ib (iy - 2)
    sbc.iw (ix)
    sbc.iw (ix - 2)
    sbc.iw (iy)
    sbc.iw (iy - 2)

    and.ib (ix)                                 ; +0 displacement is default, when not specified (waste of instruction opcode space, but valid)
    and.ib a,(ix - 2)
    and.ib (iy)                                 ; +0 displacement is default, when not specified (waste of instruction opcode space, but valid)
    and.ib (iy - 2)
    and.iw (ix)
    and.iw (ix - 2)
    and.iw (iy)
    and.iw (iy - 2)

    xor.ib (ix)
    xor.ib a,(ix - 2)
    xor.ib (iy)
    xor.ib (iy - 2)
    xor.iw (ix)
    xor.iw (ix - 2)
    xor.iw (iy)
    xor.iw (iy - 2)

    or.ib (ix)
    or.ib a,(ix - 2)
    or.ib (iy)
    or.ib (iy - 2)
    or.iw (ix)
    or.iw (ix - 2)
    or.iw (iy)
    or.iw (iy - 2)

    cp.ib (ix)
    cp.ib a,(ix - 2)
    cp.ib (iy)
    cp.ib (iy - 2)
    cp.iw (ix)
    cp.iw (ix - 2)
    cp.iw (iy)
    cp.iw (iy - 2)

; Increase / Decrease 8bit indirect with immediate 16bit or 24bit offsets
    inc.ib (ix)                ; +0 displacement is default, when not specified (waste of instruction opcode space, but valid)
    inc.ib (ix - 2)
    inc.ib (iy)                ; +0 displacement is default, when not specified (waste of instruction opcode space, but valid)
    inc.ib (iy - 2)
    inc.iw (ix)
    inc.iw (ix - 2)
    inc.iw (iy)
    inc.iw (iy - 2)

    dec.ib (ix)                ; +0 displacement is default, when not specified (waste of instruction opcode space, but valid)
    dec.ib (ix - 2)
    dec.ib (iy)                ; +0 displacement is default, when not specified (waste of instruction opcode space, but valid)
    dec.ib (iy - 2)
    dec.iw (ix)
    dec.iw (ix - 2)
    dec.iw (iy)
    dec.iw (iy - 2)


; Rotate / Shift 8bit Indirect with immediate 16bit or 24bit offsets
    rlc.ib  (ix+$1234)
    rlc.ib  (ix)               ; +0 displacement is default, when not specified
    rrc.ib  (ix+$1234)
    rrc.ib  (ix)               ; +0 displacement is default, when not specified
    rl.ib   (ix+$1234)
    rl.ib   (ix)               ; +0 displacement is default, when not specified
    rr.ib   (ix+$1234)
    rr.ib   (ix)               ; +0 displacement is default, when not specified
    sla.ib  (ix+$1234)
    sla.ib  (ix)               ; +0 displacement is default, when not specified
    sra.ib  (ix+$1234)
    sra.ib  (ix)               ; +0 displacement is default, when not specified
    srl.ib  (ix+$1234)
    srl.ib  (ix)               ; +0 displacement is default, when not specified
    rlc.ib  (iy+$1234)
    rlc.ib  (iy)               ; +0 displacement is default, when not specified
    rrc.ib  (iy+$1234)
    rrc.ib  (iy)               ; +0 displacement is default, when not specified
    rl.ib   (iy+$1234)
    rl.ib   (iy)               ; +0 displacement is default, when not specified
    rr.ib   (iy+$1234)
    rr.ib   (iy)               ; +0 displacement is default, when not specified
    sla.ib  (iy+$1234)
    sla.ib  (iy)               ; +0 displacement is default, when not specified
    sra.ib  (iy+$1234)
    sra.ib  (iy)               ; +0 displacement is default, when not specified
    srl.ib  (iy+$1234)
    srl.ib  (iy)               ; +0 displacement is default, when not specified

    rlc.iw  (ix + $123456)
    rlc.iw  (ix)               ;  +0 displacement is default, when not specified
    rrc.iw  (ix + $123456)
    rrc.iw  (ix)               ;  +0 displacement is default, when not specified
    rl.iw   (ix + $123456)
    rl.iw   (ix)               ;  +0 displacement is default, when not specified
    rr.iw   (ix + $123456)
    rr.iw   (ix)               ;  +0 displacement is default, when not specified
    sla.iw  (ix + $123456)
    sla.iw  (ix)               ;  +0 displacement is default, when not specified
    sra.iw  (ix + $123456)
    sra.iw  (ix)               ;  +0 displacement is default, when not specified
    srl.iw  (ix + $123456)
    srl.iw  (ix)               ;  +0 displacement is default, when not specified
    rlc.iw  (iy + $123456)
    rlc.iw  (iy)               ;  +0 displacement is default, when not specified
    rrc.iw  (iy + $123456)
    rrc.iw  (iy)               ;  +0 displacement is default, when not specified
    rl.iw   (iy + $123456)
    rl.iw   (iy)               ;  +0 displacement is default, when not specified
    rr.iw   (iy + $123456)
    rr.iw   (iy)               ;  +0 displacement is default, when not specified
    sla.iw  (iy + $123456)
    sla.iw  (iy)               ;  +0 displacement is default, when not specified
    sra.iw  (iy + $123456)
    sra.iw  (iy)               ;  +0 displacement is default, when not specified
    srl.iw  (iy + $123456)
    srl.iw  (iy)               ;  +0 displacement is default, when not specified

; Rotate Left Circular Word
    rlcw bc
    rlcw de
    rlcw (hl)
    rlcw hl
    rlcw ix
    rlcw iy
    rlcw (ix)                                   ; implicit 0 offset
    rlcw (ix + $12)
    rlcw (iy)
    rlcw (iy + $12)
    rlcw.ib (ix)                                ; implicit 0 offset, waste of 3 extra bytes (DD C3 + 1 byte extra for displacement), however valid.
    rlcw.ib (ix - 1000)                         ; 16bit displacement
    rlcw.iw (ix)                                ; implicit 0 offset, waste of 4 extra bytes (FD C3 + 2 bytes extra for displacement), however valid.
    rlcw.iw (ix + $123456)                      ; 24bit displacement
    rlcw.ib (iy)                                ; implicit 0 offset, waste of 3 extra bytes (DD C3 + 1 byte extra for displacement), however valid.
    rlcw.ib (iy - 1000)                         ; 16bit displacement
    rlcw.iw (iy)                                ; implicit 0 offset, waste of 4 extra bytes (FD C3 + 2 bytes extra for displacement), however valid.
    rlcw.iw (iy + $123456)                      ; 24bit displacement

; Rotate Right Circular Word
    rrcw bc
    rrcw de
    rrcw (hl)
    rrcw hl
    rrcw ix
    rrcw iy
    rrcw (ix)                                   ; implicit 0 offset
    rrcw (ix + $12)
    rrcw (iy)
    rrcw (iy + $12)
    rrcw.ib (ix)                                ; implicit 0 offset, waste of 3 extra bytes (DD C3 + 1 byte extra for displacement), however valid.
    rrcw.ib (ix - 1000)                         ; 16bit displacement
    rrcw.iw (ix)                                ; implicit 0 offset, waste of 4 extra bytes (FD C3 + 2 bytes extra for displacement), however valid.
    rrcw.iw (ix + $123456)                      ; 24bit displacement
    rrcw.ib (iy)                                ; implicit 0 offset, waste of 3 extra bytes (DD C3 + 1 byte extra for displacement), however valid.
    rrcw.ib (iy - 1000)                         ; 16bit displacement
    rrcw.iw (iy)                                ; implicit 0 offset, waste of 4 extra bytes (FD C3 + 2 bytes extra for displacement), however valid.
    rrcw.iw (iy + $123456)                      ; 24bit displacement

; Rotate Left Word
    rlw bc
    rlw de
    rlw (hl)
    rlw hl
    rlw ix
    rlw iy
    rlw (ix)                                   ; implicit 0 offset
    rlw (ix + $12)
    rlw (iy)
    rlw (iy + $12)
    rlw.ib (ix)                                ; implicit 0 offset, waste of 3 extra bytes (DD C3 + 1 byte extra for displacement), however valid.
    rlw.ib (ix - 1000)                         ; 16bit displacement
    rlw.iw (ix)                                ; implicit 0 offset, waste of 4 extra bytes (FD C3 + 2 bytes extra for displacement), however valid.
    rlw.iw (ix + $123456)                      ; 24bit displacement
    rlw.ib (iy)                                ; implicit 0 offset, waste of 3 extra bytes (DD C3 + 1 byte extra for displacement), however valid.
    rlw.ib (iy - 1000)                         ; 16bit displacement
    rlw.iw (iy)                                ; implicit 0 offset, waste of 4 extra bytes (FD C3 + 2 bytes extra for displacement), however valid.
    rlw.iw (iy + $123456)                      ; 24bit displacement

; Rotate Right Word
    rrw bc
    rrw de
    rrw (hl)
    rrw hl
    rrw ix
    rrw iy
    rrw (ix)                                   ; implicit 0 offset
    rrw (ix + $12)
    rrw (iy)
    rrw (iy + $12)
    rrw.ib (ix)                                ; implicit 0 offset, waste of 3 extra bytes (DD C3 + 1 byte extra for displacement), however valid.
    rrw.ib (ix - 1000)                         ; 16bit displacement
    rrw.iw (ix)                                ; implicit 0 offset, waste of 4 extra bytes (FD C3 + 2 bytes extra for displacement), however valid.
    rrw.iw (ix + $123456)                      ; 24bit displacement
    rrw.ib (iy)                                ; implicit 0 offset, waste of 3 extra bytes (DD C3 + 1 byte extra for displacement), however valid.
    rrw.ib (iy - 1000)                         ; 16bit displacement
    rrw.iw (iy)                                ; implicit 0 offset, waste of 4 extra bytes (FD C3 + 2 bytes extra for displacement), however valid.
    rrw.iw (iy + $123456)                      ; 24bit displacement

; Shift Left Arithmetic Word
    slaw bc
    slaw de
    slaw (hl)
    slaw hl
    slaw ix
    slaw iy
    slaw (ix)                                   ; implicit 0 offset
    slaw (ix + $12)
    slaw (iy)
    slaw (iy + $12)
    slaw.ib (ix)                                ; implicit 0 offset, waste of 3 extra bytes (DD C3 + 1 byte extra for displacement), however valid.
    slaw.ib (ix - 1000)                         ; 16bit displacement
    slaw.iw (ix)                                ; implicit 0 offset, waste of 4 extra bytes (FD C3 + 2 bytes extra for displacement), however valid.
    slaw.iw (ix + $123456)                      ; 24bit displacement
    slaw.ib (iy)                                ; implicit 0 offset, waste of 3 extra bytes (DD C3 + 1 byte extra for displacement), however valid.
    slaw.ib (iy - 1000)                         ; 16bit displacement
    slaw.iw (iy)                                ; implicit 0 offset, waste of 4 extra bytes (FD C3 + 2 bytes extra for displacement), however valid.
    slaw.iw (iy + $123456)                      ; 24bit displacement

; Shift Right Arithmetic Word
    sraw bc
    sraw de
    sraw (hl)
    sraw hl
    sraw ix
    sraw iy
    sraw (ix)                                   ; implicit 0 offset
    sraw (ix + $12)
    sraw (iy)
    sraw (iy + $12)
    sraw.ib (ix)                                ; implicit 0 offset, waste of 3 extra bytes (DD C3 + 1 byte extra for displacement), however valid.
    sraw.ib (ix - 1000)                         ; 16bit displacement
    sraw.iw (ix)                                ; implicit 0 offset, waste of 4 extra bytes (FD C3 + 2 bytes extra for displacement), however valid.
    sraw.iw (ix + $123456)                      ; 24bit displacement
    sraw.ib (iy)                                ; implicit 0 offset, waste of 3 extra bytes (DD C3 + 1 byte extra for displacement), however valid.
    sraw.ib (iy - 1000)                         ; 16bit displacement
    sraw.iw (iy)                                ; implicit 0 offset, waste of 4 extra bytes (FD C3 + 2 bytes extra for displacement), however valid.
    sraw.iw (iy + $123456)                      ; 24bit displacement

; Shift Right Logical Word
    srlw bc
    srlw de
    srlw (hl)
    srlw hl
    srlw ix
    srlw iy
    srlw (ix)                                   ; implicit 0 offset
    srlw (ix + $12)
    srlw (iy)
    srlw (iy + $12)
    srlw.ib (ix)                                ; implicit 0 offset, waste of 3 extra bytes (DD C3 + 1 byte extra for displacement), however valid.
    srlw.ib (ix - 1000)                         ; 16bit displacement
    srlw.iw (ix)                                ; implicit 0 offset, waste of 4 extra bytes (FD C3 + 2 bytes extra for displacement), however valid.
    srlw.iw (ix + $123456)                      ; 24bit displacement
    srlw.ib (iy)                                ; implicit 0 offset, waste of 3 extra bytes (DD C3 + 1 byte extra for displacement), however valid.
    srlw.ib (iy - 1000)                         ; 16bit displacement
    srlw.iw (iy)                                ; implicit 0 offset, waste of 4 extra bytes (FD C3 + 2 bytes extra for displacement), however valid.
    srlw.iw (iy + $123456)                      ; 24bit displacement

; bit, res & set instructions with 16bit & 24bit immediate offset
    bit.ib  0,(ix+$1234)
    bit.ib  0,(ix)             ; +0 displacement is default, when not specified, waste of 3 extra bytes (DD C3 + 1 byte extra for displacement), however valid.
    bit.ib  1,(ix+$1234)
    bit.ib  2,(ix+$1234)
    bit.ib  3,(ix+$1234)
    bit.ib  4,(ix+$1234)
    bit.ib  5,(ix+$1234)
    bit.ib  6,(ix+$1234)
    bit.ib  7,(ix+$1234)

    res.ib  0,(ix+$1234)
    res.ib  0,(ix)             ; +0 displacement is default, when not specified
    res.ib  1,(ix+$1234)
    res.ib  2,(ix+$1234)
    res.ib  3,(ix+$1234)
    res.ib  4,(ix+$1234)
    res.ib  5,(ix+$1234)
    res.ib  6,(ix+$1234)
    res.ib  7,(ix+$1234)

    set.ib  0,(ix+$1234)
    set.ib  0,(ix)             ; +0 displacement is default, when not specified
    set.ib  1,(ix+$1234)
    set.ib  2,(ix+$1234)
    set.ib  3,(ix+$1234)
    set.ib  4,(ix+$1234)
    set.ib  5,(ix+$1234)
    set.ib  6,(ix+$1234)
    set.ib  7,(ix+$1234)

    bit.ib  0,(iy+$1234)
    bit.ib  0,(iy)             ; +0 displacement is default, when not specified
    bit.ib  1,(iy+$1234)
    bit.ib  2,(iy+$1234)
    bit.ib  3,(iy+$1234)
    bit.ib  4,(iy+$1234)
    bit.ib  5,(iy+$1234)
    bit.ib  6,(iy+$1234)
    bit.ib  7,(iy+$1234)

    res.ib  0,(iy+$1234)
    res.ib  0,(iy)             ; +0 displacement is default, when not specified
    res.ib  1,(iy+$1234)
    res.ib  2,(iy+$1234)
    res.ib  3,(iy+$1234)
    res.ib  4,(iy+$1234)
    res.ib  5,(iy+$1234)
    res.ib  6,(iy+$1234)
    res.ib  7,(iy+$1234)

    set.ib  0,(iy+$1234)
    set.ib  0,(iy)             ; +0 displacement is default, when not specified
    set.ib  1,(iy+$1234)
    set.ib  2,(iy+$1234)
    set.ib  3,(iy+$1234)
    set.ib  4,(iy+$1234)
    set.ib  5,(iy+$1234)
    set.ib  6,(iy+$1234)
    set.ib  7,(iy+$1234)

    bit.iw  0,(ix+$123456)
    bit.iw  0,(ix)             ; +0 displacement is default, when not specified
    bit.iw  1,(ix+$123456)
    bit.iw  2,(ix+$123456)
    bit.iw  3,(ix+$123456)
    bit.iw  4,(ix+$123456)
    bit.iw  5,(ix+$123456)
    bit.iw  6,(ix+$123456)
    bit.iw  7,(ix+$123456)

    res.iw  0,(ix+$123456)
    res.iw  0,(ix)             ; +0 displacement is default, when not specified
    res.iw  1,(ix+$123456)
    res.iw  2,(ix+$123456)
    res.iw  3,(ix+$123456)
    res.iw  4,(ix+$123456)
    res.iw  5,(ix+$123456)
    res.iw  6,(ix+$123456)
    res.iw  7,(ix+$123456)

    set.iw  0,(ix+$123456)
    set.iw  0,(ix)             ; +0 displacement is default, when not specified
    set.iw  1,(ix+$123456)
    set.iw  2,(ix+$123456)
    set.iw  3,(ix+$123456)
    set.iw  4,(ix+$123456)
    set.iw  5,(ix+$123456)
    set.iw  6,(ix+$123456)
    set.iw  7,(ix+$123456)

    bit.iw  0,(iy+$123456)
    bit.iw  0,(iy)             ; +0 displacement is default, when not specified
    bit.iw  1,(iy+$123456)
    bit.iw  2,(iy+$123456)
    bit.iw  3,(iy+$123456)
    bit.iw  4,(iy+$123456)
    bit.iw  5,(iy+$123456)
    bit.iw  6,(iy+$123456)
    bit.iw  7,(iy+$123456)

    res.iw  0,(iy+$123456)
    res.iw  0,(iy)             ; +0 displacement is default, when not specified
    res.iw  1,(iy+$123456)
    res.iw  2,(iy+$123456)
    res.iw  3,(iy+$123456)
    res.iw  4,(iy+$123456)
    res.iw  5,(iy+$123456)
    res.iw  6,(iy+$123456)
    res.iw  7,(iy+$123456)

    set.iw  0,(iy+$123456)
    set.iw  0,(iy)             ; +0 displacement is default, when not specified
    set.iw  1,(iy+$123456)
    set.iw  2,(iy+$123456)
    set.iw  3,(iy+$123456)
    set.iw  4,(iy+$123456)
    set.iw  5,(iy+$123456)
    set.iw  6,(iy+$123456)
    set.iw  7,(iy+$123456)

; Input direct from port address (byte)
    ina a,($fffe)
    ina.ib a,($123456)
    ina.iw a,($12345678)

; Input direct from port address (word)
    inaw hl,($fffe)
    inaw.ib hl,($123456)
    inaw.iw hl,($12345678)

; Input and decrement (word)
    indw

; Input, decrement and repeat (word)
    indrw

; Input and increment (word)
    iniw

; Input, increment and repeat (word)
    inirw

; Input word
    inw bc,(c)
    inw de,(c)
    inw hl,(c)

; Exchange, 16bit registers
    ex bc,bc'
    ex bc,de
    ex bc,hl
    ex bc,ix
    ex bc,iy

    ex de,de'
    ex de,ix
    ex de,iy

    ex hl,hl'
    ex hl,ix
    ex hl,iy

    ex ix,ix'
    ex ix,iy
    ex iy,iy'

; Exchange, 8bit registers
    ex a,(hl)
    ex a,a
    ex a,b
    ex a,c
    ex a,d
    ex a,e
    ex a,h
    ex a,l

    ex b,b'
    ex c,c'
    ex d,d'
    ex e,e'
    ex h,h'
    ex l,l'
    ex a,a'

; Exchange all registers with alternate bank
    exall

; Exchange IX register with alternate bank
    exxx

; Exchange IY register with alternate bank
    exxy

; Extend Sign into HL
    exts
    exts a

; 32bit sign-extend HL
    extsw
    extsw hl

; single cases of LD A,(nn)
    ld.ib a,($123456)
    ld.w a,($1234)              ; word mode, no immediate suffix
    ld.iw,lw a,($12345678)      ; long word mode, 32bit immediate suffix

; ld[.ib | .iw] r,(ix | iy + d)
    ld.ib a,(ix)                ; implicit 0 offset, waste of 3 extra bytes (DD C3 + 1 byte extra for displacement), however valid.
    ld.ib a,(ix - 1000)         ; 16bit displacement
    ld.iw a,(iy)                ; implicit 0 offset, waste of 4 extra bytes (FD C3 + 2 bytes extra for displacement), however valid.
    ld.iw a,(iy + $123456)      ; 24bit displacement

    ld.ib b,(ix + $1234)        ; 16bit displacement
    ld.ib c,(ix + $1234)
    ld.ib d,(ix + $1234)
    ld.ib e,(ix + $1234)
    ld.ib h,(ix + $1234)
    ld.ib l,(ix + $1234)

    ld.ib b,(iy + $1234)
    ld.ib c,(iy + $1234)
    ld.ib d,(iy + $1234)
    ld.ib e,(iy + $1234)
    ld.ib h,(iy + $1234)
    ld.ib l,(iy + $1234)

    ld.iw b,(ix + $123456)      ; 24bit displacement
    ld.iw c,(ix + $123456)
    ld.iw d,(ix + $123456)
    ld.iw e,(ix + $123456)
    ld.iw h,(ix + $123456)
    ld.iw l,(ix + $123456)

    ld.iw b,(iy + $123456)
    ld.iw c,(iy + $123456)
    ld.iw d,(iy + $123456)
    ld.iw e,(iy + $123456)
    ld.iw h,(iy + $123456)
    ld.iw l,(iy + $123456)

; single case LD (nn),a
    ld.ib ($123456),a
    ld.w ($1234),a              ; word mode, no immediate suffix
    ld.iw,lw ($12345678),a      ; long word mode, 32bit immediate suffix

; ld[.ib | .iw] (ix | iy + d),r
    ld.ib (ix),a                ; implicit 0 offset, waste of 3 extra bytes (DD C3 + 1 byte extra for displacement), however valid.
    ld.ib (ix - 1000),a         ; 16bit displacement
    ld.iw (iy),a                ; implicit 0 offset, waste of 4 extra bytes (FD C3 + 2 bytes extra for displacement), however valid.
    ld.iw (iy + $123456),a      ; 24bit displacement

    ld.ib (ix + $1234),b        ; 16bit displacement
    ld.ib (ix + $1234),c
    ld.ib (ix + $1234),d
    ld.ib (ix + $1234),e
    ld.ib (ix + $1234),h
    ld.ib (ix + $1234),l

    ld.ib (iy + $1234),b
    ld.ib (iy + $1234),c
    ld.ib (iy + $1234),d
    ld.ib (iy + $1234),e
    ld.ib (iy + $1234),h
    ld.ib (iy + $1234),l

    ld.iw (ix + $123456),b      ; 24bit displacement
    ld.iw (ix + $123456),c
    ld.iw (ix + $123456),d
    ld.iw (ix + $123456),e
    ld.iw (ix + $123456),h
    ld.iw (ix + $123456),l

    ld.iw (iy + $123456),b
    ld.iw (iy + $123456),c
    ld.iw (iy + $123456),d
    ld.iw (iy + $123456),e
    ld.iw (iy + $123456),h
    ld.iw (iy + $123456),l

; ld[.ib | .iw] (ix | iy + d),n
    ld.ib (ix),$99              ; implicit 0 offset, waste of 3 extra bytes (DD C3 + 1 byte extra for displacement), however valid.
    ld.ib (ix - 1000),$99       ; 16bit displacement
    ld.ib (ix + $1234),$99
    ld.iw (iy),$99
    ld.iw (iy - 1000),$99
    ld.iw (iy + $123456),$99


; ld[.ddir] rr, nn
    ld.ib bc, $123456
    ld.ib de, $123456
    ld.ib hl, $123456
    ld.ib ix, $123456
    ld.ib iy, $123456
    ld.ib sp, $123456

    ld.iw bc, $12345678
    ld.iw de, $12345678
    ld.iw hl, $12345678
    ld.iw ix, $12345678
    ld.iw iy, $12345678
    ld.iw sp, $12345678

; ldw[.ddir] rr, nn
    ldw bc, $1234               ; correct notation, also alias for LD rr,nn
    ldw de, $1234
    ldw hl, $1234
    ldw ix, $1234
    ldw iy, $1234
    ldw sp, $1234

    ldw.ib bc, $123456
    ldw.ib de, $123456
    ldw.ib hl, $123456
    ldw.ib ix, $123456
    ldw.ib iy, $123456
    ldw.ib sp, $123456

    ldw.iw bc, $12345678
    ldw.iw de, $12345678
    ldw.iw hl, $12345678
    ldw.iw ix, $12345678
    ldw.iw iy, $12345678
    ldw.iw sp, $12345678

; ld[.ddir] rr,(ix + d)  -- alias for LDW
    ld bc,(ix)                  ; implicit 0 displacement
    ld bc,(ix - 2)
    ld bc,(ix + 127)
    ld de,(ix + 127)
    ld hl,(ix + 127)
    ld iy,(ix + 127)

    ld.ib bc,(ix)               ; implicit 0 displacement, waste of 3 extra bytes (DD C3 + 1 byte extra for displacement), however valid.
    ld.ib bc,(ix - 2)
    ld.ib bc,(ix + $1234)
    ld.ib de,(ix + $1234)
    ld.ib hl,(ix + $1234)
    ld.ib iy,(ix + $1234)

    ld.iw bc,(ix)               ; implicit 0 offset, waste of 4 extra bytes (FD C3 + 2 bytes extra for displacement), however valid.
    ld.iw bc,(ix - 2)
    ld.iw bc,(ix + $123456)
    ld.iw de,(ix + $123456)
    ld.iw hl,(ix + $123456)
    ld.iw iy,(ix + $123456)

; ldw[.ddir] rr,(ix + d)
    ldw bc,(ix)                  ; implicit 0 displacement
    ldw bc,(ix - 2)
    ldw bc,(ix + 127)
    ldw de,(ix + 127)
    ldw hl,(ix + 127)
    ldw iy,(ix + 127)

    ldw.ib bc,(ix)               ; implicit 0 displacement, waste of 3 extra bytes (DD C3 + 1 byte extra for displacement), however valid.
    ldw.ib bc,(ix - 2)
    ldw.ib bc,(ix + $1234)
    ldw.ib de,(ix + $1234)
    ldw.ib hl,(ix + $1234)
    ldw.ib iy,(ix + $1234)

    ldw.iw bc,(ix)               ; implicit 0 offset, waste of 4 extra bytes (FD C3 + 2 bytes extra for displacement), however valid.
    ldw.iw bc,(ix - 2)
    ldw.iw bc,(ix + $123456)
    ldw.iw de,(ix + $123456)
    ldw.iw hl,(ix + $123456)
    ldw.iw iy,(ix + $123456)

; ld[.ddir] (ix + d),rr  -- alias for LDW
    ld (ix),bc                  ; implicit 0 displacement
    ld (ix - 2),bc
    ld (ix + 127),bc
    ld (ix + 127),de
    ld (ix + 127),hl
    ld (ix + 127),iy

    ld.ib (ix),bc               ; implicit 0 displacement, waste of 3 extra bytes (DD C3 + 1 byte extra for displacement), however valid.
    ld.ib (ix - 2),bc
    ld.ib (ix + $1234),bc
    ld.ib (ix + $1234),de
    ld.ib (ix + $1234),hl
    ld.ib (ix + $1234),iy

    ld.iw (ix),bc               ; implicit 0 offset, waste of 4 extra bytes (FD C3 + 2 bytes extra for displacement), however valid.
    ld.iw (ix - 2),bc
    ld.iw (ix + $123456),bc
    ld.iw (ix + $123456),de
    ld.iw (ix + $123456),hl
    ld.iw (ix + $123456),iy

; ldw[.ddir] (ix + d),rr
    ldw (ix),bc                  ; implicit 0 displacement
    ldw (ix - 2),bc
    ldw (ix + 127),bc
    ldw (ix + 127),de
    ldw (ix + 127),hl
    ldw (ix + 127),iy

    ldw.ib (ix),bc               ; implicit 0 displacement, waste of 3 extra bytes (DD C3 + 1 byte extra for displacement), however valid.
    ldw.ib (ix - 2),bc
    ldw.ib (ix + $1234),bc
    ldw.ib (ix + $1234),de
    ldw.ib (ix + $1234),hl
    ldw.ib (ix + $1234),iy

    ldw.iw (ix),bc               ; implicit 0 offset, waste of 4 extra bytes (FD C3 + 2 bytes extra for displacement), however valid.
    ldw.iw (ix - 2),bc
    ldw.iw (ix + $123456),bc
    ldw.iw (ix + $123456),de
    ldw.iw (ix + $123456),hl
    ldw.iw (ix + $123456),iy

; ld[.ddir] rr,(iy + d)  -- alias for LDW
    ld bc,(iy)                  ; implicit 0 displacement
    ld bc,(iy - 2)
    ld bc,(iy + 127)
    ld de,(iy + 127)
    ld hl,(iy + 127)
    ld ix,(iy + 127)

    ld.ib bc,(iy)               ; implicit 0 displacement, waste of 3 extra bytes (DD C3 + 1 byte extra for displacement), however valid.
    ld.ib bc,(iy + $1234)
    ld.ib de,(iy + $1234)
    ld.ib hl,(iy + $1234)
    ld.ib ix,(iy + $1234)

    ld.iw bc,(iy)               ; implicit 0 offset, waste of 4 extra bytes (FD C3 + 2 bytes extra for displacement), however valid.
    ld.iw bc,(iy - 2)
    ld.iw bc,(iy + $123456)
    ld.iw de,(iy + $123456)
    ld.iw hl,(iy + $123456)
    ld.iw ix,(iy + $123456)

; ldw[.ddir] rr,(iy + d)
    ldw bc,(iy)                  ; implicit 0 displacement
    ldw bc,(iy - 2)
    ldw bc,(iy + 127)
    ldw de,(iy + 127)
    ldw hl,(iy + 127)
    ldw ix,(iy + 127)

    ldw.ib bc,(iy)               ; implicit 0 displacement, waste of 3 extra bytes (DD C3 + 1 byte extra for displacement), however valid.
    ldw.ib bc,(iy + $1234)
    ldw.ib de,(iy + $1234)
    ldw.ib hl,(iy + $1234)
    ldw.ib ix,(iy + $1234)

    ldw.iw bc,(iy)               ; implicit 0 offset, waste of 4 extra bytes (FD C3 + 2 bytes extra for displacement), however valid.
    ldw.iw bc,(iy - 2)
    ldw.iw bc,(iy + $123456)
    ldw.iw de,(iy + $123456)
    ldw.iw hl,(iy + $123456)
    ldw.iw ix,(iy + $123456)

; ld[.ddir] (iy + d),rr -- alias for LDW
    ld (iy),bc                  ; implicit 0 displacement
    ld (iy - 2),bc
    ld (iy + 127),bc
    ld (iy + 127),de
    ld (iy + 127),hl
    ld (iy + 127),ix

    ld.ib (iy),bc               ; implicit 0 displacement, waste of 3 extra bytes (DD C3 + 1 byte extra for displacement), however valid.
    ld.ib (iy + $1234),bc
    ld.ib (iy + $1234),de
    ld.ib (iy + $1234),hl
    ld.ib (iy + $1234),ix

    ld.iw (iy),bc               ; implicit 0 offset, waste of 4 extra bytes (FD C3 + 2 bytes extra for displacement), however valid.
    ld.iw (iy - 2),bc
    ld.iw (iy + $123456),bc
    ld.iw (iy + $123456),de
    ld.iw (iy + $123456),hl
    ld.iw (iy + $123456),ix

; ldw[.ddir] (iy + d),rr
    ldw (iy),bc                  ; implicit 0 displacement
    ldw (iy - 2),bc
    ldw (iy + 127),bc
    ldw (iy + 127),de
    ldw (iy + 127),hl
    ldw (iy + 127),ix

    ldw.ib (iy),bc               ; implicit 0 displacement, waste of 3 extra bytes (DD C3 + 1 byte extra for displacement), however valid.
    ldw.ib (iy + $1234),bc
    ldw.ib (iy + $1234),de
    ldw.ib (iy + $1234),hl
    ldw.ib (iy + $1234),ix

    ldw.iw (iy),bc               ; implicit 0 offset, waste of 4 extra bytes (FD C3 + 2 bytes extra for displacement), however valid.
    ldw.iw (iy - 2),bc
    ldw.iw (iy + $123456),bc
    ldw.iw (iy + $123456),de
    ldw.iw (iy + $123456),hl
    ldw.iw (iy + $123456),ix

; ld[.ddir] rr,(sp + d)  -- alias for LDW
    ld bc,(sp)                  ; implicit 0 displacement
    ld bc,(sp - 2)
    ld bc,(sp + 127)
    ld de,(sp + 127)
    ld hl,(sp + 127)
    ld ix,(sp + 127)
    ld iy,(sp + 127)

    ld.ib bc,(sp)               ; implicit 0 displacement, waste of 3 extra bytes (DD C3 + 1 byte extra for displacement), however valid.
    ld.ib bc,(sp - 2)
    ld.ib bc,(sp + $1234)
    ld.ib de,(sp + $1234)
    ld.ib hl,(sp + $1234)
    ld.ib ix,(sp + $1234)
    ld.ib iy,(sp + $1234)

    ld.iw bc,(sp)               ; implicit 0 offset, waste of 4 extra bytes (FD C3 + 2 bytes extra for displacement), however valid.
    ld.iw bc,(sp - 2)
    ld.iw bc,(sp + $123456)
    ld.iw de,(sp + $123456)
    ld.iw hl,(sp + $123456)
    ld.iw ix,(sp + $123456)
    ld.iw iy,(sp + $123456)

; ldw[.ddir] rr,(sp + d)
    ldw bc,(sp)                  ; implicit 0 displacement
    ldw bc,(sp - 2)
    ldw bc,(sp + 127)
    ldw de,(sp + 127)
    ldw hl,(sp + 127)
    ldw ix,(sp + 127)
    ldw iy,(sp + 127)

    ldw.ib bc,(sp)               ; implicit 0 displacement, waste of 3 extra bytes (DD C3 + 1 byte extra for displacement), however valid.
    ldw.ib bc,(sp - 2)
    ldw.ib bc,(sp + $1234)
    ldw.ib de,(sp + $1234)
    ldw.ib hl,(sp + $1234)
    ldw.ib ix,(sp + $1234)
    ldw.ib iy,(sp + $1234)

    ldw.iw bc,(sp)               ; implicit 0 offset, waste of 4 extra bytes (FD C3 + 2 bytes extra for displacement), however valid.
    ldw.iw bc,(sp - 2)
    ldw.iw bc,(sp + $123456)
    ldw.iw de,(sp + $123456)
    ldw.iw hl,(sp + $123456)
    ldw.iw ix,(sp + $123456)
    ldw.iw iy,(sp + $123456)

; ld[.ddir] (sp + d),rr  -- alias for LDW
    ld (sp),bc                  ; implicit 0 displacement
    ld (sp - 2),bc
    ld (sp + 127),bc
    ld (sp + 127),de
    ld (sp + 127),hl
    ld (sp + 127),ix
    ld (sp + 127),iy

    ld.ib (sp),bc               ; implicit 0 displacement, waste of 3 extra bytes (DD C3 + 1 byte extra for displacement), however valid.
    ld.ib (sp - 2),bc
    ld.ib (sp + $1234),bc
    ld.ib (sp + $1234),de
    ld.ib (sp + $1234),hl
    ld.ib (sp + $1234),ix
    ld.ib (sp + $1234),iy

    ld.iw (sp),bc               ; implicit 0 offset, waste of 4 extra bytes (FD C3 + 2 bytes extra for displacement), however valid.
    ld.iw (sp - 2),bc
    ld.iw (sp + $123456),bc
    ld.iw (sp + $123456),de
    ld.iw (sp + $123456),hl
    ld.iw (sp + $123456),ix
    ld.iw (sp + $123456),iy

; ldw[.ddir] (sp + d),rr
    ldw (sp),bc                  ; implicit 0 displacement
    ldw (sp - 2),bc
    ldw (sp + 127),bc
    ldw (sp + 127),de
    ldw (sp + 127),hl
    ldw (sp + 127),ix
    ldw (sp + 127),iy

    ldw.ib (sp),bc               ; implicit 0 displacement, waste of 3 extra bytes (DD C3 + 1 byte extra for displacement), however valid.
    ldw.ib (sp - 2),bc
    ldw.ib (sp + $1234),bc
    ldw.ib (sp + $1234),de
    ldw.ib (sp + $1234),hl
    ldw.ib (sp + $1234),ix
    ldw.ib (sp + $1234),iy

    ldw.iw (sp),bc               ; implicit 0 offset, waste of 4 extra bytes (FD C3 + 2 bytes extra for displacement), however valid.
    ldw.iw (sp - 2),bc
    ldw.iw (sp + $123456),bc
    ldw.iw (sp + $123456),de
    ldw.iw (sp + $123456),hl
    ldw.iw (sp + $123456),ix
    ldw.iw (sp + $123456),iy

; ld rr,(nn) - load 16/32bit register from indirect 24bit or 32bit addresses (alias for LDW)
    ld.ib bc,($123456)
    ld.w bc,($1234)             ; word mode, no immediate suffix
    ld.iw,lw bc,($12345678)     ; long word mode, 32bit immediate suffix

    ld.ib de,($123456)
    ld.ib hl,($123456)
    ld.ib sp,($123456)
    ld.ib ix,($123456)
    ld.ib iy,($123456)

    ld.iw bc,($12345678)
    ld.iw de,($12345678)
    ld.iw hl,($12345678)
    ld.iw sp,($12345678)
    ld.iw ix,($12345678)
    ld.iw iy,($12345678)

; ldw rr,(nn) - load 16/32bit register from indirect 24bit or 32bit addresses
    ldw bc,($1234)
    ldw de,($1234)
    ldw hl,($1234)
    ldw sp,($1234)
    ldw ix,($1234)
    ldw iy,($1234)

    ldw.ib bc,($123456)
    ldw.w bc,($1234)             ; word mode, no immediate suffix
    ldw.iw,lw bc,($12345678)     ; long word mode, 32bit immediate suffix

    ldw.ib de,($123456)
    ldw.ib hl,($123456)
    ldw.ib sp,($123456)
    ldw.ib ix,($123456)
    ldw.ib iy,($123456)

    ldw.iw bc,($12345678)
    ldw.iw de,($12345678)
    ldw.iw hl,($12345678)
    ldw.iw sp,($12345678)
    ldw.iw ix,($12345678)
    ldw.iw iy,($12345678)


; ld (nn),rr - load indirect 24bit or 32bit addresses with 16/32bit register (alias for LDW)
    ld.ib ($123456),bc
    ld.w ($1234),bc             ; word mode, no immediate suffix
    ld.iw,lw ($12345678),bc     ; long word mode, 32bit immediate suffix

    ld.ib ($123456),de
    ld.ib ($123456),hl
    ld.ib ($123456),sp
    ld.ib ($123456),ix
    ld.ib ($123456),iy

    ld.iw ($12345678),bc
    ld.iw ($12345678),de
    ld.iw ($12345678),hl
    ld.iw ($12345678),sp
    ld.iw ($12345678),ix
    ld.iw ($12345678),iy

; ldw (nn),rr - load indirect 24bit or 32bit addresses with 16/32bit register
    ldw ($1234),bc
    ldw ($1234),de
    ldw ($1234),hl
    ldw ($1234),sp
    ldw ($1234),ix
    ldw ($1234),iy

    ldw.ib      ($123456),bc
    ldw.w       ($1234),bc      ; word mode, no immediate suffix
    ldw.iw,lw   ($12345678),bc  ; long word mode, 32bit immediate suffix

    ldw.ib ($123456),de
    ldw.ib ($123456),hl
    ldw.ib ($123456),sp
    ldw.ib ($123456),ix
    ldw.ib ($123456),iy

    ldw.iw ($12345678),bc
    ldw.iw ($12345678),de
    ldw.iw ($12345678),hl
    ldw.iw ($12345678),sp
    ldw.iw ($12345678),ix
    ldw.iw ($12345678),iy


; ld rr16,rr16  --  alias for LDW
    ld  bc,bc
    ld  bc,de
    ld  bc,hl
    ld  bc,ix
    ld  bc,iy

    ld  de,bc
    ld  de,de
    ld  de,hl
    ld  de,ix
    ld  de,iy

    ld  hl,bc
    ld  hl,de
    ld  hl,hl
    ld  hl,ix
    ld  hl,iy

    ld  hl,i                    
    ld  i,hl                    

    ld  ix,bc
    ld  ix,de
    ld  ix,hl
    ld  ix,iy

    ld  iy,bc
    ld  iy,de
    ld  iy,hl
    ld  iy,ix

; ldw rr16,rr16
    ldw  bc,bc
    ldw  bc,de
    ldw  bc,hl
    ldw  bc,ix
    ldw  bc,iy

    ldw  de,bc
    ldw  de,de
    ldw  de,hl
    ldw  de,ix
    ldw  de,iy

    ldw  hl,bc
    ldw  hl,de
    ldw  hl,hl
    ldw  hl,ix
    ldw  hl,iy

    ldw  hl,i
    ldw  i,hl

    ldw  ix,bc
    ldw  ix,de
    ldw  ix,hl
    ldw  ix,iy

    ldw  iy,bc
    ldw  iy,de
    ldw  iy,hl
    ldw  iy,ix


; ld[.w | .lw] (bc),rr    -- alias for LDW
; ld[.w | .lw] (de),rr
; ld[.w | .lw] (hl),rr
    ld (bc),bc
    ld (bc),de
    ld (bc),hl
    ld (bc),ix
    ld (bc),iy

    ld.w (bc),bc
    ld.w (bc),de
    ld.w (bc),hl
    ld.w (bc),ix
    ld.w (bc),iy
    ld.lw (bc),bc
    ld.lw (bc),de
    ld.lw (bc),hl
    ld.lw (bc),ix
    ld.lw (bc),iy

    ld (de),bc
    ld (de),de
    ld (de),hl
    ld (de),ix
    ld (de),iy
    ld (hl),bc
    ld (hl),de
    ld (hl),hl
    ld (hl),ix
    ld (hl),iy

; ldw[.w | .lw] (bc),rr
; ldw[.w | .lw] (de),rr
; ldw[.w | .lw] (hl),rr
    ldw (bc),bc
    ldw (bc),de
    ldw (bc),hl
    ldw (bc),ix
    ldw (bc),iy

    ldw.w (bc),bc
    ldw.w (bc),de
    ldw.w (bc),hl
    ldw.w (bc),ix
    ldw.w (bc),iy
    ldw.lw (bc),bc
    ldw.lw (bc),de
    ldw.lw (bc),hl
    ldw.lw (bc),ix
    ldw.lw (bc),iy

    ldw (de),bc
    ldw (de),de
    ldw (de),hl
    ldw (de),ix
    ldw (de),iy
    ldw (hl),bc
    ldw (hl),de
    ldw (hl),hl
    ldw (hl),ix
    ldw (hl),iy

; ldw[.ib|.iw] (bc|de,hl),nn
    ldw (bc),0
    ldw (de),const
    ldw (hl),$8081

    ldw.ib (bc),0
    ldw.ib (de),const24bit
    ldw.ib (hl),const24bit

    ldw.iw (bc),0
    ldw.iw (de),$12345678
    ldw.iw (hl),$12345678

; ld[.w | .lw] rr,(bc)  -- alias for LDW
; ld[.w | .lw] rr,(de)
; ld[.w | .lw] rr,(hl)
    ld bc,(bc)
    ld de,(bc)
    ld hl,(bc)
    ld ix,(bc)
    ld iy,(bc)

    ld.w bc,(bc)
    ld.w de,(bc)
    ld.w hl,(bc)
    ld.w ix,(bc)
    ld.w iy,(bc)
    ld.lw bc,(bc)
    ld.lw de,(bc)
    ld.lw hl,(bc)
    ld.lw ix,(bc)
    ld.lw iy,(bc)

    ld bc,(de)
    ld de,(de)
    ld hl,(de)
    ld ix,(de)
    ld iy,(de)

    ld bc,(hl)
    ld de,(hl)
    ld hl,(hl)
    ld ix,(hl)
    ld iy,(hl)

; ldw[.w | .lw] rr,(bc)
; ldw[.w | .lw] rr,(de)
; ldw[.w | .lw] rr,(hl)
    ldw bc,(bc)
    ldw de,(bc)
    ldw hl,(bc)
    ldw ix,(bc)
    ldw iy,(bc)

    ldw.w bc,(bc)
    ldw.w de,(bc)
    ldw.w hl,(bc)
    ldw.w ix,(bc)
    ldw.w iy,(bc)
    ldw.lw bc,(bc)
    ldw.lw de,(bc)
    ldw.lw hl,(bc)
    ldw.lw ix,(bc)
    ldw.lw iy,(bc)

    ldw bc,(de)
    ldw de,(de)
    ldw hl,(de)
    ldw ix,(de)
    ldw iy,(de)

    ldw bc,(hl)
    ldw de,(hl)
    ldw hl,(hl)
    ldw ix,(hl)
    ldw iy,(hl)


; Load and decrement (word)
    lddw

; Load, decrement and repeat (word)
    lddrw

; Load and increment (word)
    ldiw

; Load, increment and repeat (word)
    ldirw

; Mode test
    mtest

; Negate HL Register
    negw
    negw hl

; Output, decrement and repeat (word)
    otdrw

; Output, increment and repeat (word)
    otirw

; Output and decrement (word)
    outdw

; Output and increment (word)
    outiw

; Output (byte)
    out (c),$80

; Output (word)
    outw (c),bc
    outw (c),de
    outw (c),hl
    outw (c),$7fff
    outw (c),const+1

; Output A to address (byte)
    outa ($1234),a
    outa.ib ($123456),a
    outa.iw ($12345678),a

; Output HL to address (word)
    outaw ($1234),hl
    outaw.ib ($123456),hl
    outaw.iw ($12345678),hl


; Return from breakpoint
    retb

; push sr and push immediate (Z380 only)
    push sr
    push const
    push.ib const24bit
    push.iw $C03fffff

; pop sr (Z380 only)
    pop sr

; optional syntax for push & pop
    push af,bc,de,hl,ix,iy,sr
    pop af,bc,de,hl,ix,iy,sr
    push $8000,$c000
    push.iw $C03fffff,$7f010203

; swap
    swap bc
    swap de
    swap hl
    swap ix
    swap iy
