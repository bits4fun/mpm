:: *************************************************************************************
::
::  MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
::   MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
::   MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
::   MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
::   MMMM       MMMM     PPPP              MMMM       MMMM
::   MMMM       MMMM     PPPP              MMMM       MMMM
::  MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
::
:: Windows compile script for Mpm
:: (C) Gunther Strube (hello@bits4fun.net) 2012-2022
::
:: Optional:
::   when git and .git folder exists, add git commit ID string for mpm -h
::   Build using Cmake if found on system (priority #1)
::
:: Requirements:
::   mingw32-make and gcc-compatible compiler available in PATH
::
::  This file is part of Mpm.
::  Mpm is free software; you can redistribute it and/or modify
::  it under the terms of the GNU General Public License as published by the Free Software Foundation;
::  either version 2, or (at your option) any later version.
::  Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
::  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
::  See the GNU General Public License for more details.
::  You should have received a copy of the GNU General Public License along with Mpm;
::  see the file COPYING.  If not, write to the
::  Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
::
:: *************************************************************************************

@echo off

:: --------------------------------------------------------------------------
:: remember the current path in from where this script is called
set MPM_PATH=%cd%
set revision_file=%cd%\src\build.h
set git_folder=%cd%\.git\

:: delete any previous compile output
call cleanup.bat

:: try to locate git.exe...
set gitfound=0
for %%x in (git.exe) do if not [%%~$PATH:x]==[] set gitfound=1
if "%gitfound%"=="0" goto NO_GITBUILDID
if not exist %git_folder% goto NO_GITBUILDID

:: output current Git revision as a 32bit hex string, to be included as part of Mpm release information
git.exe log  -n 1 --pretty=format:"#define MPM_BUILDMSG \"%%h\"%%n" > %revision_file%
goto CHECK_CMAKE

:: Git nor .git folder was found, use "0" as build commit ID
:NO_GITBUILDID
echo #define MPM_BUILDMSG "0" > %revision_file%

:: try to locate cmake.exe...
:CHECK_CMAKE
set cmakefound=0
for %%x in (cmake.exe) do if not [%%~$PATH:x]==[] set cmakefound=1
if "%cmakefound%"=="0" goto CHECK_MINGW32
mkdir build
cd build
cmake .. -G "MinGW Makefiles"
if ERRORLEVEL 1 goto CMAKE_FAILED
mingw32-make
if ERRORLEVEL 1 goto MINGW_FAILED
cd ..
DIR build\src\mpm.exe
goto END

:: try to locate mingw32-make.exe...
:CHECK_MINGW32
set mingw32found=0
for %%x in (mingw32-make.exe) do if not [%%~$PATH:x]==[] set mingw32found=1
if "%mingw32found%"=="0" goto MINGW32MAKE_NOT_AVAILABLE

:: Check that Mingw stand-alone compiler is available
set gccfound=0
for %%x in (gcc.exe) do if not [%%~$PATH:x]==[] set gccfound=1
if "%gccfound%"=="0" goto MINGW32_COMPILER_NOT_AVAILABLE
mingw32-make.exe -f makefile.gcc.all platform=MSWIN
if ERRORLEVEL 0 goto MPM_COMPILED
goto MINGW_FAILED

:MINGW32MAKE_NOT_AVAILABLE
echo mingw32-make.exe utility could not be located!
echo (PATH env. variable doesn't point to Mingw32 compiler tools and libs)
goto END

:MINGW32_COMPILER_NOT_AVAILABLE
echo gcc Mingw compiler could not be located!
echo (PATH env. variable doesn't point to Mingw compiler tools and libs)
goto END

:MPM_COMPILED
echo mpm compiled successfully on your system.
DIR build\mpm.exe
goto END

:CMAKE_FAILED
echo Cmake failed to generate Makefile!

:END
@echo on
