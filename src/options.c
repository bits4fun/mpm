/***************************************************************************************************
 *
 *  MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
 *   MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
 *   MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
 *   MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *  MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
 *
 * Copyright (C) 1991-2024, Gunther Strube, hello@bits4fun.net
 *
 * This file is part of Mpm.
 * Mpm is free software; you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation - either version 2, or (at your option)
 * any later version.
 * Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Mpm -
 * see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ***************************************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "config.h"
#include "build.h"
#include "datastructs.h"
#include "main.h"
#include "options.h"
#include "libraries.h"
#include "symtables.h"
#include "sourcefile.h"
#include "pass.h"
#include "prsline.h"
#include "i8080.h"
#include "z80.h"
#include "zxn.h"
#include "z180.h"
#include "z380.h"
#include "errors.h"

/******************************************************************************
                          Global variables
 ******************************************************************************/
const char copyrightmsg[] = MPM_COPYRIGHTMSG;
const char buildmsg[] = MPM_BUILDMSG;

bool uselistingfile;
bool link2binaryfile;
bool writeline;
bool assembleupdates;
bool addressalign;
bool writesymtable;
bool writeaddrmapfile;
bool writeobjectfile;
bool writelistingfile;
bool writeexpandedmacro;
bool writeihexfile;
bool writesha256file;
bool writeglobaldeffile;
bool validaterefs;
bool deforigin;
bool verbose;
bool uselibraries;
bool createlibrary;
bool z80autorelocate;
bool z80reltablefile;
bool useothersrcext;
bool z80codesegment;
bool expl_binflnm;
bool crc32file;
bool clinemode;
bool BIGENDIAN;
bool USEBIGENDIAN;

bool asmerror;
unsigned long EXPLICIT_ORIGIN;          /* origin defined from command line */



/******************************************************************************
                          Local functions and variables
 ******************************************************************************/

static void
GetCmdLineDefSym(const char *flagid)
{
    char ident[MAX_NAME_SIZE+1];
    int i;

    strncpy (ident, flagid, MAX_NAME_SIZE);     /* Copy argument string */
    if (!isalpha(ident[0]) && ident[0] != '_') {
        ReportError (NULL, Err_IllegalIdent);    /* symbol must begin with alpha */
        return;
    }
    i = 0;
    while (ident[i] != '\0') {
        if (strchr (separators, ident[i]) == NULL) {
            if (!isalnum (ident[i]) && ident[i] != '_') {
                ReportError (NULL, Err_IllegalIdent);        /* illegal char in identifier */
                return;
            } else {
                ident[i] = (char) toupper (ident[i]);
            }
        } else {
            ReportError (NULL, Err_IllegalIdent);        /* illegal char in identifier */
            return;
        }
        ++i;
    }

    if (FindSymbol (ident, staticsymbols) == NULL) {
        DefineDefSym (NULL, ident, 1, &staticsymbols);
    }
}



/******************************************************************************
                               Public functions
 ******************************************************************************/

void
DefaultOptions (int argc, char *argv[])
{
    /* define the default paths for INCLUDE files and for libraries */
    AddPathNode (getenv(ENVNAME_INCLUDEPATH), &gIncludePath);
    AddPathNode (getenv(ENVNAME_LIBRARYPATH), &gLibraryPath);

    /* Zilog Z80 is default parsing and code generation */
    SetCpuAssembler(z80, ParseLineZ80, LookupZ80Mnemonic);
    GetCmdLineDefSym("Z80");

    asmerror = false;
    writeline = writeaddrmapfile = true;
    verbose = useothersrcext = link2binaryfile = assembleupdates = z80codesegment = addressalign = false;
    deforigin = uselibraries = createlibrary = z80autorelocate = z80reltablefile = clinemode = false;
    writeglobaldeffile = validaterefs = writelistingfile = writeexpandedmacro = writeobjectfile = writesymtable = false;
    crc32file = writeihexfile = writesha256file = false;

    strncpy(objext, ".obj", 5); /* default object filename extension */

    if (argc == 1) {
        printf("%s - build %s\n", copyrightmsg, buildmsg);        
        puts("Try -h for more information.");
        exit (1);
    } else if ((argc == 2) && (strcmp(argv[1],"-h") == 0)) {
        prompt();
        exit(1);
    }
}


void
SetAsmFlag (char *flagid)
{
    bool Option;

    if (strcmp (flagid, "version") == 0) {
        printf("%s - build %s\n", copyrightmsg, buildmsg);
        exit(VERSION_NUMBER);
    }

    /* create CRC32 *.crc file of compiled binary */
    if (strcmp(flagid, "crc32") == 0) {
        crc32file = true;
        return;
    }

    if (strcmp(flagid, "sha2") == 0) {
        writesha256file = true;
        return;
    }

    if (strcmp(flagid, "ihex") == 0) {
        writeihexfile = true;
        return;
    }

    if (strcmp(flagid, "cz80") == 0) {
        z80codesegment = true;
        return;
    }

    if (strcmp(flagid, "rz80") == 0) {
        z80autorelocate = true;
        return;
    }

    if (strcmp(flagid, "Rz80") == 0) {
        z80autorelocate = z80reltablefile = true;
        return;
    }

    if (strcmp(flagid, "Map") == 0) {
        writeaddrmapfile = true;
        return;
    }

    if (strcmp(flagid, "nMap") == 0) {
        writeaddrmapfile = false;
        return;
    }

    /* specify cpu machine architecture for specific syntax parsing and code generation */
    if (*flagid == 'm') {
        if (strcmp((flagid + 1), "8080") == 0) {
            /* Intel 8080 */
            SetCpuAssembler(i8080, ParseLineZ80, Lookupi8080Mnemonic);
            GetCmdLineDefSym("i8080");
        }
        if (strcmp((flagid + 1), "z80") == 0) {
            SetCpuAssembler(z80, ParseLineZ80, LookupZ80Mnemonic);
            GetCmdLineDefSym("Z80");
        }
        if (strcmp((flagid + 1), "zxn") == 0) {
            /* ZX Spectrum Next FPGA (Z80N) */
            SetCpuAssembler(zxn, ParseLineZ80, LookupZXNMnemonic);
            GetCmdLineDefSym("ZXN");
        }
        if (strcmp((flagid + 1), "z180") == 0) {
            SetCpuAssembler(z180, ParseLineZ80, LookupZ180Mnemonic);
            GetCmdLineDefSym("Z180");
        }
        if (strcmp((flagid + 1), "z380") == 0) {
            SetCpuAssembler(z380, ParseLineZ80, LookupZ380Mnemonic);
            GetCmdLineDefSym("Z380");
        }

        return;
    }

    /* use ".xxx" as source file in stead of ".asm" */
    if (*flagid == 'e') {
        useothersrcext = true;
        srcext[0] = '.';
        strncpy ((srcext + 1), (flagid + 1), 3);  /* Copy argument string */
        srcext[4] = '\0';                         /* max. 3 letters extension */
        return;
    }

    /* djm: mod to get .o files produced instead of .obj */
    /* gbs: extended to use argument as definition, e.g. -Mo, which defines .o extension */
    if (*flagid == 'M') {
        strncpy ((objext + 1), (flagid + 1), 3);   /* copy argument string (append after '.') */
        objext[4] = '\0';                          /* max. 3 letters extension */
        return;
    }

    /* specify library file for static linking */
    if (*flagid == 'l') {
        RegisterLibfile ((flagid + 1));
        return;
    }

    /* create Include Path list from argument */
    if (*flagid == 'I') {
        ++flagid;
        AddPathNode (flagid, &gIncludePath);
        return;
    }

    /* create Library Path list from argument */
    if (*flagid == 'L') {
        ++flagid;
        AddPathNode (flagid, &gLibraryPath);
        return;
    }

    /* create library file */
    if (*flagid == 'x') {
        createlibrary = true;
        libfilename = CreateLibfileName ((flagid + 1));
        return;
    }

    /* explicit origin */
    if (*flagid == 'r') {
        sscanf (flagid + 1, "%lx", &EXPLICIT_ORIGIN);
        deforigin = true;           /* explicit origin has been defined */
        return;
    }

    /* explicit output filename */
    if (*flagid == 'o') {
        if (strlen(flagid+1) > 255) {
            *(flagid+255) = '\0';    /* truncate if filename argument > 255 */
        }

        sscanf (flagid + 1, "%s", binfilename); /* store explicit filename for compiled binary */
        expl_binflnm = true;
        return;
    }

    /* explicit symbol definition */
    if (*flagid == 'D') {
        GetCmdLineDefSym(flagid+1);
        return;
    }

    /* all special cases evaluated, now check for single letter flag options... */
    if (*flagid == 'n') {
        Option = false;
        flagid++;
    } else {
        Option = true;
    }

    while(*flagid != 0) {
        switch(*flagid) {
        case 'C':
            clinemode = Option;
            break;
        case 'a':
            writelistingfile = uselistingfile = Option;
            break;
        case 'm':
            writeexpandedmacro = Option;
            break;
        case 's':
            writesymtable = Option;
            break;
        case 'b':
            link2binaryfile = Option;
            break;
        case 'j':
            writeobjectfile = Option;
            break;
        case 'v':
            verbose = Option;
            break;
        case 'd':
            assembleupdates = Option;
            break;
        case 'g':
            writeglobaldeffile = Option;
            break;
        case 'G':
            validaterefs = Option;
            break;
        case 'A':
            addressalign = Option;
            break;
        default:
            fprintf(stderr,"'%c': ", *flagid);
            ReportError (NULL, Err_IllegalOption);
            return;
        }

        flagid++;
    }
}


void
display_options (void)
{
    if (assembleupdates == true) {
        puts ("Assemble only updated files.");
    } else {
        puts ("Assemble all files");
    }
    if (writelistingfile == true) {
        puts ("Create listing file.");
    }
    if (writesymtable == true && writelistingfile == true) {
        puts ("Create symbol table.");
    }
    if (writeexpandedmacro == true && writelistingfile == true) {
        puts ("Write expanded macro definition into listing file.");
    }
    if (writesymtable == true && writelistingfile == false) {
        puts ("Create symbol table file.");
    }
    if (writeglobaldeffile == true) {
        puts ("Create global definition file.");
    }
    if (validaterefs == true) {
        puts ("Validate usage XDEF & XREF references");
    }
    if (createlibrary == true) {
        puts ("Create library from specified modules.");
    }
    if (link2binaryfile == true) {
        puts ("Create & link object modules to executable code.");
    }
    if (writeobjectfile == true) {
        puts ("Create Object code from sources.");
    }
    if (uselibraries == true) {
        puts ("Link referenced library modules with code.");
    }
    if (link2binaryfile == true && writeaddrmapfile == true) {
        puts ("Create address map file.");
    }
    if (z80autorelocate == true) {
        puts ("Create relocatable code.");
    }
    if (z80reltablefile == true) {
        puts ("Create relocation address table and ORG 0 binary files.");
    }
    if (z80codesegment == true && z80autorelocate == false) {
        puts ("Split code into 16K banks.");
    }
    putchar ('\n');
}


void
prompt (void)
{
    printf("%s - build %s\n", copyrightmsg, buildmsg);
    puts("https://gitlab.com/b4works/mpm\n");
    puts ("mpm [options] {<filename>} | @<modulefile>}");
    printf ("To assemble 'program%s' use \"mpm program\" or \"mpm program%s\".\n\n", asmext, asmext);
    puts ("@<modulefile> contains file names of all modules to be linked; File names");
    puts ("are put on separate lines ended with \\n. File types recognized by or");
    puts ("created by mpm (defined by the following extensions):");
    printf ("%s = source file (default), or alternative -e<ext> (3 chars)\n", asmext);
    printf ("%s = object file, %s = listing file, %s = symbol table file\n", objext, lstext, symext);
    printf ("%s = static linked executable binary, %s = address map file\n", binext, mapext);
    printf ("%s = global constant definition file, %s = error file, %s = library file\n", defext, errext, libext);
    puts ("\nFlag Options: -n = option OFF, eg. -nas = no listing file, no symbol table.");
    puts ("-v verbose output");
    puts ("-a listing file, -s symbol table, -m expanded macro in listing file");
    puts ("-j write linkable object files from source file modules.");
    puts ("-b linking & relocation into executable binary from specified object files.");
    puts ("-g Global Relocation Address DEF File, XDEF from modules as DEFC address defs.");
    puts ("-G Validate XDEF & XREF usage and report warning if declared but not used.");
    puts ("-C Override compile error line numbers using LINE directive line number");
    puts ("-A Address align DEFW & DEFL constants");
    puts ("-d date stamp control, assemble only if source file newer than object file.");
    puts ("-Map write address map file");
    puts ("-m8080 -mz80 -mzxn -mz180 -mz380 Set assembler to Intel 8080 or Zilog Z80,Z80N,Z180,Z380");
    puts ("-rz80 Generate relocatable Z80 code (uses address patching - must run in RAM)");
    puts ("-Rz80 Generate relocation address table and ORG 0 binary files separately");
    puts ("-cz80 split Z80 executable code into 16K files using *.bnX extension.");
    puts ("-ihex output executable code in Intel Hex format with .ihx extension.");
    puts ("-sha2 iterate executable code with sha256 checksum and generate a .sha2 file.");
    puts ("-crc32 iterate executable code with crc32 checksum and generate a .crc file.");
    puts (DEFAULT_OPTIONS);

    puts ("\nParameterized Options (parameter value immediately followed after option):");
    puts ("-r<ORG> Explicit relocation <ORG> defined in hex (ignore ORG in first module).");
    puts("-I<Include File Path> Multiple Search Path for INCLUDE directive.");
    puts("-L<Library File Path> Multiple Search Path for -l option (link with library).");
    printf("  use %c to separate individual directory paths or multiple -I or -L options.\n", ENVPATHSEP);
    puts ("-D<symbol> define symbol as logically TRUE (used for conditional assembly)");
    puts ("-o<bin filename> explicit output filename of static linked modules.");
    printf ("-l<library file> link LIB modules into binary as referenced by %s modules.\n", objext);
    puts ("-x<library file> create library from specified modules ( e.g. with @<modules> )");
}
