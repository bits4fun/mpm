/***************************************************************************************************
 *
 *  MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
 *   MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
 *   MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
 *   MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *  MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
 *
 * Copyright (C) 1991-2024, Gunther Strube, hello@bits4fun.net
 *
 * This file is part of Mpm.
 * Mpm is free software; you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation - either version 2, or (at your option)
 * any later version.
 * Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Mpm -
 * see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ***************************************************************************************************/


#include <time.h>
#include <stdlib.h>

#include "config.h"             /* compilation constant definitions */
#include "options.h"            /* functions to cmd line arguments and display of help */
#include "compile.h"            /* functions for high level assembly operations */
#include "main.h"               /* global variable declarations */


/******************************************************************************
    Specify API for localtime_r() C2X Ansi C standard
 ******************************************************************************/
extern struct tm *localtime_r (const time_t *__timer, struct tm *__tp);


/******************************************************************************
                          Global variables
 ******************************************************************************/

char *libfilename;

const char asmext[] = ".asm";
const char lstext[] = ".lst";
const char symext[] = ".sym";
const char defext[] = ".def";
const char binext[] = ".bin";
const char mapext[] = ".map";
const char wrnext[] = ".wrn";
const char errext[] = ".err";
const char libext[] = ".lib";
const char crcext[] = ".crc";

char srcext[5];                                 /* contains default source file extension */
char objext[5];                                 /* contains default object file extension */
char binfilename[MAX_FILENAME_SIZE+1];          /* -o explicit filename buffer */
char date[MAX_STRBUF_SIZE];                     /* pointer to datestring calculated from asmtime */

size_t CODESIZE;
size_t oldPC;


/******************************************************************************
                          Local functions and variables
 ******************************************************************************/

static void DefineEndianLayout(void);
static time_t asmtime;                     /* time of assembly in seconds */


/*!
 * \details
 * Investigate whether the memory architecture running this assembler use Little Endian
 * (low byte - high byte order) or Big Endian (high byte - low byte order)
 *
 * Global variable BIGENDIAN (boolean) is set accordingly.
 */
static void
DefineEndianLayout(void)
{
    unsigned short  v = 0x8000;
    const unsigned char *vp;

    vp = (unsigned char *) &v;  /* point at first byte of signed long word */
    BIGENDIAN = *vp == 0x80 ? true: false;

    /* for Z80, code generation always use little endian format on integers (TODO: move this for CPU-specific output) */
    USEBIGENDIAN = false;
}


/******************************************************************************
                               Public functions
 ******************************************************************************/


/*!
 * \brief Main entry of Mpm from operating system command line
 * \param argc
 * \param argv
 * \return
 */
int
main (int argc, char *argv[])
{
    /* get current system time for time stamp in listing file */
#if MSWIN
    time (&asmtime);
    strftime(date, MAX_STRBUF_SIZE-1, "%c\n", localtime(&asmtime));
#else
    struct tm localtm;

    time (&asmtime);
    strftime(date, MAX_STRBUF_SIZE-1, "%c\n", localtime_r(&asmtime, &localtm));
#endif
    /*
       Investigate whether the memory architecture running this assembler use Little Endian
       (low byte - high byte order) or Big Endian (high byte - low byte order)
     */
    DefineEndianLayout();

    DefaultOptions(argc, argv);

    /* compile & link code, based on command line arguments */
    compile(argc, argv);

    FreeAllocatedMemory();

    exit (asmerror == true ? 1: 0);
}
