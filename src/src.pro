
# ------------------------------------------------------------------------------
#   MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
#    MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
#    MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
#    MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
#    MMMM       MMMM     PPPP              MMMM       MMMM
#    MMMM       MMMM     PPPP              MMMM       MMMM
#   MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
#
# ------------------------------------------------------------------------------
#
# Qt-Creator Project for the Mpm Assembler.
#
# Copyright (C) 1991-2024, Gunther Strube, hello@bits4fun.net
#
# Use qmake mpm.pro, then just make.
#
# Mpm is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the Free Software Foundation;
# either version 2, or (at your option) any later version.
# Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with Mpm;
# see the file COPYING.  If not, write to the
# Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# ------------------------------------------------------------------------------

TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CFLAGS += -std=c99 -pedantic

contains(QMAKE_CXX,g++) {
  # GCC compiler is used on platform
  QMAKE_CFLAGS += -Wno-format-truncation      
}

TARGET = mpm
DESTDIR = ../build
MOC_DIR = ../build/moc
RCC_DIR = ../build/rcc
unix:OBJECTS_DIR = ../build/o/unix
win32:OBJECTS_DIR = ../build/o/win32
macx:OBJECTS_DIR = ../build/o/mac

win32 {
        DEFINES += MSWIN
}

!win32 {
        DEFINES += UNIX
}

HEADERS  += config.h main.h compile.h asmdrctv.h macros.h prsline.h datastructs.h exprprsr.h modules.h pass.h avltree.h \
            sourcefile.h memfile.h ihex.h crc32.h lonesha256.h errors.h objfile.h libraries.h options.h section.h \
            symtables.h i8080.h z80.h zxn.h z180.h z380.h

SOURCES  += main.c compile.c asmdrctv.c macros.c sourcefile.c memfile.c prsline.c crc32.c exprprsr.c options.c symtables.c \
            ihex.c avltree.c errors.c libraries.c objfile.c modules.c pass.c section.c i8080.c z80.c zxn.c z180.c z380.c
