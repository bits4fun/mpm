/***************************************************************************************************
 *
 *  MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
 *   MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
 *   MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
 *   MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *  MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
 *
 * Copyright (C) 1991-2024, Gunther Strube, hello@bits4fun.net
 *
 * This file is part of Mpm.
 * Mpm is free software; you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation - either version 2, or (at your option)
 * any later version.
 * Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Mpm -
 * see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ***************************************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "config.h"
#include "objfile.h"
#include "options.h"
#include "avltree.h"
#include "symtables.h"
#include "sourcefile.h"
#include "modules.h"
#include "memfile.h"
#include "errors.h"


/******************************************************************************
                          Global variables
 ******************************************************************************/


/******************************************************************************
                          Local functions and variables
 ******************************************************************************/
static const char objhdrprefix[] = "oooomodnexprnamelibnmodc";  /* template of pointers to sections of OBJ file */

static memfile_t *GetNodeObjMemfile(const symbol_t * node);
static void ObjFileStoreLibReference (const symbol_t *node);
static void ObjFileStoreGlobalName (const symbol_t *node);
static void ObjFileStoreLocalName (const symbol_t *node);
static void ObjFileWriteName (const symbol_t *node, const unsigned long symscope);
static void ObjFileInstallSymbol(avltree_t **symbols, const char *symname, symvalue_t symval, unsigned long symtype, module_t *symowner);
static int ObjFileCheckWaterMark(const char *string, const char *watermark, size_t sizeofWaterMark);

/*!
 * \brief Get object (memory) file via symbol node that is associated to current module
 * \param node
 * \return pointer to object memory file, or NULL if there is no node or no object file
 */
static memfile_t *
GetNodeObjMemfile(const symbol_t * node)
{
    if (node == NULL) {
        return NULL;
    }
    if (node->owner == NULL) {
        return NULL;
    }

    return node->owner->objfile;
}


/*!
 * \brief Store library reference name declaration to object (memory) file
 * \param node the current symbol node containing the reference
 */
static void
ObjFileStoreLibReference (const symbol_t *node)
{
    size_t b;
    memfile_t *mobjf = GetNodeObjMemfile(node);

    /* if there is no node or no object file, do nothing */
    if (mobjf == NULL) {
        return;
    }

    if ((node->type & SYMXREF) && (node->type & SYMDEF) && (node->type & SYMTOUCHED)) {
        b = strlen (node->symname);
        MemfPutc(mobjf, (int) b);               /* write length of symbol name to relocatable file */
        MemfWrite(mobjf, node->symname, b);     /* write symbol name to relocatable file */
    }
}


/*!
 * \brief Store global name reference declaration to object (memory) file
 * \param node the current symbol node containing the reference
 */
static void
ObjFileStoreGlobalName (const symbol_t *node)
{
    if (node == NULL) {
        return;
    }

    if (node->type & SYMXDEF && node->type & SYMTOUCHED && strcmp("$PC", node->symname) != 0) {
        /* Any global name that has been references (ignore $PC, it's internal used only) */
        ObjFileWriteName (node, SYMXDEF);
    }
}


/*!
 * \brief Store local name reference declaration to object (memory) file
 * \param node the current symbol node containing the reference
 */
static void
ObjFileStoreLocalName (const symbol_t * node)
{
    if (node == NULL) {
        return;
    }

    if ((node->type & SYMLOCAL) && (node->type & SYMTOUCHED)) {
        ObjFileWriteName (node, SYMLOCAL);
    }
}


/*!
 * \brief Store name reference declaration to object (memory) file
 * \details The object (memory) file is accessed via symbol node that is associated to current module.
 * \param node the current symbol node containing the reference
 * \param scope
 */
static void
ObjFileWriteName (const symbol_t *node, const unsigned long scope)
{
    size_t b;
    memfile_t *mobjf = GetNodeObjMemfile(node);

    /* if there is no node or no object file in current module, abort mission */
    if (mobjf == NULL) {
        return;
    }

    if (scope == SYMLOCAL) {
        MemfPutc(mobjf, 'L');
    }

    if (scope == SYMXDEF) {
        if (node->type & SYMDEF) {
            MemfPutc(mobjf, 'X');
        } else {
            MemfPutc(mobjf, 'G');
        }
    }

    if (node->type & SYMADDR) {     /* then write type of symbol */
        MemfPutc(mobjf, 'A');       /* either a relocatable 32bit address */
    } else {
        MemfPutc(mobjf, 'C');       /* or a 32bit constant */
    }
    MemObjFileWriteLong (mobjf, node->symvalue);

    b = strlen (node->symname);
    MemfPutc(mobjf, (int) b);               /* write length of symbol name to relocatable file */
    MemfWrite(mobjf, node->symname, b);     /* write symbol name to relocatable file */
}


static void
ObjFileInstallSymbol(avltree_t **symbols, const char *symname, symvalue_t symval, unsigned long symtype, module_t *symowner)
{
    char rtmmsg[256];
    symbol_t *foundsymbol = FindSymbol (symname, *symbols);

    if (foundsymbol == NULL) {
        foundsymbol = CreateSymbol (symname, symval, symtype, symowner);
        if (foundsymbol != NULL && !Insert (symbols, foundsymbol, (compfunc_t) cmpidstr)) {
            FreeSym(foundsymbol);
            ReportError (NULL, Err_Memory);
        }
    } else {
        foundsymbol->symvalue = symval;
        foundsymbol->type |= symtype;
        foundsymbol->owner = symowner;

        snprintf (rtmmsg, 256, "Symbol <%s> redefined in module '%s'\n", symname, symowner->mname);
        ReportRuntimeErrMsg (NULL, rtmmsg);
    }
}


/*!
 * \brief Check Mpm Object or Library File Watermark and return file level
 * \param string pointer to first 8 bytes of loaded Mpm object or library file
 * \param watermark pointer to Mpm default watermark
 * \return file level of Mpm Object/library File format or -1 if watermark is unknown
 */
static int
ObjFileCheckWaterMark(const char *string, const char *watermark, size_t sizeofWaterMark)
{
    int filelevel = -1;
    char str[3];

    if (memcmp (string, watermark, sizeofWaterMark-2) == 0) {
        memcpy(str, watermark+sizeofWaterMark-2, 2);
        filelevel = atoi(str);
    }

    return filelevel;
}



/******************************************************************************
                               Public functions
 ******************************************************************************/


/*!
 * \brief Cache object or library file from hosting filing system as memory file
 * \param filename
 * \return return pointer to cached file, or NULL if it failed.
 */
memfile_t *
CacheObjFile(char *filename, const pathlist_t *pathlist)
{
    FILE *objf;
    memfile_t *mf;

    if ((objf = OpenFile (AdjustPlatformFilename(filename), pathlist, false)) == NULL) {
        ReportIOError (filename);
        return NULL;
    }

    if ( (mf = MemfNew(filename, 0, 0)) == NULL) {
        fclose (objf);
        ReportError (NULL, Err_Memory);
        return NULL;
    }

    if (MemfCacheHostFile (mf, objf) == NULL) {
        /* failed to cache object file from host filing system */
        MemfFree(mf);
        mf = NULL;
    }

    /* we're done with the host file handle */
    fclose (objf);
    return mf;
}


/*!
 * \brief Get object (memory) file via expression that links to current module
 * \param node
 * \return pointer to object memory file, or NULL if there no (memory) object file
 */
memfile_t *
GetExprMemObjfile(const expression_t *pfixexpr)
{
    if (pfixexpr == NULL)
        return NULL;
    if (pfixexpr->srcfile == NULL)
        return NULL;
    if (pfixexpr->srcfile->module == NULL)
        return NULL;

    return pfixexpr->srcfile->module->objfile;
}


/*!
 * \brief Validate Mpm Library File Watermark and return file level
 * \param watermark pointer to first 8 bytes of loaded Mpm library file
 * \return file level of Mpm Library File format or -1 if watermark is unknown
 */
int
LibFileWaterMark(const char *watermark)
{
    return ObjFileCheckWaterMark(watermark, MPMLIBRARYHEADER, SIZEOF_MPMLIBHDR);
}


/*!
 * \brief Validate Mpm Object File Watermark and return file level
 * \param watermark pointer to first 8 bytes of loaded Mpm object file
 * \return file level of Mpm Object File format or -1 if watermark is unknown
 */
int
ObjFileWaterMark(const char *watermark)
{
    return ObjFileCheckWaterMark(watermark, MPMOBJECTHEADER, SIZEOF_MPMOBJHDR);
}


/*!
 * \brief Create (memory) object template with host filename, preset with object file watermark
 * \details As parsing and code generation progresses, the object file is streamed with contents
 * \param objfname
 * \return
 */
memfile_t *
MemObjFileCreate(char *objfname)
{
    /* object file output is with initial 1K space, incremented by 1K block */
    memfile_t *mobjf = MemfNew (AdjustPlatformFilename(objfname), 1024, 1024);

    if (mobjf == NULL) {
        ReportRuntimeErrMsg (NULL, "Failed to create object memory file");
    } else {
        /* Create relocatable object file with watermark and header */
        MemfWrite(mobjf, MPMOBJECTHEADER, strlen (MPMOBJECTHEADER));
        MemfWrite(mobjf, objhdrprefix, strlen (objhdrprefix));
    }

    return mobjf;
}


/*!
 * \brief Flush contents of object memory file to host file system
 * \details
 * The host filename is embedded in the memory file structure. If any
 * I/O error happens during file flushing, it is reported to the
 * global error system.
 *
 * \param mf pointer to memory file
 */
void
MemObjFileFlush(const memfile_t *mf)
{
    if (mf != NULL && mf->filesize > 0) {
        WriteBuffer2HostFile(mf->fname, "w", mf->filedata, (size_t) mf->filesize);
    }
}


/*!
 * \brief Open & cache as memory file, then evaluate if it is an Mpm Object File
 * \details
 * If successfully validated, the new (loaded from host file system) memory file is
 * returned to the caller, and a pointer to the object file watermark type string is returned.
 * The file pointer has been positioned at the first byte after the watermark
 * (it points at the object module ORIGIN).
 *
 * If the host object file couldn't be opened or is not recognized,
 * a NULL file handle and NULL watermark is returned.
 * The routine also reports errors to the global error system for file I/O and
 * unrecognized object file.
 * \param filename
 * \param objwatermark
 * \param objlevel
 * \return pointer to allocated & loaded memory (object) file
 */
memfile_t *
MemObjFileOpen(char *filename, const char **objwatermark, int *objlevel)
{
    memfile_t *mf;
    char watermark[SIZEOF_MPMOBJHDR+1];

    if ((mf = CacheObjFile(filename, NULL)) == NULL) {
        ReportIOError (filename);
        *objwatermark = NULL;
        return NULL;
    }

    /* try to read Mpm object file watermark */
    if (MemfRead (mf, watermark, SIZEOF_MPMOBJHDR) == SIZEOF_MPMOBJHDR) {
        watermark[SIZEOF_MPMOBJHDR] = '\0';

        if ( (*objlevel = ObjFileWaterMark(watermark)) != -1) {
            /* found Mpm object file matching default release version */
            *objwatermark = MPMOBJECTHEADER;
            return mf;
        }
    }

    /* object file was not recognized */
    MemfFree(mf);
    ReportRuntimeErrMsg (filename, GetErrorMessage(Err_Objectfile));
    *objwatermark = NULL;
    return NULL;
}


/*!
 * \brief read signed 32bit integer in LSB format from memory object file at current file pointer
 * \param pointer to memory object file "handle"
 * \return
 */
long
MemObjFileReadLong (memfile_t *mf)
{
    long fptr = 0;

    if (BIGENDIAN == true) {
        /* load integer as LSB order into MSB order internally */
        for (int i = 1; i <= 3; i++) {
            fptr |= MemfGetc (mf) << 24;
            fptr >>= 8;
        }
        fptr |= MemfGetc (mf) << 24;
    } else {
        /* default host architecture is LSB order... */
        MemfRead (mf, (char *) &fptr, 4);
    }

    return fptr;
}


/*!
 * \brief Write signed 32bit integer to memory object file in LSB format
 * \param int32
 * \param fileid
 */
void
MemObjFileWriteLong (memfile_t *mobjf, long int32)
{
    /* if no object file, abort mission */
    if (mobjf == NULL)
        return;

    if (BIGENDIAN == true) {
        for (int i = 0; i < 4; i++) {
            MemfPutc(mobjf, int32 & 255);
            int32 >>= 8;
        }
    } else {
        /* low byte, high byte order... */
        MemfWrite(mobjf, (const char *) &int32, 4);
    }
}


/*!
 * \brief Fetch a string at current file position in the memory object file
 * \details format is <length><stringsequence>. The string is truncated at MAX_NAME_SIZE length (see config.h).
 * \param mobjf pointer to memory object file
 * \param[out] string pointer to string buffer (MAX_NAME_SIZE length)
 */
void
MemObjFileReadString (memfile_t *mobjf, char *const string)
{
    int strlength = MemfGetc (mobjf);

    if (strlength > MAX_NAME_SIZE) {
        strlength = MAX_NAME_SIZE;    /* truncate name to avoid buffer overflow */
    }

    strlength = MemfRead(mobjf, string, (size_t) strlength);
    string[strlength] = '\0';
}


/*!
 * \brief Store expression (infix notation) to (memory) object file
 * \details
 * The Expression contains symbols declared as external or defined
 * as a relocatable address.
 * \param pfixexpr
 * \param exprtype the result type of the expression (for range check)
 */
void
MemObjFileWriteExpr (expression_t *pfixexpr, const unsigned char exprtype)
{
    unsigned char b;
    memfile_t *mobjf = GetExprMemObjfile(pfixexpr);

    if (pfixexpr->wrobjfile == false && mobjf != NULL) {
        /* this expression is only written once to object file */
        pfixexpr->wrobjfile = true;

        /* type of expression */
        MemfPutc(mobjf, exprtype);

        if (atoi(MPMOBJECTVERSION) >= 3) {
            /* write $PC as it is defined for expression */
            MemObjFileWriteLong (mobjf, pfixexpr->pc);
        }

        /* write patch pointer inside code module */
        MemObjFileWriteLong (mobjf, (long) pfixexpr->codepos);

        /* length prefixed string */
        b = (unsigned char) strlen (pfixexpr->infixexpr);
        MemfPutc(mobjf, b);
        MemfWrite(mobjf, pfixexpr->infixexpr, b);

        /* null-terminate expression */
        MemfPutc(mobjf, 0);
    }
}


void
MemObjFileWriteHeader(sourcefile_t *csfile, memfile_t *mobjf, size_t moduleCodesize)
{
    int modnamelen;
    long fptr_exprdecl;
    long fptr_namedecl;
    long fptr_modname;
    long fptr_modcode;
    long fptr_libnmdecl;

    if (csfile != NULL && csfile->module != NULL && mobjf != NULL) {
        /* Write parsed data structures to object file */
        fptr_namedecl = MemfTell (mobjf);

        /* Store Local Name declarations to object file */
        InOrder (csfile->module->localsymbols, (void (*)(void *)) ObjFileStoreLocalName);

        /* Store Global name declarations to object file */
        InOrder (globalsymbols, (void (*)(void *)) ObjFileStoreGlobalName);

        /* Store library reference name declarations to object file */
        fptr_libnmdecl = MemfTell (mobjf);
        InOrder (globalsymbols, (void (*)(void *)) ObjFileStoreLibReference);

        fptr_modname = MemfTell (mobjf);
        if (csfile->module->mname != NULL) {
            modnamelen = (int) strlen (csfile->module->mname);

            /* write length of module name to object file */
            MemfPutc (mobjf, modnamelen);
            /* write module name to object file */
            MemfWrite(mobjf, csfile->module->mname, (size_t) modnamelen);
        } else {
            /* no module name defined */
            MemfPutc (mobjf, 0);
        }

        if (moduleCodesize == 0) {
            fptr_modcode = 0xffffffff;    /* no code generated! */
        } else {
            fptr_modcode = MemfTell (mobjf);
            /* write module code size (32bit) */
            MemObjFileWriteLong (mobjf, (long) moduleCodesize);
            /* then the actual binary code */
            MemfWrite(mobjf, (const char *) csfile->module->csection->area, moduleCodesize);
        }

        MemfSeek (mobjf, SIZEOF_MPMOBJHDR);  /* set file pointer to point at ORG (just after watermark) */
        if ((modules->first == csfile->module) && (deforigin == true)) {
            csfile->module->origin = EXPLICIT_ORIGIN;      /* use origin from command line */
        }
        MemObjFileWriteLong (mobjf, (long) csfile->module->origin);    /* Write Origin (32bit) */

        fptr_exprdecl = SIZEOF_MPMOBJHDR + 4+4+4+4+4+4;       /* distance to expression section... */

        if (fptr_namedecl == fptr_exprdecl) {
            fptr_exprdecl = 0xffffffff;     /* no expressions */
        }
        if (fptr_libnmdecl == fptr_namedecl) {
            fptr_namedecl = 0xffffffff;     /* no name declarations */
        }
        if (fptr_modname == fptr_libnmdecl) {
            fptr_libnmdecl = 0xffffffff;    /* no library reference declarations */
        }

        MemObjFileWriteLong (mobjf, fptr_modname);    /* write fptr. to module name */
        MemObjFileWriteLong (mobjf, fptr_exprdecl);   /* write fptr. to name declarations */
        MemObjFileWriteLong (mobjf, fptr_namedecl);   /* write fptr. to name declarations */
        MemObjFileWriteLong (mobjf, fptr_libnmdecl);  /* write fptr. to library name declarations */
        MemObjFileWriteLong (mobjf, fptr_modcode);    /* write fptr. to module code */
    }
}


/**
 * @brief Validate range of expression according to object file type
 * @details If the range is not valid or unknown, report a global error, according to current source file
 * @param csfile
 * @param exprvalue
 * @param exprtype
 */
bool
ObjFileExpressionRange(sourcefile_t *csfile, long exprvalue, unsigned char exprtype)
{
    int defRangeError = Err_IntegerRange;

    switch(exprtype) {
        case RANGE_8SIGN:
            if ( (exprvalue >= -128) && (exprvalue <= 127) ) {
                return true;
            }
            break;

        case RANGE_PCREL8:
            if ( (exprvalue >= -128) && (exprvalue <= 127) ) {
                /* The jump is measured from the address of the instruction op code and contains a range of -126 to +129 bytes */
                return true;
            }

            defRangeError = Err_PcRelRange;
            break;

        case RANGE_8UNSIGN:
            if ( (exprvalue >= -128) && (exprvalue <= 255) ) {
                return true;
            }
            break;

        case RANGE_LSB_16SIGN:
        case RANGE_MSB_16SIGN:
            if ( (exprvalue >= -32768 ) && (exprvalue <= 32767) ) {
                return true;
            }
            break;

        case RANGE_LSB_PCREL16:
        case RANGE_MSB_PCREL16:
            if ( (exprvalue >= -32768 ) && (exprvalue <= 32767) ) {
                return true;
            }

            defRangeError = Err_PcRelRange;
            break;

        case RANGE_LSB_16CONST:
        case RANGE_MSB_16CONST:
            if ( (exprvalue >= -32768) && (exprvalue <= 65535) ) {
                return true;
            }
            break;

        case RANGE_LSB_24SIGN:
        case RANGE_MSB_24SIGN:
            if ( (exprvalue >= -8388608 ) && (exprvalue <= 8388607) ) {
                return true;
            }
            break;

        case RANGE_LSB_PCREL24:
        case RANGE_MSB_PCREL24:
            if ( (exprvalue >= -8388608 ) && (exprvalue <= 8388607) ) {
                return true;
            }
            defRangeError = Err_PcRelRange;
            break;

        case RANGE_LSB_32SIGN:
        case RANGE_MSB_32SIGN:
            if ( exprvalue >= -2147483648 && exprvalue <= 2147483647 ) {
                return true;
            }
            break;

        case RANGE_LSB_32CONST:
        case RANGE_MSB_32CONST:
            if (exprvalue < 0) {
                if (exprvalue >= -2147483648) {
                    return true;
                }
            } else {
                if ( (unsigned long) exprvalue <= 4294967295 ) {
                    return true;
                }
            }
            break;

        default:
            ReportSrcAsmMessage (csfile, "Unknown Expression Range Type");
            return false;
    }

    /* according to type, expression was outside valid range */
    ReportError (csfile, defRangeError);
    return false;
}


/*!
 * \brief Load all symbols from memory object file (or library) into module local or global symbols
 * \details Object file is already opened and points to first symbol name definition
 * \param mobjf pointer to memory object/library file
 * \param orgaddr the ORG (absolute base address) of the final output binary
 * \param nextname file pointer to symbol name section (first symbol name)
 * \param endnames file pointer to end of symbol name section (last symbol name)
 */
void
MemObjFileReadSymbols (module_t *curmodule, memfile_t *mobjf, size_t orgaddr, long nextname, long endnames)
{
    char scope;
    char symid;
    char symname[MAX_NAME_SIZE];
    unsigned long symtype = SYMDEFINED;
    symvalue_t value;
    symvalue_t symval;

    do {
        scope = (char) MemfGetc (mobjf);
        symid = (char) MemfGetc (mobjf);     /* type of name */
        value = MemObjFileReadLong (mobjf);
        MemObjFileReadString (mobjf, symname);
        nextname += 1 + 1 + 4 + 1 + strlen (symname);

        switch (symid) {
            case 'A':
                symtype = SYMADDR | SYMDEFINED;
                value += orgaddr + curmodule->startoffset;   /* Absolute address */
                break;

            case 'C':
                symtype = SYMDEFINED;
                break;

            default:
                ReportError (NULL, Err_UnknownIdent);
        }

        symval = value;

        switch (scope) {
            case 'L':
                ObjFileInstallSymbol(&curmodule->localsymbols, symname, symval, symtype | SYMLOCAL, curmodule);
                break;

            case 'G':
                ObjFileInstallSymbol(&globalsymbols, symname, symval, symtype | SYMXDEF, curmodule);
                break;

            case 'X':
                ObjFileInstallSymbol(&globalsymbols, symname, symval, symtype | SYMXDEF | SYMDEF, curmodule);
                break;

            default:
                ReportError (NULL, Err_UnknownIdent);
        }
    } while (nextname < endnames);
}


/**
 * @brief Load expression from current memory object file pointer
 *
 * @param cfile for error reporting
 * @param mobjf pointer to object memory file to load expression from
 * @param objfilelevel the MPM Oject file watermark level (see MPMOBJECTVERSION)
 * @param type return expression type (eg. 'U', 'J'...)
 * @param pc return assembler PC for expression
 * @param patchcodeidx return the relative patch code offset where expression result is updated
 * @param nextexpr return file pointer to next object file expression
 *
 * @return alloced memory file (actually just a string) containing the expression infix expression, or NULL
 */
memfile_t *
MemObjFileLoadExpr(
    sourcefile_t *cfile,
    memfile_t *mobjf,
    int objfilelevel,
    unsigned char *type,
    symvalue_t *pc,
    size_t *patchcodeidx,
    long *nextexpr)
{
    int        i;
    memfile_t  *mf;
    char       mexpr[MAX_EXPR_SIZE+1];

    *type = (unsigned char) MemfGetc (mobjf);

    if (objfilelevel >= 3) {
        /* fetch $PC (defined in Mpm object file level 03 and later */
        *pc = MemObjFileReadLong (mobjf);
    }

    *patchcodeidx = (size_t) MemObjFileReadLong (mobjf);
    if (objfilelevel < 3) {
        /* object file levels 01 and 02 defines $PC = codepos */
        *pc = *patchcodeidx;

        /* type (8bit) + patchcodeidx (32bit) + length of string (8bit) */
        *nextexpr += 1 + 4 + 1;
    } else {
        /* type (8bit) + pc (32bit) + patchcodeidx (32bit) + length of string (8bit) */
        *nextexpr += 1 + 4 + 4 + 1;
    }

    /* get length of infix expression + terminator */
    i = MemfGetc (mobjf) + 1;
    /* load infix expression into file data for parsing */
    if ( (mf = MemfNew(NULL, i+1, 0)) == NULL) {
        ReportError (cfile, Err_Memory);
        return NULL;
    }

    if (i > MAX_EXPR_SIZE) {
        ReportError (cfile, Err_Memory);
        MemfFree(mf);
        return NULL;
    }

    if (MemfRead (mobjf, mexpr, (size_t) i) != i) {
        ReportError (cfile, Err_FileIO);
        MemfFree(mf);
        return NULL;
    }
    mexpr[i] = '\0'; /* expression successfully loaded */
    /* added length of infix expression string, to know file pointer to next expression in objfile */
    *nextexpr += i;

    /* copy (I/O) write experssion string into temporary file */
    if ((MemfWrite(mf, mexpr, (size_t) i)) == MFEOF) {
        ReportError (cfile, Err_FileIO);
        MemfFree(mf);
        return NULL;
    }

    /* set file pointer to beginning of expression of temporary file */
    MemfSeek(mf, 0);

    return mf;
}
