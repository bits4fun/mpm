/***************************************************************************************************
 *
 *  MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
 *   MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
 *   MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
 *   MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *  MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
 *
 * Copyright (C) 1991-2024, Gunther Strube, hello@bits4fun.net
 *
 * This file is part of Mpm.
 * Mpm is free software; you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation - either version 2, or (at your option)
 * any later version.
 * Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Mpm -
 * see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ***************************************************************************************************/


#if ! defined MPM_OPTIONS_HEADER_
#define MPM_OPTIONS_HEADER_

#include <stdbool.h>

/* global variable declarations */
extern const char copyrightmsg[];
extern const char buildmsg[];

extern bool assembleupdates;
extern bool verbose;
extern bool useothersrcext;
extern bool writesymtable;
extern bool z80autorelocate;
extern bool z80reltablefile;
extern bool crc32file;
extern bool writeihexfile;
extern bool writesha256file;
extern bool writelistingfile;
extern bool uselistingfile;
extern bool createlibrary;
extern bool uselibraries;
extern bool link2binaryfile;
extern bool writeline;
extern bool writeexpandedmacro;
extern bool addressalign;
extern bool clinemode;
extern bool BIGENDIAN;
extern bool USEBIGENDIAN;
extern bool deforigin;
extern bool z80codesegment;
extern bool expl_binflnm;
extern bool writeobjectfile;
extern bool writeaddrmapfile;
extern bool writeglobaldeffile;
extern bool validaterefs;
extern bool asmerror;
extern unsigned long EXPLICIT_ORIGIN;

/* global functions */
void prompt (void);
void SetAsmFlag (char *flagid);
void DefaultOptions (int argc, char *argv[]);
void display_options (void);

#endif
