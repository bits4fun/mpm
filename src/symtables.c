/***************************************************************************************************
 *
 *  MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
 *   MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
 *   MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
 *   MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *  MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
 *
 * Copyright (C) 1991-2024, Gunther Strube, hello@bits4fun.net
 *
 * This file is part of Mpm.
 * Mpm is free software; you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation - either version 2, or (at your option)
 * any later version.
 * Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Mpm -
 * see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ***************************************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "config.h"
#include "datastructs.h"
#include "avltree.h"
#include "errors.h"
#include "prsline.h"
#include "options.h"
#include "symtables.h"



/******************************************************************************
                          Global variables
 ******************************************************************************/

avltree_t *globalsymbols = NULL;
avltree_t *staticsymbols = NULL;
symbol_t *gAsmpcPtr;        /* pointer to Assembler PC symbol, '$PC' */



/******************************************************************************
                          Local functions and variables
 ******************************************************************************/

static symbol_t *DefLocalSymbol (sourcefile_t *csfile, const char *identifier, symvalue_t value, unsigned long symboltype);
static symbol_t *AllocSymbol (void);


static symbol_t *
DefLocalSymbol (sourcefile_t *csfile,
                const char *identifier,
                symvalue_t value,           /* value of symbol, label */
                unsigned long symboltype)   /* symbol is either address label or constant */
{
    symbol_t *foundsymbol;

    if ((foundsymbol = FindSymbol (identifier, csfile->module->localsymbols)) == NULL) {
        /* symbol not declared as local */
        foundsymbol = CreateSymbol (identifier, value, symboltype | SYMLOCAL | SYMDEFINED, csfile->module);
        if (foundsymbol == NULL) {
            return NULL;
        } else {
            if (!Insert (&csfile->module->localsymbols, foundsymbol, (compfunc_t) cmpidstr)) {
                ReportError (NULL, Err_Memory);
                FreeSym(foundsymbol);
                return NULL;
            }
        }

        return foundsymbol;
    } else if ((foundsymbol->type & SYMDEFINED) == 0) {
        /* symbol declared local, but not yet defined */
        foundsymbol->symvalue = value;
        /* local symbol type set to address label or constant */
        foundsymbol->type |= symboltype | SYMLOCAL | SYMDEFINED;

        foundsymbol->owner = csfile->module;   /* owner of symbol is always creator */
        return foundsymbol;
    } else {
        ReportError (csfile, Err_SymDefined);  /* local symbol already defined */
        return NULL;
    }
}



static symbol_t *
AllocSymbol (void)
{
    return (symbol_t *) malloc (sizeof (symbol_t));
}



/******************************************************************************
                               Public functions
 ******************************************************************************/

bool
CreateAsmPc(sourcefile_t *csfile)
{
    gAsmpcPtr = DefineDefSym (csfile, ASSEMBLERPC, 0, &globalsymbols);      /* Create standard '$PC' identifier */
    if (gAsmpcPtr == NULL) {
        ReportRuntimeErrMsg (NULL, "Failed to allocate $PC identifier");
        return false;
    } else {
        /* Establish the type of the $PC (it's a global relocatable address) */
        gAsmpcPtr->type |= SYMFUNC | SYMDEF | SYMADDR | SYMXDEF;
        return true;
    }
}


symbol_t *
CreateSymNode (const symbol_t *symptr)
{
    if (symptr != NULL)
        return CreateSymbol (symptr->symname, symptr->symvalue, symptr->type, symptr->owner);
    else
        return NULL;
}


symbol_t *
CreateSymbol (const char *identifier, symvalue_t value, unsigned long symboltype, module_t *symowner)
{
    symbol_t *newsym;

    if ((newsym = AllocSymbol ()) == NULL) {
        /* Create area for a new symbol structure */
        ReportRuntimeErrMsg (NULL, "Failed to allocate new symbol");
        return NULL;
    }
    newsym->symname = strclone(identifier);  /* Allocate area for a new symbol identifier */
    if (newsym->symname == NULL) {
        free (newsym);        /* Ups no more memory left.. */
        ReportRuntimeErrMsg (NULL, "Failed to allocate symbol identifier");
        return NULL;
    }

    newsym->owner = symowner;
    newsym->type = symboltype;
    newsym->symvalue = value;

    return newsym;        /* pointer to new symbol node */
}


int
cmpidstr (const symbol_t *kptr, const symbol_t *p)
{
    return strcmp (kptr->symname, p->symname);
}


int
cmpidval (const symbol_t *kptr, const symbol_t *p)
{
    return (int) (kptr->symvalue - p->symvalue);
}


/*
 * DefineSymbol will create a record in memory, inserting it into an AVL tree (or creating the first record)
 */
symbol_t *
DefineSymbol (sourcefile_t *csfile,
              const char *identifier,
              symvalue_t value,         /* value of symbol, label */
              unsigned long symboltype) /* symbol is either address label or constant */
{
    symbol_t *foundsymbol;

    if ((foundsymbol = FindSymbol (identifier, globalsymbols)) == NULL) { /* symbol not declared as global/extern */
        return DefLocalSymbol (csfile, identifier, value, symboltype);
    } else if (foundsymbol->type & SYMXDEF) {
        if ((foundsymbol->type & SYMDEFINED) == 0) {
            /* symbol declared global, but not yet defined */
            foundsymbol->symvalue = value;
            /* defined, and typed as address label or constant */
            foundsymbol->type |= (symboltype | SYMDEFINED);

            foundsymbol->owner = csfile->module;   /* owner of symbol is always creator */
            return foundsymbol;
        } else {
            ReportError (csfile, Err_SymDefined);  /* global symbol already defined */
            return NULL;
        }
    } else {
        /* Symbol was already declared Extern, but now defined as a local symbol. */
        ReportError (csfile, Err_SymDeclExtern);
        return DefLocalSymbol (csfile, identifier, value, symboltype);
    }

    /* the extern symbol is now no longer accessible */
}


/*
 * search for symbol in either local tree or global tree, return found pointer if defined/declared, otherwise return
 * NULL
 */
symbol_t *
GetSymPtr (sourcefile_t *csfile, const char *identifier)
{
    symbol_t *symbolptr = FindSymbol (identifier, csfile->module->localsymbols);

    if (symbolptr != NULL) {
        /* symbol declared local in source code */
        return symbolptr;
    }

    if ((symbolptr = FindSymbol (identifier, globalsymbols)) != NULL) {
        /* symbol declared global, accessible for all modules */
        return symbolptr;
    }

    if (writesymtable == true && writelistingfile == true) {
        if (FindSymbol (identifier, csfile->module->notdeclsymbols) == NULL) {
            if ((symbolptr = CreateSymbol (identifier, 0, SYM_NOTDEFINED, csfile->module)) != NULL) {
                if (!Insert (&csfile->module->notdeclsymbols, symbolptr, (compfunc_t) cmpidstr)) {
                    ReportError (NULL, Err_Memory);
                    FreeSym(symbolptr);
                }
            }
        }
    }

    return NULL;
}


int
compidentifier (const char *identifier, const symbol_t *p)
{
    return strcmp (identifier, p->symname);
}


/*
 * return pointer to found symbol in a symbol tree, otherwise NULL if not found
 */
symbol_t *
FindSymbol (const char *identifier,   /* pointer to current identifier */
            const avltree_t * treeptr)  /* pointer to root of AVL tree */
{
    symbol_t *found;

    if (treeptr == NULL) {
        return NULL;
    } else {
        found = Find (treeptr, identifier, (compfunc_t) compidentifier);
        if (found == NULL) {
            return NULL;
        } else {
            found->type |= SYMTOUCHED;
            return found;     /* symbol found (declared/defined) */
        }
    }
}


void
DeclSymGlobal (sourcefile_t *csfile, const char *identifier, unsigned long libtype)
{
    symbol_t *foundsym;
    symbol_t *clonedsym;
    symvalue_t symval;

    if ((foundsym = FindSymbol (identifier, csfile->module->localsymbols)) == NULL) {
        if ((foundsym = FindSymbol (identifier, globalsymbols)) == NULL) {
            symval = 0;
            foundsym = CreateSymbol (identifier, symval, SYM_NOTDEFINED | SYMXDEF | libtype, csfile->module);
            if (foundsym != NULL) {
                if (!Insert (&globalsymbols, foundsym, (compfunc_t) cmpidstr)) {    /* declare symbol as global */
                    ReportError (NULL, Err_Memory);
                    FreeSym(foundsym);
                }
            }
        } else {
            if (foundsym->owner != csfile->module) {
                /* this symbol is declared in another module */
                if (foundsym->type & SYMXREF) {
                    foundsym->owner = csfile->module;  /* symbol now owned by this module */
                    foundsym->type &= XREF_OFF;       /* re-declare symbol as global if symbol was */
                    foundsym->type |= SYMXDEF | libtype;  /* declared extern in another module */
                } else {                          /* cannot declare two identical global's */
                    ReportError (csfile, Err_SymDeclGlobalModule);    /* Already declared global */
                }
            } else {
                ReportError (csfile, Err_SymRedeclaration);    /* re-declaration not allowed */
            }
        }
    } else {
        if (FindSymbol (identifier, globalsymbols) == NULL) {
            /* If no global symbol of identical name has been created, then re-declare local symbol as global symbol */
            foundsym->type &= SYMLOCAL_OFF;
            foundsym->type |= SYMXDEF;
            clonedsym = CreateSymbol (foundsym->symname, foundsym->symvalue, foundsym->type, csfile->module);
            if (clonedsym != NULL) {
                if (Insert (&globalsymbols, clonedsym, (compfunc_t) cmpidstr)) {
                    /* original local symbol cloned as global symbol, now delete old local ... */
                    DeleteNode (&csfile->module->localsymbols, foundsym, (compfunc_t) cmpidstr, (void (*)(void *)) FreeSym);
                } else {
                    ReportError (NULL, Err_Memory);
                    FreeSym(clonedsym);
                }
            }
        } else {
            ReportError (csfile, Err_SymDeclGlobal);    /* already declared global */
        }
    }
}


void
DeclSymExtern (sourcefile_t *csfile, const char *identifier, unsigned long libtype)
{
    symbol_t *foundsym;
    symbol_t *extsym;
    symvalue_t symval;

    if ((foundsym = FindSymbol (identifier, csfile->module->localsymbols)) == NULL) {
        if ((foundsym = FindSymbol (identifier, globalsymbols)) == NULL) {
            symval = 0;
            foundsym = CreateSymbol (identifier, symval, SYM_NOTDEFINED | SYMXREF | libtype, csfile->module);
            if (foundsym != NULL) {
                if (!Insert (&globalsymbols, foundsym, (compfunc_t) cmpidstr)) {    /* declare symbol as extern */
                    ReportError (NULL, Err_Memory);
                    FreeSym(foundsym);
                }
            }
        } else if (foundsym->owner == csfile->module) {
            ReportError (csfile, Err_SymRedeclaration);    /* Re-declaration not allowed */
        }
    } else {
        if (FindSymbol (identifier, globalsymbols) == NULL) {
            /* If no external symbol of identical name has been declared, then re-declare local
               symbol as external symbol, but only if local symbol is not defined yet */
            if ((foundsym->type & SYMDEFINED) == 0) {
                symval = 0;
                foundsym->type &= SYMLOCAL_OFF;
                foundsym->type |= (SYMXREF | libtype);
                extsym = CreateSymbol (identifier, symval, foundsym->type, csfile->module);
                if (extsym != NULL) {
                    if (Insert (&globalsymbols, extsym, (compfunc_t) cmpidstr)) {
                        /* original local symbol cloned as external symbol, now delete old local ... */
                        DeleteNode (&csfile->module->localsymbols, foundsym, (compfunc_t) cmpidstr, (void (*)(void *)) FreeSym);
                    } else {
                        ReportError (NULL, Err_Memory);
                        FreeSym(extsym);
                    }
                }
            } else {
                ReportError (csfile, Err_SymDeclLocal);    /* already declared local */
            }
        } else {
            ReportError (csfile, Err_SymRedeclaration);    /* re-declaration not allowed */
        }
    }
}


symbol_t *
DefineDefSym (sourcefile_t *csfile, const char *identifier, long value, avltree_t ** root)
{
    symbol_t *staticsym;
    symvalue_t symval;

    if ( (staticsym = FindSymbol (identifier, *root)) == NULL) {
        symval = value;
        staticsym = CreateSymbol (identifier, symval, SYMDEF | SYMDEFINED, (csfile != NULL) ? csfile->module: NULL);
        if (staticsym != NULL) {
            if (Insert (root, staticsym, (compfunc_t) cmpidstr)) {
                return staticsym;
            } else {
                FreeSym(staticsym);
                return NULL;
            }
        } else {
            return NULL;
        }
    } else {
        ReportError (csfile, Err_SymDefined);  /* symbol already defined */
        return staticsym;
    }
}


char *
AllocIdentifier (size_t len)
{
    return (char *) calloc (len, sizeof(char));
}


void
FreeIdentifier (char **string)
{
    if (*string != NULL) {
        free (*string);
        *string = NULL;
    }
}


void
FreeSym (symbol_t *node)
{
    if (node == NULL)
        return;

    if (node->symname != NULL) {
        free (node->symname);    /* release symbol identifier */
    }

    free (node);          /* then release the symbol record */
}
