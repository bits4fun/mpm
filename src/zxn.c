/****************************************************************************************************
 *
 *   MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
 *    MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
 *    MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
 *    MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
 *    MMMM       MMMM     PPPP              MMMM       MMMM
 *    MMMM       MMMM     PPPP              MMMM       MMMM
 *   MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
 *
 *                         ZZZZZZZZZZZZZZ     888888888888       000000000       NNNNN       NNNNN
 *                       ZZZZZZZZZZZZZZ     8888888888888888    0000000000000     NNNNNN     NNNN
 *                               ZZZZ       8888        8888  0000         0000   NNNNNNNN   NNNN
 *                             ZZZZ           888888888888    0000         0000   NNNN NNNNN NNNN
 *                           ZZZZ           8888        8888  0000         0000   NNNN   NNNNNNNN
 *                         ZZZZZZZZZZZZZZ   8888888888888888    0000000000000     NNNN     NNNNNN
 *                       ZZZZZZZZZZZZZZ       888888888888        000000000      NNNNNN      NNNNN
 *
 * Copyright (C) 1991-2024, Gunther Strube, hello@bits4fun.net
 *
 * This file is part of Mpm.
 * Mpm is free software; you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation - either version 2, or (at your option)
 * any later version.
 * Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Mpm -
 * see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ****************************************************************************************************/


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "config.h"
#include "datastructs.h"
#include "exprprsr.h"
#include "asmdrctv.h"
#include "errors.h"
#include "pass.h"
#include "prsline.h"
#include "memfile.h"
#include "z80.h"
#include "zxn.h"


/******************************************************************************
                          Local functions and variables
 ******************************************************************************/
static void LDIRX (const sourcefile_t *csfile);
static void LDDRX (const sourcefile_t *csfile);
static void LDPIRX (const sourcefile_t *csfile);
static void LDDX (const sourcefile_t *csfile);
static void LDIX (const sourcefile_t *csfile);
static void LDWS (const sourcefile_t *csfile);
static void OUTINB (const sourcefile_t *csfile);
static void PIXELAD (const sourcefile_t *csfile);
static void PIXELDN (const sourcefile_t *csfile);
static void SETAE (const sourcefile_t *csfile);
static void SWAPNIB (const sourcefile_t *csfile);
static void MIRROR (sourcefile_t *csfile);
static void MUL (sourcefile_t *csfile);
static void TEST (sourcefile_t *csfile);
static void Bxxx (sourcefile_t *csfile, int opcode);
static void BSLA (sourcefile_t *csfile);
static void BSRA (sourcefile_t *csfile);
static void BSRL (sourcefile_t *csfile);
static void BSRF (sourcefile_t *csfile);
static void BRLC (sourcefile_t *csfile);
static void NEXTREG (sourcefile_t *csfile);
static void PUSH_zxn (sourcefile_t *csfile);
static void ADD_zxn (sourcefile_t *csfile);
static void JP_zxn (sourcefile_t *csfile);
static void ADD_zxn_bcdehl (sourcefile_t *csfile, enum z80rr dst16);

/* ------------------------------------------------------------------------------
   Pre-sorted array of Z80N (ZXN) instruction mnemonics (includes Z80 mnemonics and Z80 meta instructions).
   ------------------------------------------------------------------------------ */
static idf_t zxnidents[] = {
    {"ADC", {.mpf = ADC}},
    {"ADD", {.mpf = ADD_zxn}},
    {"AND", {.mpf = AND}},
    {"BIT", {.mpf = BIT}},
    {"BRLC", {.mpf = BRLC}},
    {"BSLA", {.mpf = BSLA}},
    {"BSRA", {.mpf = BSRA}},
    {"BSRF", {.mpf = BSRF}},
    {"BSRL", {.mpf = BSRL}},
    {"CALL", {.mpf = CALL}},
    {"CCF", {.cmpf = CCF}},
    {"CP", {.mpf = CP}},
    {"CPD", {.cmpf = CPD}},
    {"CPDR", {.cmpf = CPDR}},
    {"CPI", {.cmpf = CPI}},
    {"CPIR", {.cmpf = CPIR}},
    {"CPL",  {.cmpf = CPL}},
    {"DAA", {.cmpf = DAA}},
    {"DEC", {.mpf = DEC}},
    {"DI", {.cmpf = DI}},
    {"DJNZ", {.mpf = DJNZ}},
    {"EI", {.cmpf = EI}},
    {"EX", {.mpf = EX}},
    {"EXX", {.cmpf = EXX}},
    {"HALT", {.cmpf = HALT}},
    {"IM", {.mpf = IM}},
    {"IN", {.mpf = IN}},
    {"INC", {.mpf = INC}},
    {"IND", {.cmpf = IND}},
    {"INDR", {.cmpf = INDR}},
    {"INI", {.cmpf = INI}},
    {"INIR", {.cmpf = INIR}},
    {"JP", {.mpf = JP_zxn}},
    {"JR", {.mpf = JR}},
    {"LD", {.mpf = LD}},
    {"LDD", {.cmpf = LDD}},
    {"LDDR", {.cmpf = LDDR}},
    {"LDDRX", {.cmpf = LDDRX}},
    {"LDDX", {.cmpf = LDDX}},
    {"LDI", {.cmpf = LDI}},
    {"LDIR", {.cmpf = LDIR}},
    {"LDIRX", {.cmpf = LDIRX}},
    {"LDIX", {.cmpf = LDIX}},
    {"LDPIRX", {.cmpf = LDPIRX}},
    {"LDRX", {.cmpf = LDDRX}},      /* LDDRX alias */
    {"LDWS", {.cmpf = LDWS}},
    {"LIRX", {.cmpf = LDIRX}},      /* LDIRX alias */
    {"LPRX", {.cmpf = LDPIRX}},     /* LDPIRX alias */
    {"MIRR", {.mpf = MIRROR}},      /* MIRROR alias */
    {"MIRROR", {.mpf = MIRROR}},
    {"MUL", {.mpf = MUL}},
    {"NEG", {.cmpf = NEG}},
    {"NEXTREG", {.mpf = NEXTREG}},
    {"NOP", {.cmpf = NOP}},
    {"NOP", {.cmpf = NOP}},
    {"NREG", {.mpf = NEXTREG}},     /* NEXTREG alias */
    {"OR", {.mpf = OR}},
    {"OTDR", {.cmpf = OTDR}},
    {"OTIB", {.cmpf = OUTINB}},     /* OUTINB alias */
    {"OTIR", {.cmpf = OTIR}},
    {"OUT", {.mpf = OUT}},
    {"OUTD", {.cmpf = OUTD}},
    {"OUTI", {.cmpf = OUTI}},
    {"OUTINB", {.cmpf = OUTINB}},
    {"PIXELAD", {.cmpf = PIXELAD}},
    {"PIXELDN", {.cmpf = PIXELDN}},
    {"POP", {.mpf = POP}},
    {"PUSH", {.mpf = PUSH_zxn}},
    {"PXAD", {.cmpf = PIXELAD}},    /* PIXELAD alias */
    {"PXDN", {.cmpf = PIXELDN}},    /* PIXELDN alias */
    {"RES", {.mpf = RES}},
    {"RET", {.mpf = RET}},
    {"RETI", {.cmpf = RETI}},
    {"RETN", {.cmpf = RETN}},
    {"RL", {.mpf = RL}},
    {"RLA", {.cmpf = RLA}},
    {"RLC", {.mpf = RLC}},
    {"RLCA", {.cmpf = RLCA}},
    {"RLD", {.cmpf = RLD}},
    {"RLW", {.mpf = RLW}},          /* Z80 meta instruction */
    {"RR", {.mpf = RR}},
    {"RRA", {.cmpf = RRA}},
    {"RRC", {.mpf = RRC}},
    {"RRCA", {.cmpf = RRCA}},
    {"RRD", {.cmpf = RRD}},
    {"RRW", {.mpf = RRW}},          /* Z80 meta instruction */
    {"RST", {.mpf = RST}},
    {"SBC", {.mpf = SBC}},
    {"SCF", {.cmpf = SCF}},
    {"SET", {.mpf = SET}},
    {"SETAE", {.cmpf = SETAE}},
    {"SLA", {.mpf = SLA}},
    {"SLAW", {.mpf = SLAW}},        /* Z80 meta instruction */
    {"SLL", {.mpf = SLL}},
    {"SRA", {.mpf = SRA}},
    {"SRAW", {.mpf = SRAW}},        /* Z80 meta instruction */
    {"SRL", {.mpf = SRL}},
    {"SRLW", {.mpf = SRLW}},        /* Z80 meta instruction */
    {"STAE", {.cmpf = SETAE}},      /* SETAE alias */
    {"SUB", {.mpf = SUB}},
    {"SUBW", {.mpf = SUBW}},        /* meta instruction */
    {"SWAP", {.cmpf = SWAPNIB}},    /* SWAPNIB alias */
    {"SWAPNIB", {.cmpf = SWAPNIB}},
    {"TEST", {.mpf = TEST}},
    {"XOR", {.mpf = XOR}}
};


static void
SWAPNIB (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xED);
    StreamCodeByte (csfile, 0x23);
}


static void MIRROR (sourcefile_t *csfile)
{
    GetSym(csfile);
    if ( CheckReg8Mnem (csfile) != z80r_a ) {
        ReportError (csfile, Err_IllegalIdent);
        return;
    }

    StreamCodeByte (csfile, 0xED);
    StreamCodeByte (csfile, 0x24);
}


/*!
 * \brief TEST n instruction
 * \param csfile
 */
static void
TEST (sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xED);
    StreamCodeByte (csfile, 0x27);

    GetSym(csfile);
    ProcessExpr (csfile, NULL, RANGE_8UNSIGN, 2);
}


/*!
 * \brief BSLA | BSRA | BSRL | BSRF | BRLC DE,B
 * \param csfile
 * \param opcode
 */
static void Bxxx (sourcefile_t *csfile, int opcode)
{
    GetSym(csfile);
    if (CheckReg16Mnem (csfile) != z80r_de) {
        ReportError (csfile, Err_IllegalIdent);
        return;
    }
    if (GetSym(csfile) != comma) {
        ReportError (csfile, Err_Syntax);
        return;
    }
    GetSym(csfile);
    if ( CheckReg8Mnem (csfile) != z80r_b ) {
        ReportError (csfile, Err_IllegalIdent);
        return;
    }

    StreamCodeByte (csfile, 0xED);
    StreamCodeByte (csfile, (unsigned char) opcode);
}


static void BSLA (sourcefile_t *csfile)
{
    Bxxx(csfile, 0x28);
}


static void BSRA (sourcefile_t *csfile)
{
    Bxxx(csfile, 0x29);
}


static void BSRL (sourcefile_t *csfile)
{
    Bxxx(csfile, 0x2A);
}


static void BSRF (sourcefile_t *csfile)
{
    Bxxx(csfile, 0x2B);
}


static void BRLC (sourcefile_t *csfile)
{
    Bxxx(csfile, 0x2C);
}


/*!
 * \brief MUL D,E
 * \param csfile
 */
static void
MUL (sourcefile_t *csfile)
{
    GetSym(csfile);
    if ( CheckReg8Mnem (csfile) != z80r_d ) {
        ReportError (csfile, Err_IllegalIdent);
        return;
    }
    if (GetSym(csfile) != comma) {
        ReportError (csfile, Err_Syntax);
        return;
    }
    GetSym(csfile);
    if ( CheckReg8Mnem (csfile) != z80r_e ) {
        ReportError (csfile, Err_IllegalIdent);
        return;
    }

    StreamCodeByte (csfile, 0xED);
    StreamCodeByte (csfile, 0x30);
}


/*!
 * \brief PUSH nn | AF,BC,DE,HL,IX,IY
 * \param csfile
 */
static void
PUSH_zxn (sourcefile_t *csfile)
{
    enum z80rr qq;

    GetSym(csfile);
    do {
        switch (qq = CheckReg16Mnem (csfile)) {
            case z80r_af:
            case z80r_bc:
            case z80r_de:
            case z80r_hl:
            case z80r_ix:
            case z80r_iy:
                /* PUSH standard Z80 16bit registers */
                PushPop_instr (csfile, 197, qq);
                break;

            case z80rr_unknown:
                /* PUSH immediate constant expression */
                StreamCodeByte (csfile, 0xED);
                StreamCodeByte (csfile, 0x8A);
                ProcessExpr (csfile, NULL, RANGE_MSB_16CONST, 2);
                break;

            default:
                ReportError (csfile, Err_IllegalIdent);
                break;
        }

        if (GetSym(csfile) == comma)
            GetSym(csfile);

        /* stop parsing if a ':' (instruction separator) or end of line has been reached */
    } while (csfile->sym != newline && csfile->sym != colon);
}


static void
OUTINB (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xED);
    StreamCodeByte (csfile, 0x90);
}


/*!
 * \brief NEXTREG $im8, $im8  or NEXTREG $im8, A
 * \param csfile
 */
static void NEXTREG (sourcefile_t *csfile)
{
    int opcodeidx;

    StreamCodeByte (csfile, 0xED);
    /* get index to 2nd instruction opcode */
    opcodeidx = GetStreamCodePC(csfile);
    /* by default, generate NEXTREG $im8, $im8 */
    StreamCodeByte (csfile, 0x91);

    GetSym(csfile);
    ProcessExpr (csfile, NULL, RANGE_8UNSIGN, 2);

    if (csfile->sym == comma) {
        GetSym(csfile);
    } else {
        ReportError (csfile, Err_Syntax);
        return;
    }

    if ( CheckReg8Mnem (csfile) == z80r_a ) {
        /* NEXTREG $im8,A */
        UpdateStreamCodeByte (csfile, opcodeidx, 0x92);
    } else {
        ProcessExpr (csfile, NULL, RANGE_8UNSIGN, 3);
    }
}


static void
PIXELDN (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xED);
    StreamCodeByte (csfile, 0x93);
}


static void
PIXELAD (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xED);
    StreamCodeByte (csfile, 0x94);
}


static void
SETAE (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xED);
    StreamCodeByte (csfile, 0x95);
}


/*!
 * \brief JP (C), JP (HL), JP (IX), JP (IY), JP [cc],nn
 * \param csfile
 */
static void JP_zxn (sourcefile_t *csfile)
{
    if (GetSym(csfile) == lparen) {
        GetSym(csfile);
        if ( CheckReg8Mnem (csfile) == z80r_c ) {
            /* JP (C) */
            StreamCodeByte (csfile, 0xED);
            StreamCodeByte (csfile, 0x98);
            GetSym(csfile); /* Fetch ')' */
            return;
        } else {
            JP_IndReg16(csfile);
        }
    } else {
        /* no indirect register were found, point at expr. after 'JP' */
        CallJpZ80 (csfile, 195, 194);  /* base opcode for <instr> nn; <instr> cc, nn */
        ProcessExpr (csfile, NULL, RANGE_LSB_16CONST, 1);
    }
}


static void
LDIX (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xED);
    StreamCodeByte (csfile, 0xA4);
}


static void
LDWS (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xED);
    StreamCodeByte (csfile, 0xA5);
}


static void
LDDX (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xED);
    StreamCodeByte (csfile, 0xAC);
}


static void
LDIRX (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xED);
    StreamCodeByte (csfile, 0xB4);
}


static void
LDPIRX (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xED);
    StreamCodeByte (csfile, 0xB7);
}


static void
LDDRX (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xED);
    StreamCodeByte (csfile, 0xBC);
}


/*!
 * \brief ADD BC|DE|HL,A   or  ADD BC|DE|HL,nn  or  ADD HL,rr
 * \param csfile
 * \param dst16
 */
static void
ADD_zxn_bcdehl (sourcefile_t *csfile, enum z80rr dst16)
{
    enum z80rr src16;
    expression_t *im16;

    if (GetSym(csfile) != comma) {
        ReportError (csfile, Err_Syntax);
        return;
    }

    GetSym(csfile);
    if ( CheckReg8Mnem (csfile) == z80r_a ) {
        /* ZXN: ADD BC|DE|HL,A */
        StreamCodeByte (csfile, 0xED);
        switch(dst16) {
            case z80r_bc:
                StreamCodeByte (csfile, 0x33);
                return;
            case z80r_de:
                StreamCodeByte (csfile, 0x32);
                return;
            case z80r_hl:
                StreamCodeByte (csfile, 0x31);
                return;
            default:
                return;
        }
    }

    switch ((src16 = CheckReg16Mnem (csfile))) {
        case z80rr_unknown:
            /* ZXN: ADD BC|DE|HL,nn */
            im16 = ParseNumExpr (csfile);
            StreamCodeByte (csfile, 0xED);
            switch(dst16) {
                case z80r_bc:
                    StreamCodeByte (csfile, 0x36);
                    break;
                case z80r_de:
                    StreamCodeByte (csfile, 0x35);
                    break;
                case z80r_hl:
                    StreamCodeByte (csfile, 0x34);
                    break;
                default:
                    break;
            }
            ProcessExpr (csfile, im16, RANGE_LSB_16CONST, 2);
            return;

        case z80r_bc:
        case z80r_de:
        case z80r_hl:
        case z80r_sp:
            if (dst16 == z80r_hl)
                /* Z80: ADD HL,rr */
                StreamCodeByte (csfile, 9 + (unsigned char) src16 * 16);
            else
                ReportError (csfile, Err_IllegalIdent);
            return;

        default:
            ReportError (csfile, Err_IllegalIdent);
    }
}


static void
ADD_zxn (sourcefile_t *csfile)
{
    enum z80rr acc16;
    const unsigned char *ptr = MemfTellPtr(csfile->mf);

    GetSym(csfile);
    switch (acc16 = CheckReg16Mnem (csfile)) {
        case z80rr_unknown:
            /* 16 bit register wasn't found - try to evaluate the 8 bit version */
            MemfSeekPtr(csfile->mf, ptr);
            ArithLog8bitZ80(csfile, 0);
            break;

        case z80r_bc:
        case z80r_de:
        case z80r_hl:
            ADD_zxn_bcdehl (csfile, acc16);
            break;

        case z80r_ix:
        case z80r_iy:
            if (GetSym(csfile) != comma) {
                ReportError (csfile, Err_Syntax);
                return;
            }
            AddIxIyReg16(csfile, acc16, 9);
            break;

        default:
            ReportError (csfile, Err_UnknownIdent);
            break;
    }
}


/******************************************************************************
                               Public functions
 ******************************************************************************/


/*!
 * \brief Return pointer to function that parses ZXN (Z80N) mnemonic source line and generates code
 * \param st symbol type
 * \param id pointer to identifier
 * \return pointer to mnemonic parser function, or NULL if not found
 */
mnemprsrfunc
LookupZXNMnemonic(enum symbols st, const char *id)
{
    return LookupIdentifier (id, st, (identfunc_t *) zxnidents, sizeof(zxnidents)/sizeof(idf_t));
}
