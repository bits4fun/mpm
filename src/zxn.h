/****************************************************************************************************
 *
 *   MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
 *    MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
 *    MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
 *    MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
 *    MMMM       MMMM     PPPP              MMMM       MMMM
 *    MMMM       MMMM     PPPP              MMMM       MMMM
 *   MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
 *
 *                         ZZZZZZZZZZZZZZ     888888888888       000000000       NNNNN       NNNNN
 *                       ZZZZZZZZZZZZZZ     8888888888888888    0000000000000     NNNNNN     NNNN
 *                               ZZZZ       8888        8888  0000         0000   NNNNNNNN   NNNN
 *                             ZZZZ           888888888888    0000         0000   NNNN NNNNN NNNN
 *                           ZZZZ           8888        8888  0000         0000   NNNN   NNNNNNNN
 *                         ZZZZZZZZZZZZZZ   8888888888888888    0000000000000     NNNN     NNNNNN
 *                       ZZZZZZZZZZZZZZ       888888888888        000000000      NNNNNN      NNNNN
 *
 * Copyright (C) 1991-2024, Gunther Strube, hello@bits4fun.net
 *
 * This file is part of Mpm.
 * Mpm is free software; you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation - either version 2, or (at your option)
 * any later version.
 * Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Mpm -
 * see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ****************************************************************************************************/


#if ! defined MPM_ZXN_HEADER_
#define MPM_ZXN_HEADER_

#include "datastructs.h"

/* globally available functions */
mnemprsrfunc LookupZXNMnemonic(enum symbols st, const char *id);

#endif
