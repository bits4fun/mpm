/***************************************************************************************************
 *
 *  MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
 *   MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
 *   MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
 *   MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *  MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
 *
 * Copyright (C) 1991-2024, Gunther Strube, hello@bits4fun.net
 *
 * This file is part of Mpm.
 * Mpm is free software; you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation - either version 2, or (at your option)
 * any later version.
 * Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Mpm -
 * see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ***************************************************************************************************/


#include <stdlib.h>
#include <string.h>
#include "config.h"
#include "errors.h"
#include "prsline.h"
#include "memfile.h"


/******************************************************************************
                          Local functions and variables
 ******************************************************************************/

static memfile_t *MemfAlloc (void);
static unsigned char *MemfExpandFile(memfile_t * const mf);

/*!
 * \brief Allocate space for memory file from heap
 * \return Returns pointer to allocated structure
 */
static memfile_t *
MemfAlloc (void)
{
    return (memfile_t *) malloc (sizeof (memfile_t));
}


/*!
 * \brief Expand file space with default block size
 * \param mf
 * \return pointer to new filespace that contains the original file image
 */
static unsigned char *
MemfExpandFile(memfile_t *const mf)
{
    unsigned char *newfilespace;

    if (mf == NULL) {
        return NULL;
    }

    newfilespace = calloc ( (size_t) (mf->filespace + mf->blocksize + 1), sizeof (char));
    if (newfilespace != NULL) {
        mf->filespace += mf->blocksize;
        memcpy (newfilespace, mf->filedata, (size_t) mf->filesize);
        free(mf->filedata);
        mf->filedata = newfilespace;
    }

    return newfilespace;
}


/******************************************************************************
                               Public functions
 ******************************************************************************/



/*!
 * \brief Allocate & iniitalize new memory file with optional filename and size
 * \details
 * Create the memory file with no contents, and optionally pre-allocated space
 * for output mode.
 * \param filename optional pointer to (host) filename (NULL, if not spec.)
 * \param initialsize optional initial file space for buffered resizing during output, or 0
 * \param resizeblock the amount of bytes to increase memory file, when file space is exhausted
 * \return pointer to allocated structure, or NULL if an error occurred
 */
memfile_t *
MemfNew (const char *filename, long initialsize, long resizeblock)
{
    unsigned char *mbuf = NULL;
    memfile_t *mf = MemfAlloc();

    if (mf == NULL)
        return NULL;

    /* default state of memory file */
    mf->filesize = 0;       /* total bytes of real file data */
    mf->filespace = 0;      /* file space is by default not used */
    mf->blocksize = 0;      /* block file resize amount is by default not used */
    mf->filedata = NULL;
    mf->memfileptr = 0;
    mf->eof = false;
    mf->fname = NULL;       /* filename is optional */

    if (initialsize > 0) {
        mbuf = calloc ( (size_t) initialsize + 1, sizeof (char));
        if (mbuf != NULL) {
            mf->filespace = initialsize;    /* this memory will be used for output, define initial space for it */
            mf->blocksize = resizeblock;    /* also define the block (buffer) increment whenever file space needs to be resized */
            mf->filedata = mbuf;
        } else {
            /* initial memory file size failed */
            free(mf);
            return NULL;
        }
    }

    if (filename != NULL) {
        mf->fname = strclone (filename);
        if (mf->fname == NULL) {
            /* filename were specified, however cloning of it failed */
            if (mf->filedata != NULL)
                free(mf->filedata); /* also release initial memory size */
            free(mf);
            mf = NULL;
        }
    }

    return mf;
}


/*!
 * \brief Release memory file ressources
 * \param mf pointer to memory file
 */
void
MemfFree (memfile_t *const mf)
{
    if (mf == NULL) {
        return;
    }

    if ( mf->fname != NULL) {
        free (mf->fname);
    }

    if ( mf->filedata != NULL) {
        free (mf->filedata);
    }

    free(mf);
}


/*!
 * \brief Release memory file contents and indicate no file contents
 * \details the filename is preserved for reference in module functionality
 * \param mf pointer to memory file
 */
void
MemfFreeCache (memfile_t *const mf)
{
    if (mf != NULL && mf->filedata != NULL) {
        free (mf->filedata);
        mf->filedata = NULL;

        mf->filespace = 0;
        mf->memfileptr = 0;
        mf->blocksize = 0;
        mf->filesize = 0;
        mf->eof = true;
    }
}


/*!
 * \brief Link externally cached file data to Memory file, initialize file pointer
 * \details
 * This functionalitity is typically used for macro expansion and include files (read-only)
 * Using this functionality may imply to control free'ing of file data externally - not via MemfFree()
 * See ReleaseSourceMemFile() as an example of this.
 * This functionality is also used as a temporary file, see AsmEvalExpr() and CreateFileFromString()
 * \param mf pointer memory file ressource
 * \param cachedfile pointer to cached file data
 * \param totalbytes size of cached file data in bytes
 * \return
 * 0 if file data were linked successfully,
 * MFEOF (-1) if memory file ressource, cached data is unspecified or
 * memory file already contains file data
*/
int
MemfLinkCachedFile (memfile_t *const mf, unsigned char *const cachedfile, long totalbytes)
{
    if (mf == NULL) {
        /* nothing to do here... */
        return MFEOF;
    }

    if (cachedfile == NULL) {
        /* nothing to do here if cached file pointer is not specified */
        return MFEOF;
    }

    if (mf->filedata != NULL) {
        /* report error if data already exist in memory file */
        return MFEOF;
    }

    mf->filedata = cachedfile;
    mf->filesize = totalbytes;
    mf->filespace = totalbytes;     /* file space is same as file size */
    mf->blocksize = 0;              /* cached data is external ressource, so read-only */
    mf->memfileptr = 0;             /* file pointer index is set at beginning of data */
    mf->eof = false;

    return 0;
}


/*!
 * \brief Cache opened host file contents into specified memory file of specified bytes (from cur. host file ptr)
 * \param mf pointer to memory file structure
 * \param fd the host file descriptor
 * \param filesize amount of content to cache
 * \return NULL if file was not found or contents could not be loaded (I/O error)
 */
unsigned char *
MemfCacheHostFileStream (memfile_t *mf, FILE *fd, size_t totalbytes)
{
    unsigned char *fdbuffer = NULL;

    if (mf == NULL) {
        /* Nothing to do here... */
        return NULL;
    }

    if (fd == NULL) {
        /* report error if host file isn't open*/
        ReportIOError (mf->fname);
        return NULL;
    }

    if (mf->filedata != NULL) {
        /* report error if data already exist in memory file */
        ReportIOError (mf->fname);
        return NULL;
    }

    if (totalbytes == 0) {
        mf->filesize = 0;
        mf->filespace = 0;
        mf->memfileptr = 0;
        mf->eof = false;
    } else {
        fdbuffer = calloc (totalbytes + 1, sizeof (char));
        if (fdbuffer == NULL) {
            ReportError (NULL, Err_Memory);
            return NULL;
        }

        if (fread (fdbuffer, sizeof (char), totalbytes, fd) != totalbytes) {    /* read file data into buffer */
            ReportIOError(mf->fname);
            free (fdbuffer);
            fdbuffer = NULL;
        } else {
            mf->filesize = (long) totalbytes;
            mf->filespace = (long) totalbytes;
            mf->filedata = fdbuffer;
            mf->memfileptr = 0;
            mf->eof = false;
        }
    }

    return fdbuffer;
}


/*!
 * \brief Load complete contents of specified host file <fd> into memory
 * \param mf
 * \param fd
 * \return pointer to start of file data, if file contents was successfully loaded into buffer
 */
unsigned char *
MemfCacheHostFile (memfile_t *mf, FILE *fd)
{
    size_t filesize;

    if (mf == NULL) {
        /* Nothing to do here... */
        return NULL;
    }

    if (fd == NULL) {
        /* report error if file isn't opened */
        ReportIOError (mf->fname);
        return NULL;
    }

    fseek(fd, 0L, SEEK_END); /* file pointer to end of file */
    filesize = (size_t) ftell(fd);
    fseek(fd, 0L, SEEK_SET); /* file pointer back to start of file */

    return MemfCacheHostFileStream (mf, fd, filesize);
}



/*!
 * \brief Get filename of of memory file
 * \param mf
 * \return
 */
char *
MemfGetFilename(const memfile_t *mf)
{
    if (mf == NULL) {
        /* file not specified! */
        return NULL;
    } else {
        return mf->fname;
    }
}


/*!
 * \brief Reads a character from current memory file pointer
 * \details Internal file pointer is auto-incremented and end of file is monitored
 * \param mf pointer to memory file structure
 * \return character as an int, or MFEOF (-1) if end of file was reached
 */
int
MemfGetc(memfile_t *const mf)
{
    int memchar;

    if (mf == NULL) {
        /* file not specified! */
        return MFEOF;
    }

    if (mf->filedata == NULL || mf->eof == true) {
        return MFEOF;
    } else {
        if ( (mf->memfileptr >= mf->filesize) || (mf->memfileptr < 0) ) {
            /* memory pointer protection: ensure bounds-check before accessing memory... */
            mf->eof = true;
            return MFEOF;
        } else {
            memchar = mf->filedata[mf->memfileptr++];

            if ( mf->memfileptr >= mf->filesize) {
                /* signal EOF - last character was just returned */
                mf->eof = true;
            }
        }
    }

    return memchar;
}


/*!
 * \brief "Push back" character in memory file
 * \details
        Simply step-back the memory file pointer one character.

        If character is successfully pushed-back, the end-of-file
        indicator for the memory file stream is cleared.
        The file-position indicator of <file is also decremented
 * \param srcf
 */
void
MemfUngetc(memfile_t *const mf)
{
    if (mf != NULL && mf->filedata != NULL && mf->memfileptr >= 0) {
        /* decrease character file pointer in memory file */
        mf->memfileptr--;
        mf->eof = false;
    }
}


/*!
 * \brief Put a character into memory file at current file pointer
 * \details Memory file is auto-increased and internal file pointer is auto-incremented
 * \param mf pointer to memory file structure
 * \return
 * 0 if character were successfully added,
 * MFEOF (-1) if memory file is unspecified, empty or cannot be expanded
 */
int
MemfPutc(memfile_t *const mf, int c)
{
    if (mf == NULL) {
        /* memory file not specified */
        return MFEOF;
    }
    if (mf->filedata == NULL) {
        /* empty memory file */
        return MFEOF;
    }

    if ( (mf->memfileptr+1 >= mf->filespace) && MemfExpandFile(mf) == NULL) {
        ReportError (NULL, Err_Memory); /* report exhausted memory failure */
        return MFEOF;
    }

    mf->filedata[mf->memfileptr++] = (unsigned char) c;

    if (mf->memfileptr < mf->filesize) {
        mf->eof = false;
    } else {
        mf->filesize++;
        mf->eof = true;
    }

    /* character successfully added to memory file */
    return 0;
}


/*!
 * \brief Output a string to memory file at current file pointer
 * \details Memory file is auto-increased and internal file pointer is auto-incremented
 * \param mf pointer to memory file structure
 * \return
 * 0 if string were successfully added,
 * MFEOF (-1) if string is NULL, or memory file is unspecified, empty or cannot be expanded
 */
int
MemfPuts(memfile_t *const mf, const char *str)
{
    if (str == NULL)
        return MFEOF;

    do {
        if (MemfPutc(mf, *str) == MFEOF) {
            return MFEOF;
        }

        str++;
    } while( *str != 0 );

    /* string successfully added to memory file */
    return 0;
}



/*!
 * \brief Read contents into buffer from memory file at current file pointer
 * \details Memory file is auto-increased and internal file pointer is auto-incremented
 * \param mf pointer to memory file structure
 * \param buf pointer to buffer to read into
 * \param len total bytes to read into buffer
 * \return
 * len if buffer were successfully read,
 * MFEOF (-1) if buffer is NULL, memory file is unspecified, or EOF happened before len bytes read
 */
int
MemfRead(memfile_t *const mf, char *buf, size_t len)
{
    int c;
    size_t l = len;

    if (buf == NULL)
        return MFEOF;

    while(l) {
        if ( (c = MemfGetc(mf)) == MFEOF) {
            return MFEOF;
        }

        *buf++ = (char) c;
        l--;
    }

    /* buffer successfully read from memory file */
    return (int) len;
}


/*!
 * \brief Output buffer contents to memory file at current file pointer
 * \details Memory file is auto-increased and internal file pointer is auto-incremented
 * \param mf pointer to memory file structure
 * \return
 * 0 if buffer were successfully written,
 * MFEOF (-1) if buffer is NULL, or memory file is unspecified, empty or cannot be expanded
 */
int
MemfWrite(memfile_t *const mf, const char *buf, size_t len)
{
    if (buf == NULL) {
        /* fatal - no read buffer specified */
        return MFEOF;
    }

    while(len) {
        if (MemfPutc(mf, *buf) == MFEOF) {
            return MFEOF;
        }

        buf++;
        len--;
    }

    /* buffer successfully written to memory file */
    return 0;
}


/*!
 * \brief The current file pointer (index) of memory file
 * \param mf
 * \return MFEOF if memory file ressource is not specified or empty
 */
long
MemfTell(const memfile_t *mf)
{
    if (mf != NULL && mf->filedata != NULL) {
        return mf->memfileptr;
    } else {
        /* memory file ressource not specified! */
        return MFEOF;
    }
}


/*!
 * \brief Get the current char pointer of memory file for text parsers
 * \param mf
 * \return a NULL if memory file ressource is not specified or empty
 */
unsigned char *
MemfTellPtr(const memfile_t *mf)
{
    if (mf != NULL && mf->filedata != NULL) {
        return mf->filedata + mf->memfileptr;
    } else {
        /* memory file ressource not specified! */
        return NULL;
    }
}

/*!
 * \brief Get the current char - 1 pointer of memory file for text parsers
 * \param mf
 * \return a NULL if memory file ressource is not specified or empty, or file pointer is already at beginning of file
 */
unsigned char *
MemfTellPrevCharPtr(const memfile_t *mf)
{
    if (mf != NULL && mf->filedata != NULL && mf->memfileptr > 0) {
        return mf->filedata + mf->memfileptr - 1;
    } else {
        /* memory file ressource not specified! */
        return NULL;
    }
}


/*!
 * \brief Set file pointer (index) of memory file
 * \param mf the memory file structure
 * \param fptr
 * \return 0 if pointer was set successfully, otherwise -1 (MFEOF)
 */
int
MemfSeek(memfile_t *mf, long fptr)
{
    if ( (mf != NULL) && (mf->filedata != NULL) && (fptr >= 0) && (fptr <= mf->filesize) ) {
        mf->memfileptr = fptr;

        if (fptr == mf->filesize)
            mf->eof = true;
        else
            mf->eof = false;

        return 0;
    } else {
        /* file ressource not specified or empty, or file pointer is out of range! */
        return MFEOF;
    }
}


/*!
 * \brief Set character pointer of memory file
 * \param srcf
 * \param ptr
 * \return 0 if pointer was set successfully, otherwise -1 (MFEOF)
 */
int
MemfSeekPtr(memfile_t *mf, const unsigned char *ptr)
{
    if (
        (mf != NULL) && (mf->filedata != NULL) &&
        (ptr >= mf->filedata) && (ptr <= (mf->filedata + mf->filesize))
       ) {
        mf->memfileptr = ptr - mf->filedata;
        mf->eof = false;

        return 0;
    }

    /* file ressource not specified or empty, or file pointer is out of range! */
    return MFEOF;
}


/*!
 * \brief Set file pointer to end of memory file
 * \param mf the memory file structure
 * \return
 * 0 if pointer was set successfully,
 * otherwise -1 (MFEOF) if memory file is unspecified or empty
 */
int
MemfSeekEnd(memfile_t *mf)
{
    if ( (mf != NULL) && (mf->filedata != NULL) ) {
        mf->memfileptr = mf->filesize;
        mf->eof = true;

        return 0;
    } else {
        /* file ressource not specified or empty! */
        return MFEOF;
    }
}


/*!
 * \brief MemfEof
 * \param srcf
 * \return End of File (MFEOF) status (otherwise 0) of memory file
 */
int
MemfEof(const memfile_t *mf)
{
    if (mf == NULL) {
        /* ressource not specified! */
        return MFEOF;
    }

    if (mf->filedata == NULL || mf->eof == true) {
        return MFEOF;
    } else {
        return 0;
    }
}
