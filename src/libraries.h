/***************************************************************************************************
 *
 *  MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
 *   MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
 *   MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
 *   MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *  MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
 *
 * Copyright (C) 1991-2024, Gunther Strube, hello@bits4fun.net
 *
 * This file is part of Mpm.
 * Mpm is free software; you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation - either version 2, or (at your option)
 * any later version.
 * Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Mpm -
 * see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ***************************************************************************************************/

#if ! defined MPM_LIBS_HEADER_
#define MPM_LIBS_HEADER_

#include <stdio.h>
#include "datastructs.h"    /* base symbol data structures and routines that manages a symbol table */

/* global variables */
extern libraries_t *libraries;

/* globally declared functions */
int LinkObjFileLibModules (memfile_t *mobjf, const long fptr_base, long startnames, const long endnames);
int SearchLibfile (libfile_t *curlib, char *modname);
int SearchLibraries (const char *modname);
void PopulateLibrary (char *libraryfilename, const module_t *curmodule);
char *CreateLibfileName(const char *filename);
void CreateLibfile (void);
void RegisterLibfile (const char *filename);
void IndexLibraries(const libraries_t *libs);
void ReleaseLibraries (void);

#endif
