/***************************************************************************************************
 *
 *  MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
 *   MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
 *   MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
 *   MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *  MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
 *
 * Copyright (C) 1991-2024, Gunther Strube, hello@bits4fun.net
 *
 * This file is part of Mpm.
 * Mpm is free software; you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation - either version 2, or (at your option)
 * any later version.
 * Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Mpm -
 * see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ***************************************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include "config.h"
#include "datastructs.h"
#include "symtables.h"
#include "avltree.h"
#include "errors.h"
#include "prsline.h"
#include "pass.h"
#include "memfile.h"
#include "sourcefile.h"


/******************************************************************************
                          Global variables
 ******************************************************************************/
avltree_t *cachedincludefiles;


/******************************************************************************
                          Local functions and variables
 ******************************************************************************/
static void Fetchfilename (FILE *fptr, char *filename);
static void FetchDependencies (FILE *projectfile, filelist_t **dependencies);
static void ReleaseFileList(filelist_t *list);
static void ReleaseOwnedFile (usedsrcfile_t *ownedfile);
static sourcefile_t *AllocFile (void);
static sourcefile_t *Setfile (module_t *curmodule, sourcefile_t *curfile, sourcefile_t *newfile, const char *fname);
static includefile_t *FindCachedIncludeFile (const char *includefilename);
static includefile_t *Add2IncludeFileCache(sourcefile_t *csfile, const char *filename, FILE *inclfile);
static usedsrcfile_t *AllocUsedFile (void);
static pathlist_t *AllocPathNode(void);
static filelist_t *AllocFileListNode (void);
static int cmpFndInclfile (const char *inclfilename, const includefile_t * p);
static int cmpInsInclfile (const includefile_t *k, const includefile_t *p);
static int Flncmp(const char *f1, const char *f2);


/* ----------------------------------------------------------------------------------------
    static includefile_t *FindCachedIncludeFile (char *filename)

        Find file in the Include File Cache (a searchable AVL tree).

    returns
        includefile_t reference to found include file in cache
        NULL, if include file was not found in Cache
   ---------------------------------------------------------------------------------------- */
static includefile_t *
FindCachedIncludeFile (const char *filename)
{
    if (cachedincludefiles == NULL) {
        return NULL;
    } else {
        return Find (cachedincludefiles, filename, (compfunc_t) cmpFndInclfile);
    }
}


static int
cmpFndInclfile (const char *inclfilename, const includefile_t *p)
{
    return strcmp (inclfilename, p->fname);
}


static int
cmpInsInclfile (const includefile_t * k, const includefile_t * p)
{
    return strcmp (k->fname, p->fname);
}


/**
 * @brief Add2IncludeFileCache Add file ressource to the Include File Cache (a searchable AVL tree).
 * @param csfile
 * @param filename
 * @param inclfile
 * @return
 *         includefile_t reference to cached include file
           NULL, if include file reported file I/O problems or insufficient memory for caching
 */
static includefile_t *
Add2IncludeFileCache(sourcefile_t *csfile, const char *filename, FILE *inclfile)
{
    static const char *errmsg = "Include file caching failed - insufficient memory";
    includefile_t *newinclfile;
    unsigned char *fdbuffer = NULL;

    newinclfile = AllocIncludeFile();
    if (newinclfile == NULL) {
        ReportRuntimeErrMsg  (MemfGetFilename(csfile->mf), errmsg);
        return NULL;
    }

    newinclfile->fname = strclone(filename);  /* Allocate area for a include filename */
    if (newinclfile->fname == NULL) {
        free(newinclfile);
        ReportRuntimeErrMsg  (MemfGetFilename(csfile->mf), errmsg);
        return NULL;
    }

    fseek(inclfile, 0L, SEEK_END); /* file pointer to end of file */
    newinclfile->filesize = ftell(inclfile);
    fseek(inclfile, 0L, SEEK_SET); /* file pointer back to start of file */

    fdbuffer = (unsigned char *) calloc ( (size_t) newinclfile->filesize + 1, sizeof (char));
    if (fdbuffer == NULL) {
        free(newinclfile->fname);
        free(newinclfile);
        ReportRuntimeErrMsg  (MemfGetFilename(csfile->mf), errmsg);
        return NULL;
    } else {
        /* read file data into buffer */
        if (fread (fdbuffer, sizeof (char), (size_t) newinclfile->filesize, inclfile) != (size_t) newinclfile->filesize) {
            free (fdbuffer);
            free(newinclfile->fname);
            free(newinclfile);
            ReportError (csfile, Err_FileIO);
            return NULL;
        } else {
            newinclfile->filedata = fdbuffer;
        }
    }

    /* Insert new include file object into Include File Cache AVL tree */
    if (Insert (&cachedincludefiles, newinclfile, (compfunc_t) cmpInsInclfile)) {
        /* return reference to new, cached include file */
        return newinclfile;
    } else {
        free (fdbuffer);
        free(newinclfile->fname);
        free(newinclfile);
        ReportRuntimeErrMsg  (MemfGetFilename(csfile->mf), errmsg);
        return NULL;
    }
}


/* ----------------------------------------------------------------
   Fetch a filename from current file pointer <fptr>, onwards.
   The filename is stored / null-terminated in the buffer <filename>,
   truncated at MAX_FILENAME_SIZE chars.

   on return, the file pointer <fptr> is at the terminating character
   of the filename, ie. a space, EOL or a double quote.
   ---------------------------------------------------------------- */
static void
Fetchfilename (FILE *fptr, char *filename)
{
    bool eofilename = false;
    int l = 0;
    int c;

    do {
        c = fGetChar (fptr);
        if ((c == '\n') || (c == EOF)) {
            break;
        }

        /* read filename until a space, a comment or a double quote */
        if (!isspace(c) && c != ':' && c != '"' && c != ';') {
            /* swallow '#' (old syntax no longer used), but use all other chars in filename */
            if (c != '#' )
                filename[l++] = (char) c;
        } else {
            eofilename = true;
        }
    } while ( l<MAX_FILENAME_SIZE && eofilename == false);

    filename[l] = '\0';           /* null-terminate file name string */
    AdjustPlatformFilename(filename);

    ungetc (c, fptr);             /* evaluate last char by caller... */
}


/* ----------------------------------------------------------------------------------------
   static void FetchDependencies (FILE *projectfile, filelist_t **dependencies)

   Fetch a module file dependencies, if they are specified on the same line
   defined with a trailing ':' after the module file name:
        <module filename>: <dependency filename1> <dependency filenameX>

   return pointer to list of dependencies <dependencies> variable, or NULL if no list were
   found
   ---------------------------------------------------------------------------------------- */
static void
FetchDependencies (FILE *projectfile, filelist_t **dependencies)
{
    bool dependencySpecified = false;
    bool cstylecomment = false;
    char tempflnm[MAX_FILENAME_SIZE+1];
    int c;

    for (;;) {
        if (feof (projectfile)) {
            break; /* no dependency filenames in current line... */
        } else {
            c = fGetChar (projectfile);
            switch(c) {
            case '\n':
            case '\x1A': /* end of line, no dependencies... */
                return;

            case ';':    /* a comment, skip line and return (no dependencies) */
                fSkipLine(projectfile, &cstylecomment);
                return;

            case ':':
                /* now, we're ready to parse dependency filenames, until EOL */
                dependencySpecified = true;
                break;

            default:
                if (!isspace (c) && dependencySpecified == true) {
                    /* found a real char as first of a dependency filename... */
                    ungetc (c, projectfile); /* let Fetchfilename() get the first char.. */

                    Fetchfilename(projectfile, tempflnm);
                    AddFileNameNode (tempflnm, dependencies);

                    /* there might be another dependency file, loop again until EOL */
                }
            }
        }
    }
}


static sourcefile_t *
Setfile (module_t *curmodule,      /* pointer to record of current module that "owns" the source code file */
         sourcefile_t *curfile,    /* pointer to record of current source file */
         sourcefile_t *nfile,      /* pointer to record of new source file */
         const char *filename)     /* pointer to filename string */
{
    nfile->module = curmodule;
    nfile->prevsrcfile = curfile;
    nfile->newsrcfile = NULL;
    nfile->usedsrcfile = NULL;
    nfile->dependencies = NULL;
    nfile->ident = NULL;
    nfile->labelname = NULL;
    nfile->lineptr = NULL;
    nfile->sym = nil;
    nfile->identlen = 0;
    nfile->lineno = 0;              /* Reset to 0 as line counter during parsing */
    nfile->parsingmacro = false;
    nfile->asmerror = Err_None;
    nfile->eol = false;
    nfile->cstylecomment = false;
    nfile->includedfile = false;
    nfile->macro = NULL;
    nfile->mf = NULL;

    if ((nfile->mf = MemfNew(filename, 0, 0)) == NULL) {
        ReportError (NULL, Err_Memory);
        return nfile;
    }

    if ( (nfile->ident = AllocIdentifier(MAX_NAME_SIZE+1)) == NULL ) {
        ReportError (NULL, Err_Memory);
        return nfile;
    }

    return nfile;
}


static void
ReleaseOwnedFile (usedsrcfile_t *ownedfile)
{
    /* Release first other files called by this file */
    if (ownedfile->nextusedfile != NULL) {
        ReleaseOwnedFile (ownedfile->nextusedfile);
    }

    /* Release first file owned by this file */
    if (ownedfile->ownedsourcefile != NULL) {
        ReleaseFile (ownedfile->ownedsourcefile);
    }

    free (ownedfile);             /* Then release this owned file */
}



static int
Flncmp(const char *f1, const char *f2)
{
    int i;

    if (strlen(f1) != strlen(f2)) {
        return -1;
    } else {
        i = (int) strlen(f1);
        while(--i >= 0)
            if( tolower(f1[i]) != tolower(f2[i]) ) {
                return -1;
            }

        /* filenames equal */
        return 0;
    }
}


static void
ReleaseFileList(filelist_t *list)
{
    filelist_t *node;

    while(list != NULL) {
        node = list;
        free(node->filename);   /* release the actual filename string */

        node = node->nextfile;  /* point at next file in list */
        free(list);            /* release this file list node */
        list = node;           /* get ready for releasing the next node */
    }
}


static pathlist_t *
AllocPathNode (void)
{
    return (pathlist_t *) malloc (sizeof (pathlist_t));
}

static filelist_t *
AllocFileListNode (void)
{
    return (filelist_t *) malloc (sizeof (filelist_t));
}


static usedsrcfile_t *
AllocUsedFile (void)
{
    return (usedsrcfile_t *) malloc (sizeof (usedsrcfile_t));
}


static sourcefile_t *
AllocFile (void)
{
    return (sourcefile_t *) malloc (sizeof (sourcefile_t));
}


includefile_t *
AllocIncludeFile (void)
{
    includefile_t *incf = (includefile_t *) malloc (sizeof (includefile_t));

    if (incf != NULL) {
        incf->fname = NULL;
        incf->filesize = 0;
        incf->filedata = NULL;
    }

    return incf;
}



/******************************************************************************
                               Public functions
 ******************************************************************************/

sourcefile_t *
Prevfile (sourcefile_t *csfile)
{
    usedsrcfile_t *newusedfile;
    sourcefile_t *ownedfile;

    if ((newusedfile = AllocUsedFile ()) == NULL) {
        ReportError (NULL, Err_Memory);
        return csfile;                      /* return parameter pointer - nothing happended! */
    }

    ownedfile = csfile;
    csfile = csfile->prevsrcfile;        /* get back to owner file - now the current */
    csfile->newsrcfile = NULL;           /* current file is now the last in the list */
    ownedfile->prevsrcfile = NULL;       /* pointer to owner now obsolete... */

    /* set ptr to next record to current ptr to another used file */
    newusedfile->nextusedfile = csfile->usedsrcfile;

    csfile->usedsrcfile = newusedfile;          /* new used file now inserted into list */
    newusedfile->ownedsourcefile = ownedfile;   /* the inserted record now points to previously owned file */
    return csfile;
}


sourcefile_t *
Newfile (module_t *curmodule, sourcefile_t *curfile, const char *fname)
{
    sourcefile_t *nfile;

    if (fname == NULL) {
        /* Don't do anything ... */
        return NULL;
    }

    if (curfile == NULL) {
        /* file record has not yet been created */
        if ((curfile = AllocFile ()) == NULL) {
            ReportError (NULL, Err_Memory);
            return NULL;
        } else {
            return Setfile (curmodule, NULL, curfile, fname);
        }
    } else if ((nfile = AllocFile ()) == NULL) {
        ReportError (NULL, Err_Memory);
        return curfile;
    } else {
        return Setfile (curmodule, curfile, nfile, fname);
    }
}


sourcefile_t *
FindFile (sourcefile_t *srcfile, char *flnm)
{
    sourcefile_t *foundfile;

    if (srcfile != NULL) {
        if ((foundfile = FindFile(srcfile->prevsrcfile, flnm)) != NULL) {
            return foundfile;    /* trying to include an already included file recursively! */
        }

        if (Flncmp(MemfGetFilename(srcfile->mf),flnm) == 0) {
            return srcfile;    /* this include file already used! */
        } else {
            return NULL;    /* this include file didn't match filename searched */
        }
    } else {
        return NULL;
    }
}


/* ------------------------------------------------------------------------------------------
    includefile_t *CacheIncludeFile (sourcefile_t *csfile, char *fname)

        Include File through shared file cache. If file is not yet available in Include File
        Cache, add it - then return a includefile_t reference to it. If the file has already
        been cached, just return the includefile_t reference to it.

    Returns:
        NULL if file was not found on disc or contents could not be loaded (I/O error)
        Pointer to include file reference, if file was successfully included
   ------------------------------------------------------------------------------------------ */
includefile_t *
CacheIncludeFile (sourcefile_t *csfile, char *fname)
{
    FILE *includefile;
    includefile_t *cachedincludefile = NULL;

    if (FindFile(csfile, fname) != NULL) {
        /* Ups - this file has already been INCLUDE'ed (catch recursive include) */
        ReportError (csfile, Err_IncludeFile);
        return NULL;
    }

    if ( (cachedincludefile = FindCachedIncludeFile(fname)) != NULL) {
        /* found a cached include file with same name... return that! */
        return cachedincludefile;
    } else {
        /* this include file has not yet been cached... */
        includefile = OpenFile(fname, gIncludePath, false);
        if (includefile != NULL) {
            cachedincludefile = Add2IncludeFileCache(csfile, fname, includefile);
            fclose(includefile);
        } else {
            /* this include file wasn't found on the filing system */
            ReportError (csfile, Err_FileIO);
        }
    }

    return cachedincludefile;
}


/*!
 * \brief Create a parsable memory file, based on supplied string
 * \details
 * This is a utility function designed to let the source code parser
 * and expression parser run through a sequence as it is contained
 * in the string.
 * \param name may optionally specify name of memory file, or NULL
 * \param lineno may optionally specify a beginning line number, or set = 0
 * \param string null-terminated string to be allocated as memory file data
 * \return
 * pointer to stand-alone Memory file, or
 * NULL if insufficient heap memory was reached, or string was NULL.
 */
sourcefile_t *
CreateFileFromString (const char *name, int lineno, const char *string)
{
    sourcefile_t *nfile = NULL;
    unsigned char *buffer = NULL;

    if (string == NULL) {
        return NULL;
    }

    if ((nfile = AllocFile ()) == NULL) {
        ReportError (NULL, Err_Memory);
        return NULL;
    } else {
        buffer = (unsigned char *) strclone(string);
        if (buffer == NULL) {
            ReportError (NULL, Err_Memory);
            free(nfile);
            return NULL;
        } else {
            Setfile (NULL, NULL, nfile, name);
            if (nfile->mf != NULL) {
                MemfLinkCachedFile (nfile->mf, buffer, strlen((char *) buffer));
            }
            nfile->lineno = lineno;
        }
    }

    return nfile;
}


/* ------------------------------------------------------------------------------------------
    void ReleaseFile (sourcefile_t *srcfile)

    Release all previously allocated ressources of source file.
   ------------------------------------------------------------------------------------------ */
void
ReleaseFile (sourcefile_t *srcfile)
{
    if (srcfile != NULL) {
        if (srcfile->usedsrcfile != NULL) {
            ReleaseOwnedFile (srcfile->usedsrcfile);
        }

        if (srcfile->dependencies != NULL) {
            ReleaseFileList(srcfile->dependencies);
        }

        ReleaseSourceMemFile(srcfile);  /* Release memory file */

        if (srcfile->ident != NULL) {
            free (srcfile->ident);      /* Release allocated string identifier buffer */
        }

        free (srcfile);                 /* Release file information record for this file */
    }
}


/*!
 * \brief Release previously cached memory file and reset pointer to indicate file data is released
 * \details Only non-include file is released via this functionality
 * \param srcfile
 */
void
ReleaseSourceMemFile (sourcefile_t *srcfile)
{
    if (srcfile != NULL && srcfile->mf != NULL) {
        if (srcfile->includedfile == false) {
            MemfFree(srcfile->mf);
        } else {
            /* the cached memory file contents refers to includefile_t that will be release on it's own */
            /* only release the memory file definition */
            free(srcfile->mf);
        }
        srcfile->mf = NULL;
    }
}

/*!
 * \brief Release previously cached memory file contents, but preserve memory file name and properties
 * \details Only non-include file is released via this functionality
 * \param srcfile
 */
void
ReleaseSourceMemFileCache(sourcefile_t *srcfile)
{
    if (srcfile != NULL && srcfile->mf != NULL) {
        MemfFreeCache(srcfile->mf);
    }
}


/* ------------------------------------------------------------------------------------------
    void ReleaseCachedIncludeFiles(void)

        Free cached Include file ressource (previously allocated) on each node
        of the Include File Cache AVL tree.
   ------------------------------------------------------------------------------------------ */
void
ReleaseCachedIncludeFiles(void)
{
    DeleteAll (&cachedincludefiles, (void (*)(void *)) FreeCachedIncludeFile);
}


/*!
 * \brief Return the operating system file status information at <pfstat> memory area
 * \details
 * The filename will be combined with each directory node in <pathlist> and a file
 * stat function will be executed.

 * \param filename
 * \param pathlist
 * \param fstat
 * \return
 * -1 if the file was not found in one of the specified <pathlist>.
 * 0 or 1 and contents filled at <pfstat> location.
 */
int
StatFile(const char *filename, const pathlist_t *pathlist, struct stat *const fstat)
{
    char tempflnm[MAX_FILENAME_SIZE+1];
    FILE *filehandle;

    strncpy(tempflnm, filename, MAX_FILENAME_SIZE);
    filehandle = OpenFile(tempflnm, pathlist, true);
    if (filehandle == NULL) {
        /* file was not found */
        return -1;
    } else {
        fclose(filehandle);
        return stat (tempflnm, fstat);
    }
}


/*!
 * \brief Open file via platform-independent name and specified search path list
 * \details
 * The filename will be combined with each directory node in <pathlist> and a file
 * open IO function will be executed.
 * The absolute filename of the found file will be written to <filename> string buffer,
 * if <expandfilename> argument == true.

 * \param filename pointer to filename buffer of MAX_FILENAME_SIZE+1
 * \param pathlist
 * \param expandfilename
 * \return
 * A file handle, if the file was successfully opened in one of the specified
 * directories of <pathlist>, otherwise NULL, if the file wasn't found in
 * the <pathlist>.
 */
FILE *
OpenFile(char *filename, const pathlist_t *pathlist, bool expandfilename)
{
    char tempflnm[MAX_FILENAME_SIZE+1];
    char tempdirsep[] = {DIRSEP, 0};
    FILE *filehandle;

    filehandle = fopen (AdjustPlatformFilename(filename), "rb");
    if (filehandle != NULL) {
        return filehandle;
    }

    while (pathlist != NULL) {
        if ( (strlen(pathlist->directory) + strlen(filename) + 1) > MAX_FILENAME_SIZE) {
            /* avoid buffer overrun of path + filename */
            return NULL;
        }

        strncpy(tempflnm, pathlist->directory, MAX_FILENAME_SIZE);
        strncat(tempflnm, tempdirsep, MAX_FILENAME_SIZE);
        strncat(tempflnm, filename, MAX_FILENAME_SIZE);

        filehandle = fopen (AdjustPlatformFilename(tempflnm), "rb");
        if (filehandle != NULL) {
            if (expandfilename == true) {
                memcpy(filename, tempflnm, strlen(tempflnm)+1);
            }
            return filehandle;                  /* file was found! */
        } else {
            pathlist = pathlist->nextdir;    /* file not in this directory, try next ... */
        }
    }

    return NULL;
}


/* ------------------------------------------------------------------------------------------
   char *AddFileExtension(const char *filename, char* extension)

   Allocate a new filename and add/replace with extension.
   Return NULL, if filename couldn't be allocated.
   ------------------------------------------------------------------------------------------ */
char *
AddFileExtension(const char *oldfilename, const char *extension)
{
    char *newfilename;
    int b;
    int pathsepCount = 0;
    size_t newflnSize;

    if (oldfilename == NULL || extension == NULL)
        return NULL;

    newflnSize = strlen (oldfilename) + strlen(extension) + 1;

    if ((newfilename = AllocIdentifier (newflnSize)) != NULL) {
        memcpy (newfilename, oldfilename, strlen(oldfilename)+1);

        /* scan filename backwards and find extension, but before a pathname separator */
        for (b=(int) strlen(newfilename)-1; b>=0; b--) {
            if (newfilename[b] == '\\' || newfilename[b] == '/') {
                pathsepCount++;    /* Ups, we've scanned past the short filename */
            }

            if (newfilename[b] == '.' && pathsepCount == 0) {
                break; /* we found an extension before a path separator! */
            }
        }

        if (b > 0) {
            memcpy ((newfilename+b), extension, strlen(extension)+1);    /* replace old extension with new */
        } else {
            strncat(newfilename, extension, newflnSize);    /* missing extension, concatanate new */
        }
    } else {
        ReportError (NULL, Err_Memory);   /* No more room */
    }

    return newfilename;
}


/* ------------------------------------------------------------------------------------------
    char *
    Truncate2BaseFilename(char *filename)

    Strip extension (null-terminate at extension "." and return pointer to start of base
    filename without path.

    For example
        /path1/path2/filename.ext
    will return a pointer to
        filename\0
   ------------------------------------------------------------------------------------------ */
char *
Truncate2BaseFilename(char *filename)
{
    /* scan fetched filename backwards and truncate extension, if found, but before a pathname separator */
    for (int b = (int)strlen(filename)-1; b>=0; b--) {
        if (filename[b] == '\\' || filename[b] == '/') {
            return (filename+b+1);
        }

        if (filename[b] == '.') {
            filename[b] = '\0'; /* truncate file extension */
        }
    }

    return filename; /* no path separator found, return start of current filename */
}


/* ------------------------------------------------------------------------------------------
   char *AdjustPlatformFilename(char *filename)

   Adjust filename to use the platform specific directory specifier, which is defined as
   DIRSEP in config.h. Adjusting the filename at runtime enables the freedom to not worry
   about paths in filenames when porting Z80 projects to Windows or Unix platforms.

   Example: if a filename contains a '/' (Unix directory separator) it will be converted
   to a '\' if mpm currently is compiling on Windows (or Dos).

   Returns:
   same pointer as argument (beginning of filename)
   ------------------------------------------------------------------------------------------ */
char *
AdjustPlatformFilename(char *const filename)
{
    char *flnmptr = filename;

    if (filename == NULL) {
        return NULL;
    }

    while(*flnmptr != '\0') {
        if (*flnmptr == '/' || *flnmptr == '\\') {
            *flnmptr = DIRSEP;
        }

        flnmptr++;
    }

    return filename;
}


/**
 * Updates path list, which is referenced by <plist> pointer.
 *
 * @brief AddPathNode  Scans string <path> for sub paths (separated by ';' or ':' depending on OS) and adds them to the path list.
 * @param path
 * @param plist
 */
void
AddPathNode (char *const path, pathlist_t **plist)
{
    char pathcpy[MAX_STRBUF_SIZE];
    pathlist_t *newnode;
    const char *pathtoken;
    char *newtoken;
    char *newpath;

    if (path == NULL) {
        return;    /* nothing to do - no path has been specified */
    }
    if (strlen(path) == 0) {
        return;    /* nothing to do - path is empty! */
    }

    snprintf(pathcpy, MAX_STRBUF_SIZE,"%s", path);
    pathtoken = pathcpy;

    while(pathtoken != NULL) {
        newtoken = memchr(pathtoken, ENVPATHSEP, strlen(pathtoken));
        if (newtoken != NULL) {
            *newtoken++ = 0;
        }

        if ((newnode = AllocPathNode()) == NULL) {
            ReportError (NULL, Err_Memory);
            return;
        }

        if ((newpath = strclone(pathtoken)) == NULL) {
            ReportError (NULL, Err_Memory);
            free(newnode);
            return;    /* couldn't allocate memory for sub path */
        }

        /* remove directory separator */
        if (newpath[strlen(newpath) - 1] == DIRSEP) {
            newpath[strlen(newpath) - 1] = '\0';
        }

        newnode->directory = newpath;
        newnode->nextdir = *plist;          /* link new node before current node */
        *plist = newnode;                   /* update start of path list to new node */

        pathtoken = newtoken;      /* get next sub path, if available */
    }
}


/**
   Add filename (insert as first) to list of filenames <flist>.
   With initial list, the caller must initialize *flist = NULL.

   Returns:
    Updates filename list, which is referenced by <flist> pointer (start of list).

 * @brief AddFileNameNode
 * @param filename
 * @param flist
 */
void
AddFileNameNode (const char *filename, filelist_t **flist)
{
    filelist_t *newnode;
    char *filenameCpy;

    if (filename == NULL) {
        return;    /* nothing to do - no filename has been specified */
    }
    if (strlen(filename) == 0) {
        return;    /* nothing to do - filename is empty! */
    }

    filenameCpy = strclone(filename);
    if (filenameCpy == NULL) {
        return;    /* nothing to do - no memory to add paths to list */
    }

    if ((newnode = AllocFileListNode()) != NULL) {
        newnode->filename = filenameCpy;
        newnode->nextfile = *flist;         /* link new node before current node */
        *flist = newnode;                   /* update start of list to new node */
    } else {
        ReportError (NULL, Err_Memory);
    }
}


/**
   Fetch a module filename from a project file, @projectfilename, and optionally, the
   dependency files which are listed on the same line defined with a trailing ':' after
   the module file name:
        <module filename>: <dependency filename1> <dependency filenameX>

   Dependencies in a project file are only evaluated if the -d option is used on the
   command line for Mpm.

   Empty lines and comment lines are skipped until a real module filename is found. Then,
   on the same line the dependencies are specified.

   return found filename in <filename> variable (truncated to max MAX_FILENAME_SIZE chars),
   and a pointer to a list of dependencies files if the were specified.

 * @brief FetchModuleFilename
 * @param projectfile
 * @param filename
 * @param dependencies
 * @return file pointer of current parse position or EOF if project file failed to be opened
 */
long
FetchModuleFilename(const char *projectfilename, long projfptr, char *filename, filelist_t **dependencies)
{
    FILE *projectfile;
    int c;
    filename[0] = '\0'; /* preset with null-termination, in case no filename was found */
    bool fetchedfilename = false;
    bool cstylecomment = false;

    if ((projectfile = fopen (projectfilename, "rb")) == NULL) {
        /* unable to open project file */
        return EOF;
    }

    fseek (projectfile, projfptr, SEEK_SET);    /* point at beginning of a object file module block */

    while (!fetchedfilename) {
        if (feof (projectfile)) {
            break; /* no more module filenames in project file */
        }

        c = fGetChar (projectfile);
        switch(c) {
            case '\n':
            case '\x1A': /* end of line, get first char on next line */
                break;

            case ';':    /* a comment, skip and prepare for start of next line */
                fSkipLine(projectfile, &cstylecomment);
                break;

            default:
                if (!isspace (c)) {
                    /* found a real char as first of a filename... */
                    ungetc (c, projectfile); /* let Fetchfilename() get first char.. */

                    Fetchfilename(projectfile, filename);

                    /* fetch dependencies, if specified with ':' after this file - otherwise skip line */
                    FetchDependencies (projectfile, dependencies);
                    fetchedfilename = true;
                }
        }
    }

    projfptr = ftell(projectfile);
    fclose(projectfile);

    return projfptr;
}


void
FreeCachedIncludeFile(const includefile_t * inclfile)
{
    if (inclfile != NULL) {
        if (inclfile->fname != NULL) {
            free(inclfile->fname);
        }

        if (inclfile->filedata != NULL) {
            free(inclfile->filedata);
        }
    }
}
